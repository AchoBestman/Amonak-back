const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = { timestamps: true };

const friendsSchema = new Schema({
    from: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    to: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    status: {
        type: Number,
        enums: [
            0,  //'reject',
            1,  //'requested',
            2,  //'pending',
            3,  //'friends',
            4,  //'block friend',
        ]
    }
}, options);

module.exports = Firends = mongoose.model('Friends', friendsSchema);