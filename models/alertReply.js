const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = { timestamps: true };

const AlertReplySchema = new Schema({
    'creator': {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },
    // 'owner_pub': {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: 'User',
    // },
    'publication': {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Publication',
    },
    files: [{
        url: { type: String },
        type: { type: String },
    }],
    content: {
        type: String,
    },
    status: {
        type: String,
        default: 'enabled',
    },
}, options);

module.exports = AlertReply = mongoose.model('AlertReply', AlertReplySchema);
