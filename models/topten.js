// models/topten.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {timestamps: true };

const TopTenSchema = new Schema({
    compagny: String,
    duration: String,
    website: String,
    message: String,
    image: [
        {
            url: String,
            contentType: String
        }
    ],
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    date_fin: Date,
    status: {
        type: String,
        default: 'disabled'
    },
    followers: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }
    ],

}, options);

module.exports = TopTen = mongoose.model('TopTen', TopTenSchema);  