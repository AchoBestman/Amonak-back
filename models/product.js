// models/product.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const options = { timestamps: true };

const ProductSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    files: [{
        url: { type: String },
        type: { type: String }
    }],
    content: {
        type: String
    },
    price: {
        type: Number,
        default: 0,
        required: true,
    },
    max_weight: {
        type: Number,
        required: true,
    },
    currency: {
        type: String,
        default: 'USD',
        required: true,
    },
    quantity: {
        type: Number,
        required: true
    },
    product_category: {
        type: mongoose.Schema.Types.ObjectId, 
        ref:'Categorie',
    },
    address: {
        country_name: { type: String },
        country_code: { type: String },
        city: { type: String },
        postalCode: { type: String },
        street: { type: String }
    },
    status: {
        type: Boolean,
        default: true
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    purchase: {
        type: Number,
        default: 0
    }
}, options);

const Product = mongoose.model('Product', ProductSchema);

module.exports = Product;
