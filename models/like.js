const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// const options = { discriminatorKey: 'type', timestamps: true };
// const options = { discriminatorKey: 'type', timestamps: true , toJSON: { transform: (doc, ret)=>{ console.log(ret);return ret.creator.toString() } } };
const options = { discriminatorKey: 'type', timestamps: true };

const LikeSchema = new Schema({
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
}, options);

const Like = mongoose.model('Like', LikeSchema);

const CommentLike = Like.discriminator('comment', new Schema({
    comment:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment'
    }
}, options));

const PublicationLike = Like.discriminator('publication', new Schema({
    publication:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Publication'
    }
}, options));

module.exports = {
    Like,
    CommentLike,
    PublicationLike
}
