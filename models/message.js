const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MessageSchema = new Schema({
    content: { type: String },
    from: {
        type: mongoose.Schema.Types.ObjectId, 
        ref:'User',
    },
    to: {
        type: mongoose.Schema.Types.ObjectId, 
        ref:'User',
    },
    created_at: {
        type: Date,
        default: Date.now()
    },
    seen_at: { type: Date },
    read_at: { type: Date },
    files: [{
        url: { type: String },
        type: { type: String }
    }],
    status: {
        type: Boolean,
        default: true
    },
    deleters: [],
    type: { type: String, default: 'Text' }
});

module.exports = Message = mongoose.model('Message', MessageSchema);
