const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const options = { timestamps: true };

// Schema defines how friendship will be stored in MongoDB
const AmisSchema = new Schema({
    //pending//accepted//refused//deleted
    status: {
        type: String,
    },
    // blocked: {
    //     type: Number,
    //     enums: [
    //         0, // not blocked
    //         1, //block message,
    //         2, //block post
    //         3 //block to all
    //     ]
    // },
    blocked: {
        type: Boolean,
        default: false
    },
    blocked_date: { type: Date },
    deblocked_date: { type: Date },
    invitationSender: { 
        type: Schema.Types.ObjectId, 
        ref: 'User',
    },
    invitationReceiver: { 
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
}, options);

module.exports = Amis = mongoose.model('Amis', AmisSchema);
