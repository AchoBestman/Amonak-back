// models/comment.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = { timestamps: true };

const CommentSchema = new Schema({
    content: { type: String },
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    likes:[{
        creator: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
        },
        createdAt: { type: Date },
    }],
    comments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment'
    }],
    publication: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Publication'
    },
    status: {
        type: Boolean,
        default: true
    }
}, options);

module.exports = Comment = mongoose.model('Comment', CommentSchema);
