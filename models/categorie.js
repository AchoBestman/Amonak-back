// models/categorie.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = { timestamps: true };

const CategorieSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    description: { type: String },
    image: { type: String },
    status: {
        type: Boolean,
        default: true
    }
}, options);

module.exports = Categorie = mongoose.model('Categorie', CategorieSchema);
