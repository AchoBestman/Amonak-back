// models/user.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = { timestamps: true };
const STATIC_URL = require('../middlewares/api_url').STATIC_URL;

const UserSchema = new Schema({
    firstname: {
        type: String, 
        trim: true,
    },
    lastname: {
        type: String,
        trim: true,
    },
    username: {
        type: String,
        require: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        lowercase: true
    },
    password: {  
        type: String,
        require: true,
        trim: true
    },
    gender: {
        type: String
    },
    sectors: [{
        type: String
    }],
    profession: {
        type: String,
    }, 
    birthday: {
        type: Date,
    },
    birthday_place: {
        type: String,
    },
    studies: {
        type: String
    },
    address: [{
            country_name: { type: String },
            country_code: { type: String },
            state: { type: String },
            city: { type: String },
            postal_code: { type: String },
            street: { type: String}
        }],
    avatar: {
        type: String,
        default: `${STATIC_URL}/static/images/avatar/avatar.png`
    },
    bank_card: [{
        number: { type: String },
        cvc: { type: String },
        zip: { type: String },
        address: { type: String },
    }],
    status: {
        type: Boolean,
        default: false
    },
    phone: {
        type: String
    },
    interested_by: { 
        type: String //ddd
    },
    confessions: {
        type: String //ddd
    },
    languages: {
        type: String
    },
    web_sites: {
        type: String,
    },
    account_type: {
        type: String,
        default: 'default'
    },
    is_log: {
        type: Boolean,
        default: false
    },
    seller_info: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'SellerInfo'
    },
    friends: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Friends'
    }],
    biography: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Biography' 
    },
    countryInfos: {},
    isFirstTime: {
        type: Boolean,
        // default: true
    },
    isNewFeed: {
        type: Boolean,
        // default: true
    },
    data: {}
}, options);
module.exports = User = mongoose.model('User', UserSchema);
 