const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const options = { discriminatorKey: 'type', timestamps: true };

const NotificationSchema = new Schema({
    sender: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },
    receiver: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },
    content: { type: String },
    status: {
        type: Boolean,
        default: true,
    }
}, options);

const Notification = mongoose.model('Notification', NotificationSchema);

const WelcomeNotification = Notification.discriminator("WelcomeNotification", new Schema({
    seenAt: {
        type: Date,
        default: null
    },
    readAt: {
        type: Date,
        default: null
    },
}, options));

const InvitationNotification = Notification.discriminator('InvitationNotification', new Schema({
    // target: {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: 'User'
    // }
}, options));

const PublicationNotification = Notification.discriminator('PublicationNotification', new Schema({
    target: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Publication'
    },
    receivers: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    }],
    readBy: [{
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        readAt: {
            type: Date,
            default: null
        }
    }],
    seenBy: [{
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        seenAt: {
            type: Date,
            default: null
        }
    }],
}, options));

const CommentNotification = Notification.discriminator('CommentNotification', new Schema({
    target: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment'
    }
}, options));


// const MessageNotification = Notification.discriminator('PublicationNotification', new Schema({
//     target: {
//         type: mongoose.Schema.Types.ObjectId,
//         ref: 'Publication'
//     }
// }, options));

// module.exports = Notification = mongoose.model('Notification', NotificationSchema);
module.exports = {
    Notification,
    PublicationNotification,
    CommentNotification,
    InvitationNotification,
    WelcomeNotification
}