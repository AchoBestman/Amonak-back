const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ContactSchema = new Schema({
    _userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    subject: {
        type: String,
        required: false
    },
    message: {
        type: String,
        required: false
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date.now,
        expires: 86400
    }
});

Contact = mongoose.model('Contact', ContactSchema);




const WorkSchema = new Schema({
    _userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    speciality: {
        type: String,
        required: false
    },
    other_speciality: {
        type: String,
        required: false
    },
    message: {
        type: String,
        required: false
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date.now,
        expires: 86400
    },
    image: [
        {
            url: String,
            contentType: String
        }
    ],
});

Work = mongoose.model('Work', WorkSchema);



const MakeSchema = new Schema({
    _userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    delivery_time: {
        type: String,
        required: false
    },
    map_link: {
        type: String,
        required: false
    },
    delivery_adresse: {
        type: String,
        required: false
    },
    code: {
        type: String,
        required: false
    },
    optradio: {
        type: String,
        required: false
    },
    perishable: {
        type: String,
        required: false
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date.now,
        expires: 86400
    },
    image: [
        {
            url: String,
            contentType: String
        }
    ],
});

Make = mongoose.model('Make', MakeSchema);


module.exports = {Work, Contact, Make};
