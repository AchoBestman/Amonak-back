const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = { timestamps: true };

const SellerRequestSchema = new Schema({
    seller_info: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Seller',
    },
    files: [{
        url: { type: String },
        type: { type: String }
    }],
    status: {
        type: Number,
        enums: [
            0, //not read
            1, //accepted
            2, //refused
            3, // cancelled
        ]
    },
    message: { type: String },
}, options);

module.exports = SellerRequest = mongoose.model('SellerRequest', SellerRequestSchema);