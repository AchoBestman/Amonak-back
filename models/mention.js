const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = { timestamps: true };

const MentionSchema = new Schema({
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    publication: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Publication'
    }
}, options);

module.exports = Mention = mongoose.model('Mention', MentionSchema);
