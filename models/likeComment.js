const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const LikeCommentSchema = new Schema({
    date: {
        type: Date,
        default: new Date()
    },
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    comment: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment'
    }
});

module.exports = LikeComment = mongoose.model('LikeComment', LikeCommentSchema);