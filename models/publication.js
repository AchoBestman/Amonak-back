// models/publication.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const options = { discriminatorKey: 'type', timestamps: true };

const PublicationSchema = new Schema({
    content: { type: String },
    share: {
        type: Number,
        default: 0
    },
    files: [{
        url: { type: String },
        type: { type: String },
    }],
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    comments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment'
    }],
    likes:[{
        creator: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
        },
        createdAt: { type: Date },
    }],
    deleters: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    followers: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    }],
    savers: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    }],
    sharedBy: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    }],
    status: {
        type: Boolean,
        default: true
    }
}, options);

const PublicationReportedSchema = new Schema({
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },
    publication: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Publication',
    },
    reason: {
        type: String
    }
}, options); 

const Publication = mongoose.model('Publication', PublicationSchema);
const PostPublication = Publication.discriminator('Post', new Schema({}, options));
const SalePublication = Publication.discriminator('Sale', new Schema({
    sale_type: {
        type: String
    },
    product: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product'
    }
}, options));
const AlertPublication = Publication.discriminator('Alert', new Schema({
    name: { type: String },
    reply: {
        type: Number,
        default: 0
    },
    duration: {
        type: String,
        default: 'short'
    },
    alert_type: {
        type: String,
        default: 'Product'
    }
}, options));
const Publicity = Publication.discriminator('Publicity', new Schema({
    publicity: String
}, options));
const SharePublication = Publication.discriminator('Share', new Schema({
    publication: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Publication'
    }
}, options));
const PublicationReported = mongoose.model('PublicationReported', PublicationReportedSchema);
module.exports = {
    Publication,
    PostPublication,
    SalePublication,
    AlertPublication,
    Publicity,
    SharePublication,
    PublicationReported
};
