const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = { timestamps: true };

const SellerInfoSchema = new Schema({
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },
    email: { 
        type: String,
        trim: true,
        lowercase: true,
        // unique: true,
        required: true
    },
    phone_number: { 
        type: String
    },
    address: {
        country_name: { type: String },
        country_code: { type: String },
        city: { type: String },
        postalCode: { type: String },
        street: { type: String }
    },
    product_nature: [{ 
        type: String,
    }],
    identity_card: {
        url: { type: String },
        type: { type: String },
    },
    register_number: {
        type: String,
    },
    type: { type: String },
}, options);

module.exports = SellerInfo = mongoose.model('SellerInfo', SellerInfoSchema);
