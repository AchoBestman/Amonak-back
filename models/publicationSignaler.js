const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const options = { timestamps: true }

const PublicationSignalerSchema = new Schema({
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },
    publication: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Publication',
    },
    reason: {
        type: String
    }
}, options);

module.exports = PublicationSignaler = mongoose.model('PublicationSignaler', PublicationSignalerSchema);
