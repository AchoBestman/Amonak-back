// models/topten.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {timestamps: true };
 
const PanierSchema = new Schema({
    creator: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User'
    },
    product: [   
        {
            product_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Product'
            },
            seller_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
            },
            prix: {
             type: Number,
             default: 0
            },
            quantity: {
             type: Number, 
             default: 0
            },
            tax: {
             type: Number,
             default: 0
            },
            shipping: {
             type: Number,
             default: 0
            },
            date_ajouter: Date
        }
    ], 
    amount: {
        type: Number,
        default: 0
    },
    quantity: {
        type: Number,
        default: 0
    },
    shipping: {
        type: Number,
        default: 0
    },
    tax: {
        type: Number,
        default: 0 
    },
    status: {
        type: String,
        default: 'basket' //buy or basket or delivery
    },
    payement_type: String, //debit or credit card or paypal
    payement_id: String,
    payement_date: Date,
    comment: {
        type: String,
        default: ''
    },
    seller_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    payer_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    seller_address: {
        type: String,
        default: ''
    },
    payer_address: {
        type: String,
        default: ''
    },
    transaction_id: {
        type: String,
        default: ''
    }, 

    statut_cout_livraison: {
        type: String,
        default: '' // {en_cour, terminer"}
    },

}, options);

module.exports = Panier = mongoose.model('Panier', PanierSchema);  