const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = { timestamp: true };

const NewsletterSchema = new Schema({
    name: { type: String },
    address: { type: String },
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        lowercase: true
    },
    status: {
        type: Boolean,
        default: true
    }
}, options);

module.exports = Newsletter = mongoose.model('Newsletter', NewsletterSchema);
