// models/user.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = { timestamps: true };

const BibliographySchema = new Schema({
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },  
    firstname: String,
    lastname: String,
    emploi: [ {
        lieu: {type: String},
        fonction: {type: String},
        description: {type: String},
        date_debut: {
            annee: {type: String},
            mois:  {type: String},
            jour:  {type: String}
        },
        date_fin: {
            annee: {type: String},
            mois:  {type: String},
            jour:  {type: String}
        },
        visibilite: {type:String}
    } ],

    scolarite: {

        universite: [ {
            lieu: {type: String},
            nom_universite: {type: String},
            diplome: {type: String},
            date_debut: {
                annee: {type: String},
                mois:  {type: String},
                jour:  {type: String}
            },
            date_fin: {
                annee: {type: String},
                mois:  {type: String},
                jour:  {type: String}
            },
            visibilite: {type:String}
        } ],

        lycee: [ {
            lieu: {type: String},
            nom_lycee: {type: String},
            diplome: {type: String},
            date_debut: {
                annee: {type: String},
                mois:  {type: String},
                jour:  {type: String}
            },
            date_fin: {
                annee: {type: String},
                mois:  {type: String},
                jour:  {type: String}
            },
            visibilite: {type:String}
        } ]
    },

    residence: [{
        pays: {type: String},
        ville: {type: String},
        date_debut: {
                annee: {type: String},
                mois:  {type: String},
                jour:  {type: String}
            },
            date_fin: {
                annee: {type: String},
                mois:  {type: String},
                jour:  {type: String}
            },
            visibilite: {type:String}
    }],
    coordonnes: [{
        telephone: {type: String},
        email: {type: String},
        site_web: {type: String},
        reseau_social: {type: String},
        adresses: {
            rue: {type: String},
            code_postal: {type: String},
            quartier: {type: String},
        },
        visibilite: {type:String}

    }],
    civilite: {
        nom: {type: String},
        prenom: {type: String},
        sexe: { type: String },
        date_naissance: { type: String },
        lieu_naissance: { type: String },
        matrimoniale: { type: String}
    },
    langues: [{
        type: String,
    }],
    croyances: [{
        religion: {type: String},
        description: {type: String},
    }],
    politiques: [{
        libelle: {type: String},
        description: {type: String},
    }],

    interesse_par: [{
        type:String,
    }],
    surnom: [{
        type:String
    }],
    membre_famille: [{
        type:Boolean
    }],
}, options);
module.exports = Bibliography = mongoose.model('Bibliography', BibliographySchema);