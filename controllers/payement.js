const path = require('path');
const atob = require('atob');
const {API_URL, FRONT_URL} = require('../middlewares/api_url');
const Panier  = require('../models/panier');

exports.send = (req, res, next) => { 
	let buyer_info = atob(req.query['buyer_info']);
	let cancel = `${API_URL}/payement/cancel?${buyer_info}`;
	let success = `${API_URL}/payement/success?${buyer_info}`;
	res.render('paye', {token: atob(req.params['token']), success: success, cancel: cancel});
}

exports.cancel = (req, res, next) => { 
    res.redirect(`${FRONT_URL}/home`);
} 

exports.success = (req, res, next) => {

	let pay_infos = {};
	let d = new Date();
    let date_ajouter = d.setDate(d.getDate());

	pay_infos.transaction_id = parseInt(req.query.transaction);
	pay_infos.payment_token = req.query.payment_token;
	pay_infos.prix = parseFloat(req.query.prix);
	pay_infos.quantity = parseInt(req.query.quantity);
	pay_infos.tax = parseFloat(req.query.tax);
	pay_infos.shipping = parseFloat(req.query.shipping);
	pay_infos.status = req.query.status;
	pay_infos.payer_id = req.query.user_id; 
	pay_infos.payement_type = req.query.payement_type;
 

	if (req.query.seller_id) {
		pay_infos.seller_id = req.query.seller_id;
	}
	if (req.query.product_id) {
		pay_infos.payement_id = req.query.product_id;
	}
	if (req.query.payement_id) {
		pay_infos.payement_id = req.query.payement_id;
	}

	if (pay_infos.quantity == 1 && !req.query.product_id) {
		let query = {
			status: pay_infos.status, 
			amount: pay_infos.prix, 
			payement_type: pay_infos.payement_type,
			payement_date: date_ajouter, 
			payement_id: pay_infos.payement_id,
			shipping: pay_infos.shipping,
			tax: pay_infos.tax,
			quantity: pay_infos.quantity,
			transaction_id: pay_infos.transaction_id,
			payer_id: pay_infos.payer_id
		};

		if (pay_infos.seller_id) {
			query.seller_id = pay_infos.seller_id
		}

		Panier.updateOne({ creator: pay_infos.payer_id, status: 'basket'}, query
			)
          .then(panier => 
          	{
          		console.log('Achat finaliser');
          		res.redirect(`${FRONT_URL}/invoice/${pay_infos.transaction_id}`);
          		
          	})
          .catch(err => {console.log(err); res.status(500).json(err)});
	}else{

		if (req.query.product_id && pay_infos.status == 'buy'){
				let newPanier = new Panier({
			       creator: pay_infos.payer_id
			    });

				newPanier.product.push({
               	product_id: pay_infos.product_id,
               	seller_id: pay_infos.seller_id,
               	prix: pay_infos.prix/pay_infos.quantity,
               	tax: pay_infos.tax,
               	shipping: pay_infos.shipping,
				quantity: pay_infos.quantity,
				date_ajouter: date_ajouter
               });
				newPanier.shipping = pay_infos.shipping;
				newPanier.status = pay_infos.status;
				newPanier.tax  = pay_infos.tax;
	    		newPanier.amount = pay_infos.prix;
	    		newPanier.quantity = pay_infos.quantity;
	    		newPanier.transaction_id = pay_infos.transaction_id;
	    		newPanier.payement_type = pay_infos.payement_type;
	    		newPanier.payement_date = date_ajouter;
	    		newPanier.payement_id = pay_infos.payement_id;
				newPanier.payer_id = pay_infos.payer_id;
				newPanier.payer_address = pay_infos.payer_address;

				if (pay_infos.seller_id) {
					newPanier.seller_id = pay_infos.seller_id
				}

	    		newPanier.save()
                .then(panier => {
                	console.log('new created basket with pay');
                  	console.log(panier);
                	res.redirect(`${FRONT_URL}/invoice/${pay_infos.transaction_id}`);
                }).catch(err => res.status(500).json(err));
		}
	}

}
