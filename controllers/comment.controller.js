const Comment = require('../models/comment');
const { Publication } = require('../models/publication');
const { PublicationNotification } = require('../models/notification');
const { STATIC_URL } = require('../middlewares/api_url');

exports.save = (req, res, next) => {
    const publication_id = req.body.publication_id;
    const user_id = req.body.user_id;
    const content = req.body.content;
    const new_comment = new Comment({
        content: content,
        creator: user_id,
        publication: publication_id
    });
    new_comment.save()
        .then(comment => {
            comment.populate('creator', 'username avatar sectors', (err, data) => {
                if(err) throw err
                const query = { $push :  { comments: data._id } };
                
                Publication.findOneAndUpdate({_id: publication_id}, query, { new: true, useFindAndModify: false })
                    .then(publication => {
                        
                        const notif = new PublicationNotification({
                            sender: user_id,
                            receiver: publication.creator,
                            content: "publicationComment",
                            receivers: publication.followers,
                            target: publication_id
                        });
                    
                        notif.save()
                        .then(notifCreated => {
                            // console.log(notifCreated);
                            notifCreated
                            .populate('target')
                            .populate('sender', 'username sectors avatar is_log', (err, data) => {
                                if(err) throw err
                                console.log('in here::::');
                                    res.io.emit('notification', performData(data));
                                })
                                
                            })
                            .catch(err => res.status(500).json(err));
                    })
                    .catch(err => res.status(500).json(err))
                res.json(performData(data));
            });
        })
        .catch(err => res.status(500).json(err));
}

exports.find = (req, res, next) => {
    Comment.find()
        .sort({createdAt: -1})
        .populate('creator', 'username sectors avatar')
        .populate('puplication', '-__v')
        .then(comments => res.json(comments))
        .catch(err => res.status(500).json(err));
}

exports.findById = (req, res, next) => {
    const comment_id = req.params.id;
    Comment.findById(comment_id)
        .populate('creator', 'username sectors avatar')
        .populate('publication', '-__v')
        .then(comment => { res.json(comment) })
        .catch(err => res.status(500).json(err)); 
}

exports.findPublicationComments = (req, res, next) => {
    const user_id = req.params.user_id;
    const query = { publication: req.params.id };
    if(user_id) {
        query.creator = user_id;
    }
    Comment.find(query)
        .sort({createdAt: -1})
        .populate('creator', 'username sectors avatar')
        .then(comments => res.json(comments))
        .catch(err => res.status(500).json(err));
}

exports.findUserComments = (req, res, next) => {
    const query = { creator: req.params.id };
    Comment.find(query)
        .sort({createdAt: -1})
        .populate('publication', '-__v')
        .then(comments => res.json(comments))
        .catch(err => res.status(500).json(err));
}

exports.findByContent = (req, res, next) => {
    const searchText = req.params.search;
    Comment.find({content: {$regex: new RegExp(searchText, 'i')}})
        .sort({createdAt: -1})
        .populate('creator', 'username sectors avatar')
        .then(comments => res.json(comments))
        .catch(err => res.status(500).json(err));
}

exports.addLike = (req, res, next) => {
    const userId = req.body.userId;
    const comment_id = req.params.id;
    const query = { $push :  { likes: { creator: userId, createdAt: Date.now() } } };
    Comment.findByIdAndUpdate(comment_id, query, { new: true, useFindAndModify: false })
        .populate('creator', 'username sectors avatar likes')
        .then(comment => res.json(comment))
        .catch(err => res.status(500).json(err));
}

exports.addComment = (req, res, next) => {
    const comment_id = req.params.id;
    const new_comment = new Comment({
        creator: req.body.user_id,
        content: req.body.content,
    });
    new_comment.save()
        .then(comment => {
            const query = { $push :  { comments: comment._id } };
            Comment.findByIdAndUpdate(comment_id, query, { new: true, useFindAndModify: false })
                .populate('likes.creator', 'username sectors avatar')
                .then(comment => {
                    console.log(comment);
                    res.json(comment);
                })
                .catch(err => res.status(500).json(err));
        })
        .catch(err => res.status(500).json(err));
}

exports.removeLike = (req, res, next) => {
    const userId = req.body.userId;
    const comment_id = req.params.id;
    const query = { $pull :  { likes: { creator: userId } } };
    Comment.findByIdAndUpdate(comment_id, query, { new: true, useFindAndModify: false })
        .populate('creator', 'username sectors avatar likes')
        .then(comment => res.json(comment))
        .catch(err => res.status(500).json(err));
}

exports.removeComment = (req, res, next) => {
    const old_comment_id = req.body.comment_id;
    const comment_id = req.params.id;
    const query = { $pull :  { comments: old_comment_id } };
    Comment.findByIdAndUpdate(comment_id, query, { new: true, useFindAndModify: false })
        .populate('creator', 'username sectors avatar')
        .then(comment => res.json(comment))
        .catch(err => res.status(500).json(err));
}

exports.delete = (req, res, next) => {
    const query = { status: false };
    const comment_id = req.params.id;
    Comment.findByIdAndUpdate(comment_id, query, { new: true, useFindAndModify: false })
        .then(comment => {
            req.body.comment_id = comment._id;
            res.json(comment);
            next();
        })
        .catch(err => res.status(500).json(err));
}

function performData(data) {
    if(data.length) {
      return data.map(elt => {
        if(elt.creator && elt.creator.avatar) {
            elt.creator.avatar = `${STATIC_URL}/static${elt.creator.avatar.split('/static').length > 1 ? elt.creator.avatar.split('/static')[1]: elt.creator.avatar.split('/static')[0]}`;
        }
        if(elt.sender && elt.sender.avatar) {
          elt.sender.avatar = `${STATIC_URL}/static${elt.sender.avatar.split('/static').length > 1 ? elt.sender.avatar.split('/static')[1]: elt.sender.avatar.split('/static')[0]}`;
        }
        if(elt.receiver && elt.receiver.avatar) {
          elt.receiver.avatar = `${STATIC_URL}/static${elt.receiver.avatar.split('/static').length > 1 ? elt.receiver.avatar.split('/static')[1]: elt.receiver.avatar.split('/static')[0]}`;
        }
        if(elt.receivers && elt.receivers.length) {
            elt.receivers.forEach(r => {
                if(r && r.avatar) {
                    r.avatar = `${STATIC_URL}/static${r.avatar.split('/static').length > 1 ? r.avatar.split('/static')[1]: r.avatar.split('/static')[0]}`;
                }
            })
        }
        if(elt.readBy && elt.readBy.length) {
            elt.readBy.forEach(r => {
                if(r && r.user && r.user.avatar) {
                    r.avatar = `${STATIC_URL}/static${r.avatar.split('/static').length > 1 ? r.avatar.split('/static')[1]: r.avatar.split('/static')[0]}`;
                }
            })
        }
        if(elt.seenBy && elt.seenBy.length) {
            elt.seenBy.forEach(r => {
                if(r && r.user && r.user.avatar) {
                    r.avatar = `${STATIC_URL}/static${r.avatar.split('/static').length > 1 ? r.avatar.split('/static')[1]: r.avatar.split('/static')[0]}`;
                }
            })
        }
        return elt;
      })
    }
    if(data.creator && data.creator.avatar) {
        data.creator.avatar = `${STATIC_URL}/static${data.creator.avatar.split('/static').length > 1 ? data.creator.avatar.split('/static')[1]: data.creator.avatar.split('/static')[0]}`;
    }
    if(data.sender && data.sender.avatar) {
      data.sender.avatar = `${STATIC_URL}/static${data.sender.avatar.split('/static').length > 1 ? data.sender.avatar.split('/static')[1]: data.sender.avatar.split('/static')[0]}`;
    }
    if(data.receiver && data.receiver.avatar) {
      data.receiver.avatar = `${STATIC_URL}/static${data.receiver.avatar.split('/static').length > 1 ? data.receiver.avatar.split('/static')[1]: data.receiver.avatar.split('/static')[0]}`;
    }
    if(data.receivers && data.receivers.length) {
        data.receivers.forEach(r => {
            if(r && r.avatar) {
                r.avatar = `${STATIC_URL}/static${r.avatar.split('/static').length > 1 ? r.avatar.split('/static')[1]: r.avatar.split('/static')[0]}`;
            }
        })
    }
    if(data.readBy && data.readBy.length) {
        data.readBy.forEach(r => {
            if(r && r.user && r.user.avatar) {
                r.avatar = `${STATIC_URL}/static${r.avatar.split('/static').length > 1 ? r.avatar.split('/static')[1]: r.avatar.split('/static')[0]}`;
            }
        })
    }
    if(data.seenBy && data.seenBy.length) {
        data.seenBy.forEach(r => {
            if(r && r.user && r.user.avatar) {
                r.avatar = `${STATIC_URL}/static${r.avatar.split('/static').length > 1 ? r.avatar.split('/static')[1]: r.avatar.split('/static')[0]}`;
            }
        })
    }
    return data;
}
