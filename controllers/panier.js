const Panier  = require('../models/panier');
const {API_URL, FRONT_URL, STATIC_URL} = require('../middlewares/api_url');

let d = new Date();
let date_ajouter = d.setDate(d.getDate());

exports.createPanier = (req,res,next) => {
console.log(req.body)
	let newPanier = new Panier({
       creator: req.body.user_id
    });

	if (req.body.delete_from_basket) {
		let items = req.body.delete_from_basket;
		let item = items.split('-'); 
		let product_id = item[0]; 
		let panier_id = item[1];
		let query = {$pull: { product: { _id: product_id } } }
		Panier.findByIdAndUpdate(panier_id, query, { new: true, useFindAndModify: false })
			   .then(paniers => {
			   	res.status(200).json(paniers);
		        console.log('product remove');
		        console.log(paniers);
			   	}).catch(err => res.status(500).json(err));
	}
 
	if (req.body.status == 'reset') {
		Panier.updateOne({ creator: req.body.user_id, status: 'basket'},
			{status: 'reset'}
			)
          .then(panier => {res.json(panier); console.log('Achat Anuler');})
          .catch(err => res.status(500).json(err));
	}

	if (req.body.statut_cout_livraison && req.body.statut_cout_livraison == 'en_cour') {
		Panier.updateOne({ creator: req.body.user_id, _id: req.body.panier_id},
			{statut_cout_livraison: req.body.statut_cout_livraison}
			)
          .then(panier => {res.json(panier); console.log('Demande de coût de livraison envoyé');})
          .catch(err => res.status(500).json(err));
	}

	if (parseInt(req.body.quantity) == 1 && !req.body.product_id) {
 
		let query = {
			status: req.body.status, 
			amount: parseFloat(req.body.prix), 
			payement_type: req.body.payement_type,
			payement_date: date_ajouter, 
			payement_id: req.body.payement_id,
			shipping: parseFloat(req.body.shipping),
			tax: parseFloat(req.body.tax),
			quantity: parseFloat(req.body.quantity),
			transaction_id: Math.random().toString(36).substring(2, 15)+''+req.body.payement_id.slice(5,8),
			payer_id: req.body.payer_id,
			comment: req.body.comment,
			payer_address: req.body.payer_address
		};

		if (req.body.seller_id) {
			query.seller_id = req.body.seller_id
		}
		Panier.updateOne({ creator: req.body.user_id, status: 'basket'}, query
			)
          .then(panier => {res.json(`${FRONT_URL}/invoice/${query.transaction_id}`);})
          .catch(err => {console.log(err); res.status(500).json(err)});
	}else{

		if (req.body.status != undefined && req.body.status == 'basket') {
			Panier.find({ creator: req.body.user_id, status: 'basket'}).countDocuments()
			   .then(panier_length => {
			   		if (panier_length) {
			   			let query = { $push: { 
						   				product: {
						   				product_id: req.body.product_id,
						               	seller_id: req.body.seller_id,
						               	prix: req.body.prix,
										quantity: req.body.quantity,
										tax: req.body.tax,
										shipping: req.body.shipping,
										date_ajouter: date_ajouter
						   				} 
			   					} };

			   			Panier.find({ creator: req.body.user_id, status: 'basket'})
			   			.then(panier => {
			   			 let trouve = false;
			   			 if (trouve) {
			   			 	console.log('troué');
			   			 }else{
			   			  Panier.findByIdAndUpdate(panier[0]._id, query, { new: true, useFindAndModify: false })
			   			  .then(paniers => {
			   			  	res.status(200).json(paniers);
		                  	console.log('new created basket by update');
		                  	console.log(paniers);
			   			  }).catch(err => res.status(500).json(err));
			   			  }

			   			}).catch(err => res.status(500).json(err));
                       
			   		}else{
			   			newPanier.product.push({
		               	product_id: req.body.product_id,
		               	seller_id: req.body.seller_id,
		               	prix: req.body.prix,
		               	tax: req.body.tax,
		               	shipping: req.body.shipping,
						quantity: req.body.quantity,
						date_ajouter: date_ajouter
		               });
			   		   newPanier.status = req.body.status;

			   		   newPanier.save()
		                .then(panier => {
		                  res.status(200).json(panier);
		                  console.log('new created basket');
		                }).catch(err => res.status(500).json(err));
			   		}
			   }).catch(err => res.status(500).json(err));
		}
		
		if (req.body.status != undefined && req.body.status == 'booking'){ 
				newPanier.product.push({
               	product_id: req.body.product_id,
               	seller_id: req.body.seller_id,
               	prix: parseFloat(req.body.prix)/parseFloat(req.body.quantity),
               	tax: parseFloat(req.body.tax),
               	shipping: parseFloat(req.body.shipping),
				quantity: parseFloat(req.body.quantity),
				date_ajouter: date_ajouter
               });

				newPanier.status = req.body.status;
				newPanier.shipping = parseFloat(req.body.shipping);
	    		newPanier.amount = parseFloat(req.body.prix);
	    		newPanier.quantity = parseFloat(req.body.quantity);
	    		newPanier.tax = parseFloat(req.body.tax);
	    		newPanier.payement_type = req.body.payement_type;
	    		newPanier.payement_date = date_ajouter;
	    		newPanier.comment = req.body.comment;
	    		newPanier.payer_id = req.body.payer_id;
	    		newPanier.payer_address = req.body.payer_address;
	    		newPanier.transaction_id = Math.random().toString(36).substring(2, 15)+''+req.body.product_id.slice(5,8);
	    		newPanier.payement_id = req.body.product_id;
 				console.log(newPanier);
	    		newPanier.save()
                .then(panier => {
                  res.json(`${FRONT_URL}/invoice/${newPanier.transaction_id}`);
                  console.log('new created basket with booking');
                  console.log(panier);
                  console.log(panier);
                }).catch(err => res.status(500).json(err));
		}
	}

}


exports.getAll = (req, res, next) => { 
    Panier.find({})
        .sort({createdAt: -1})
        .populate('creator','username sectors avatar phone email _id address') 
        .populate('product.product_id')
        .populate('product.seller_id')
        .then(panier => res.json(performData(panier)))
        .catch(err => res.status(500).json(err));
}

exports.getOneByStatus = (req, res, next) => {
    Panier.find({creator: req.params.user_id, status: req.params.status})
        .sort({createdAt: -1})
        .populate('creator')
        .populate('product.product_id')
        .then(panier => {console.log(panier); res.json(performData(panier));})
        .catch(err => res.status(500).json(err));
}
 
exports.getPanierByTransactionId = (req, res, next) => {
    Panier.findOne({transaction_id: req.params.id})
        .sort({createdAt: -1})
        .populate('creator')
        .populate({
        	path: 'product',
        	populate: [
                { path: 'product_id', select: 'name content' },
                { path: 'seller_id', select: 'username' },
            ],
        }) 
        .then(panier => {console.log(panier); res.json(performData(panier))})
        .catch(err => res.status(500).json(err));
}

exports.updatePanierCommentAdress = (req, res, next) => {
	if (req.body.comment && req.body.address) {
		console.log(req.body);
		let query = { comment: req.body.comment, payer_address: req.body.address};
		Panier.updateOne({ creator: req.body.user_id, status: 'basket'}, query
			)
          .then(panier => {res.json(panier); console.log('Panier mise à jour');})
          .catch(err => {console.log(err); res.status(500).json(err)});
	}
}

function performData(panier) {
	if(panier.length) {
		return panier.map(elt => {
			if(elt.creator && elt.creator.avatar) {
				elt.creator.avatar = `${STATIC_URL}/static${elt.creator.avatar.split('/static').length > 1 ? elt.creator.avatar.split('/static')[1]: elt.creator.avatar.split('/static')[0]}`;
            }
            if(elt.product && elt.product.length) {
                elt.product.forEach(product => {
					if(product && product.product_id && product.product_id.files && product.product_id.files.length) {
						product.product_id.files.forEach(file => {
							file.url = `${STATIC_URL}/static${file.url.split('/static').length > 1 ? file.url.split('/static')[1]: file.url.split('/static')[0]}`;
						})	
					}
					if(product && product.seller_id && product.seller_id.avatar) {
						product.seller_id.avatar = `${STATIC_URL}/static${product.seller_id.avatar.split('/static').length > 1 ? product.seller_id.avatar.split('/static')[1]: product.seller_id.avatar.split('/static')[0]}`;
					}
                })
            }
			if(elt.seller_id && elt.seller_id.avatar) {
                elt.seller_id.avatar = `${STATIC_URL}/static${elt.seller_id.avatar.split('/static').length > 1 ? elt.seller_id.avatar.split('/static')[1]: elt.seller_id.avatar.split('/static')[0]}`;
            }
			if(elt.payer_id && elt.payer_id.avatar) {
                elt.payer_id.avatar = `${STATIC_URL}/static${elt.payer_id.avatar.split('/static').length > 1 ? elt.payer_id.avatar.split('/static')[1]: elt.payer_id.avatar.split('/static')[0]}`;
            }
            return elt;
        })
    }
	if(panier && panier.creator && panier.creator.avatar) {
		panier.creator.avatar = `${STATIC_URL}/static${panier.creator.avatar.split('/static').length > 1 ? panier.creator.avatar.split('/static')[1]: panier.creator.avatar.split('/static')[0]}`;
	}
	if(panier && panier.product && panier.product.length) {
		panier.product.forEach(product => {
			if(product && product.product_id && product.product_id.files && product.product_id.files.length) {
				product.product_id.files.forEach(file => {
					file.url = `${STATIC_URL}/static${file.url.split('/static').length > 1 ? file.url.split('/static')[1]: file.url.split('/static')[0]}`;
				})	
			}
			if(product && product.seller_id && product.seller_id.avatar) {
				product.seller_id.avatar = `${STATIC_URL}/static${product.seller_id.avatar.split('/static').length > 1 ? product.seller_id.avatar.split('/static')[1]: product.seller_id.avatar.split('/static')[0]}`;
			}
		})
	}
	if(panier && panier.seller_id && panier.seller_id.avatar) {
		panier.seller_id.avatar = `${STATIC_URL}/static${panier.seller_id.avatar.split('/static').length > 1 ? panier.seller_id.avatar.split('/static')[1]: panier.seller_id.avatar.split('/static')[0]}`;
	}
	if(panier && panier.payer_id && panier.payer_id.avatar) {
		panier.payer_id.avatar = `${STATIC_URL}/static${panier.payer_id.avatar.split('/static').length > 1 ? panier.payer_id.avatar.split('/static')[1]: panier.payer_id.avatar.split('/static')[0]}`;
	}
    return panier;
}

