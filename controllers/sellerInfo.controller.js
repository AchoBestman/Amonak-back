const { STATIC_URL } = require('../middlewares/api_url');
const sellerInfo = require('../models/sellerInfo');
const SellerInfo = require('../models/sellerInfo');

exports.save = (req, res, next) => {
    const creator = req.body.creator;
    const email = req.body.email;
    const phone_number = req.body.phone_number;
    const address = JSON.parse(req.body.address);
    const product_nature = req.body.product_nature;
    let identity_card = req.body.identity_card;
    identity_card.url = `${STATIC_URL}/static/images/uploads/${identity_card.name}`;
    const register_number = req.body.register_number;
    const type = req.body.type;
    
    let seller_info = new SellerInfo({
        creator: creator,
        email: email,
        phone_number: phone_number,
        address: address,
        product_nature: product_nature,
        identity_card: identity_card,
        register_number: register_number,
        type: type
    });
    console.log('new seller info::::', seller_info);
    seller_info.save()
        .then(info => {
            console.log('infoooooo=====:::::', info);
            req.body.seller_info = info._id;
            req.body.account_type = "pending";
            next();
        })
        .catch(err => {
            console.log('errrrrrrr:::::::::::', err);
            res.status(500).json(err)});
}

exports.find = (req, res, next) => {
    SellerInfo.find()
        .then(infos => res.json(infos))
        .catch(err => res.status(500).json(err));
}

exports.findOne = (req, res, next) => {
    const id = req.params.id;
    SellerInfo.findOne({ _id: id })
        .then(info => res.json(info))
        .catch(err => res.status(500).json(err));
}