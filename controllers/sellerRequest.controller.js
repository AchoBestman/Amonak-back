const SellerInfo = require('../models/sellerInfo');
const sellerRequest = require('../models/sellerRequest');
const SellerRequest = require('../models/sellerRequest');
const { STATIC_URL } = require('../middlewares/api_url');

exports.makeRequest = (req, res, next) => {
    console.log(req.body);
    const seller_info = req.body.seller_info;
    let seller_request = new SellerRequest({
        seller_info: seller_info,
        status: 0,
    });
    if(req.body.files) {
        let files = req.body.files;
        console.log(files);
        files.forEach(elt => {
            if(elt.type.match('image/*')) {
                seller_request.files.push({
                    url: `${STATIC_URL}/static/images/uploads/${elt.name}`,
                    type: elt.type
                })
            } else if(elt.type.match('video/*')) {
                seller_request.files.push({
                    url: `${STATIC_URL}/static/videos/uploads/${elt.name}`,
                    type: elt.type
                })
            }
        })
    }
    seller_request.save()
        .then(seller => {
            console.log(seller);
            // res.json(seller);
            next();
        })
        .catch(err => res.status(500).json(err));
}

exports.findAll = (req, res, next) => {
    SellerInfo.find()
        .then(sellers => {
            console.log(sellers);
            res.json(sellers);
        })
        .catch(err => res.status(500).json(err));
};

exports.modifyOne = (req, res, next) => {
    console.log("sdfghjkl");
}

exports.acceptRequest = (req, res, next) => {
    const id = req.params.id;
    const query = { status: 1 };
    SellerRequest.findOneAndUpdate({ _id: id }, query, { new: true, useFindAndModify: false })
        .then(request => {
            // res.json(request);
            req.body.creator = request.seller_info.creator;
            req.body.account_type = "seller";
            req.body.seller_id = request.seller_info._id;
            next();
        })
        .catch(err => res.status(500).json(err));
}

exports.declineRequest = (req, res, next) => {
    const id = req.params.id;
    const query = { status: 2, message: req.body.message };
    SellerRequest.findOneAndUpdate({ _id: id }, query, { new: true, useFindAndModify: false })
        .populate('seller_info', 'creator')
        .then(request => {
            req.body.creator = sellerRequest.creator;
            req.body.account_type = "refused";
            // res.json(request);
            next();
        })
        .catch(err => res.status(500).json(err));
}

exports.cancelRequest = (req, res, next) => {
    const id = req.body.seller_id;
    const query = { status: 3 };
    console.log('==================', id, query);
    if(!id) {
        res.status(500).json(err);
        return;
    }
    SellerRequest.findOneAndUpdate({ "seller_info": id }, query, { new: true, useFindAndModify: false })
        .then(request => {
            console.log('==============', request);
            next();
        })
        .catch(err => {
            console.log('+++++++++++++++++=========errr',err);
            res.status(500).json(err)});
}

