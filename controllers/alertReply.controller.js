const { STATIC_URL } = require('../middlewares/api_url');
const AlertReply = require('../models/alertReply');
const { AlertPublication } = require('../models/publication');

exports.create = (req, res, next) => {
    let newAlertReply = new AlertReply({
        creator: req.body.creator,
        publication: req.body.publication,
        // owner_pub: req.body.owner_pub,
    });
    if(req.body.content) {
        newAlertReply.content = req.body.content;
    }
    if(req.body.files) {
        req.body.files.forEach(element => {
            if(element.type.match('image/*')){
                newAlertReply.files.push({
                    url: `${STATIC_URL}/static/images/uploads/${element.name}`,
                    type: element.type
                });
            } else if(element.type.match('video/*')) {
                newAlertReply.files.push({
                    url: `${STATIC_URL}/static/videos/uploads/${element.name}`,
                    type: element.type
                });
            }
        });
    }
    AlertPublication.findByIdAndUpdate(req.body.publication, {$inc: {reply: 1}}, {new: true, useFindAndModify: false})
        .then((publication) => {
            newAlertReply.save()
                .then(alertReply => {
                    req.body.destinateur = publication.creator;
                    req.body.data = publication.files;

                    next();
                    res.json({alertReply, publication});
                })
                .catch(err => res.status(500).json(err));
        })
        .catch(err => res.status(500).json(err));
};

exports.find = (req, res, next) => {
    AlertReply.find({status: 'enabled'})
        .then(alerts => res.json(alerts))
        .catch(err => res.status(500).json(err));
};

exports.findById = (req, res, next) => {
    AlertReply.findById(req.params.id)
        .then(alert => res.json(alert))
        .catch(err => res.status(500).json(err));
};

exports.delete = (req, res, next) => {
    AlertReply.findByIdAndUpdate(req.params.id, {status: 'disabled'}, { new: true, useFindAndModify: false })
        .then(alert => res.json(alert))
        .catch(err => res.status(500).json(err));
};
