const Product = require('../models/product');
const { STATIC_URL } = require('../middlewares/api_url');

exports.save = (req, res ,next) => {
    console.log(req.body)
    let new_product = new Product({
        name: req.body.name,
        content: req.body.content,
        price: parseFloat(req.body.price),
        max_weight: parseInt(req.body.max_weight),
        currency: req.body.currency,
        quantity: parseInt(req.body.quantity),
        address: req.body.address,
        // address: JSON.parse(req.body.address),
        owner: req.body.user_id,
    });
    if(req.body.files) {
        let files = req.body.files;
        files.forEach(element => {
            if(element.type.match('image/*')) {
                new_product.files.push({
                    url: `${STATIC_URL}/static/images/uploads/${element.name}`,
                    type: element.type
                });
            }
        });
    }
    new_product.save()
        .then(product => {
            req.body.product_id = product._id;
            req.body.files = product.files;
            next();
        })
        .catch(err => { return res.status(500).json(err); })
};

exports.find = (req, res, next) => {
    Product.find()
        .populate("owner", "username sectors avatar")
        .then(products => res.json(performData(products)))
        .catch(err => res.status(500).json(err))
};

exports.findById = (req, res, next) => {
    Product.findById(req.params.id)
        .populate("owner", "username sectors avatar")
        .then(product => res.json(performData(product)))
        .catch(err => res.status(500).json(err))
};

// exports.findOne = (req, res, next) => {

// }

exports.findUserProducts = (req, res, next) => {
    Product.find({owner: req.params.id})
        .populate("owner", "username sectors avatar")
        .then(products => res.json(performData(products)))
        .catch(err => res.status(500).json(err))
};

exports.findByIdAndUpdate = (req, res, next) => {
    let query = {};
    if(req.body.name) {
        query.name = req.body.name;
    }
    if(req.body.content) {
        query.content = req.body.content;
    }
    if(req.body.price) {
        query.price = req.body.price;
    }
    if(req.body.max_weight) {
        query.max_weight = req.body.max_weight;
    }
    if(req.body.currency) {
        query.currency = req.body.currency;
    }
    if(req.body.quantity) {
        query.quantity = req.body.quantity;
    }
    if(req.body.location) {
        query.location = req.body.location;
    }
    if(req.body.status) {
        query.status = req.body.status;
    }
    if(req.body.product_category) {
        query.product_category = req.body.product_category;
    }
    if(req.body.purchase) {
        query.purchase = req.body.purchase;
    }
    Product.findByIdAndUpdate(req.params.id, query, { new: true, useFindAndModify: false })
        .populate("owner", "username sectors avatar")
        .then(product => {
            // res.json(product);
            console.log("product updated successfuly");
            next();
        })
        .catch(err => res.status(500).json(err))
};

function performData(product) {
    if(product.length) {
        return product.map(elt => {
            if(elt.creator && elt.creator.avatar) {
                elt.creator.avatar = `${STATIC_URL}/static${elt.creator.avatar.split('/static')[0]}`
            }
            if(elt.files && elt.files.length) {
                elt.files.forEach(file => {
                    file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
                })
            }
            if(elt.publication && elt.publication.files && elt.publication.files.length) {
                elt.publication.files.forEach(file => {
                    file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
                })
            }
            if(elt.product && elt.product.files) {
                elt.product.files.forEach(file => {
                    file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
                })
            }
            if(elt.creator && elt.creator.avatar) {
                
            }
            return elt;
        })
    }
    if(product.creator && product.creator.avatar) {
        product.creator.avatar = `${STATIC_URL}/static${product.creator.avatar.split('/static')[0]}`
    }
    if(product.files && product.files.length) {
        product.files.forEach(file => {
            file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
        })
    }
    if(product.publication && product.publication.files) {
        product.publication.files.forEach(file => {
            file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
        })
    }
    if(product.product && product.product.files) {
        product.product.files.forEach(file => {
            file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
        })
    }
    return product;
}
