const { Contact, Work, Make }  = require('../models/contact');
const { STATIC_URL } = require('../middlewares/api_url');

exports.create = (req, res, next) => {
    contact = new Contact({
        _userId: req.body._userId,
        subject: req.body.subject,
        message: req.body.message
    });
console.log(req.body);

    contact.save()
        .then(contact => {
        	res.json(contact);
        })
        .catch(err => res.status(500).json(err));
};


exports.getAll = (req, res, next) => {
    Contact.find({})
        .sort({createAt: -1})
        .then(contact => res.json(contact))
        .catch(err => res.status(500).json(err));
}

 


exports.createDeliveryWork = (req, res, next)=>{
    let work = new Work({
       speciality: req.body.speciality,
       other_speciality: req.body.other_speciality,
       message: req.body.message,
       _userId: req.body.user_id
    }); 

    console.log(req.body);console.log(req.files);

     if(req.files.length){
        req.files.forEach(element => {
            if(element.mimetype.match('image/*')){
                work.image.push({
                    url: STATIC_URL + '/static/images/uploads/' + element.filename,
                    // url: API_URL + 'static/img/uploads/' + element.filename,
                    contentType: element.mimetype
                });
            }
        });
    }
    
     work.save()
        .then(work => {
            work.populate('_userId', (err, data)=>{
                if(err) throw err
                res.json(data);
            });
        })
        .catch(err => res.status(500).json(err));
 
}


exports.getAllWork = (req, res, next) => {
    Work.find({})
        .sort({createAt: -1})
        .populate('_userId')
        .then(topten => res.json(topten))
        .catch(err => res.status(500).json(err));
}




exports.createDeliveryMake = (req, res, next)=>{
    let make = new Make({
       delivery_time: req.body.delivery_time,
       map_link: req.body.map_link,
       perishable: req.body.perishable,
       optradio: req.body.optradio,
       code: req.body.code,
       delivery_adresse: req.body.delivery_adresse,
       _userId: req.body.user_id
    }); 
    console.log(req.files);
    console.log(req.body);

    if(req.files.length){
        req.files.forEach(element => {
            if(element.mimetype.match('image/*')){
                make.image.push({
                    url: STATIC_URL + '/static/images/uploads/' + element.filename,
                    // url: API_URL + 'static/img/uploads/' + element.filename,
                    contentType: element.mimetype
                });
            }
        });
    }

     make.save()
        .then(make => {
            make.populate('_userId', (err, data)=>{
                if(err) throw err
                res.json(data);
            });
        })
        .catch(err => res.status(500).json(err));
}

  



exports.getAllMake = (req, res, next) => {
    Make.find({})
        .sort({createAt: -1})
        .populate('_userId')
        .then(topten => res.json(topten))
        .catch(err => res.status(500).json(err));
}