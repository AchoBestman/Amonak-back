const {Like, CommentLike, PublicationLike} = require('../models/like');

//Create like
exports.create = (req, res, next) =>{
    let newLike;
    let query;
    let LikeModel;
    if(req.body.type === 'publication'){
        LikeModel = PublicationLike;
        query = {creator: req.body.creator, publication: req.body.publication};
        newLike = new PublicationLike({
            creator: req.body.creator,
            publication: req.body.publication
        });
    }else{
        LikeModel = CommentLike;
        query = {creator: req.body.creator, comment: req.body.comment};
        newLike = new CommentLike({
            creator: req.body.creator,
            comment: req.body.comment
        });
    }
    LikeModel.find(query)
        .then(like => {
            if(like.length){
                res.json({message: 'already liked'});
                return
            }else{
                newLike.save()
                    .then(like => {
                        req.body.like = like;
                        req.body.msg = "like";
                        res.json(like);
                        next();
                    })
                    .catch(err => res.status(500).json(err));
            }
        })
        .catch(err => res.status(500).json(err));
};

//Find all Likes
exports.findAll = (req, res, next) => {
    Like.find()
        .populate('publication', '_id')
        .then(likes => res.json(likes))
        .catch(err => res.status(500).json(err));
};

//Find One
exports.findOne = (req, res, next) => {
    Like.findById(req.params.id)
        .then(likes => res.json(likes))
        .catch(err => res.status(500).json(err));
};

//Delete One
exports.delete = (req, res, next) => {
    let query = {};
    let LikeModel;
    if(req.body.publication){
        query = {creator: req.body.creator, publication: req.body.publication};
        LikeModel = PublicationLike;
    }else{
        query = {creator: req.body.creator, comment: req.body.comment};
        LikeModel = CommentLike;
    }
    LikeModel.findOneAndDelete(query)
        .then(like => {
            req.body.like = like;
            if(like){
                req.body.msg = "dislike";
                res.json(like);
                next();
            }else{
                res.json({message: 'error'});
            }
        })
        .catch(err => res.status(500).json(err));
};

//Delete all Likes
exports.deleteAll = (req, res, next) => {
    res.json({message: 'deleted'});
}; 

exports.deleteAllPub = (req, res, next) => {
    PublicationLike.deleteMany({publication: req.params.id})
        .then(likes => {
            console.log("2");
        })
        .catch(err => {
            console.log("2 - ");
            console.log(err);
            return
        });
    console.log(req.body.comments);
    CommentLike.deleteMany({comment: { $in: req.body.comments }})
        .then(likes => console.log("3"))
        .catch(err => {
            console.log("3 - ");
            console.log(err);
            return;
        });
    
    next();
};

exports.deleteAllComments = (req, res, next) => {
    console.log(req.body);
}