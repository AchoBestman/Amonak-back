const User = require('../models/user');
const jwt = require('jsonwebtoken');
const config = require('../config/auth.config');
const { STATIC_URL } = require('../middlewares/api_url');

exports.find = (req, res, next) => {
    User.find({ status: true }, "-__v -status -password -createdAt -updatedAt")
        .sort({ "createdAt": -1 })
        .populate("seller_info", "-__v -_id")
        .then(users => {
            return res.json(performData(users));
    })
        .catch(err => { 
            return res.status(500).json(err);
        });
};

exports.findById = (req, res, next) => {
    User.findOne({_id: req.params.id, status: true }, "-__v -status -password -createdAt -updatedAt")
        .populate("seller_info", "-__v -_id")
        .populate("biography", "-__v")
        .then(user => {
            if(!user) {
                return res.status(401).json({message: "User not found."});
            }
            return res.json(performData(user));
        })
        .catch(err => {
            return res.status(500).json(err);
        });
};

exports.findByName = (req, res, next) => {
    User.findOne({ username: req.body.username, status: true }, "-__v -status -password -createdAt -updatedAt")
        .populate("seller_info", "-__v -_id")
        .then(user => {
            if(!user) {
                return res.status(401).json({message: "User not found."});
            }
            return res.json(performData(user));
        })
        .catch(err => {
            return res.status(500).json(err);
        });
};

exports.findByEmail = (req, res, next) => {
    User.findOne({email: req.body.email, status: true }, "-__v -status -password -createdAt -updatedAt")
        .populate("seller_info", "-__v -_id")
        .then(user => {
            if(!user) {
                return res.status(401).json({message: "User not found."});
            }
            return res.json(performData(user));
        })
        .catch(err => {
            return res.status(500).json(err);
        });
};

exports.friendRequest = (re, res, next) => {
    const user_id = req.body.user_id;
    const user_request = req.body.user_request;
    const friend_id = req.body.friend_id;
    const friend_request = req.body.friend_request;
    const query1 = { status: true, $push: { friends: user_request } };
    const query2 = { status: true, $push: { friends: friend_request } };
    User.findByIdAndUpdate(user_id, query1, {new: true, useFindAndModify: false})
        .then(user => {
            User.findByIdAndUpdate(friend_id, query2, {new: true, useFindAndModify: false})
                .then(user => console.log(user))
                .catch(err => {
                    res.status(500).json(err);
                    return 0;
                })
        })
        .catch(err => {
            res.status(500).json(err);
            return 0;
        })
}

exports.rejectFriendRequest = (req, res, next) => {
    const user_id = req.body.user_id;
    const user_request = req.body.user_request;
    const friend_id = req.body.friend_id;
    const friend_request = req.body.friend_request;
    const query1 = { status: true, $pull: { friends: user_request } };
    const query2 = { status: true, $pull: { friends: friend_request } };
    User.findByIdAndUpdate(user_id, query1, {new: true, useFindAndModify: false})
        .then(user => {
            User.findByIdAndUpdate(friend_id, query2, {new: true, useFindAndModify: false})
                .then(user => console.log(user))
                .catch(err => {
                    res.status(500).json(err);
                    return 0;
                })
        })
        .catch(err => {
            res.status(500).json(err);
            return 0;
        })
}

exports.updateUsername = (req, res, next) => {
    const username = req.body.username;
    const id = req.params.id;
    const query = { username: username };
    User.findOneAndUpdate({ _id: id }, query, { new: true, useFindAndModify: false })
        .then(user => res.json(user))
        .catch(err => res.status(500).json(err));
}

exports.updateAvatar = (req, res, next) => {
    console.log(":::::::::::::::");
    console.log(req.body);
    console.log(req.file);
    // const avatar = `${STATIC_URL}/images/avatar/${req.body.file.name}`;
    const id = req.params.id;
    let avatar = '';
    if (req.file && req.file.filename) {
        avatar = `${STATIC_URL}/static/images/avatar/${req.file.filename}`;
    } else  {
        avatar = `${STATIC_URL}/static/images/avatar/${req.body.file}`;
    }
    const query = { avatar: avatar };
    console.log(query);
    User.findOneAndUpdate({ _id: id }, query, { new: true, useFindAndModify: false })
        .populate("seller_info", "-__v -_id")
        .then(user => {
            console.log(performData(user));
            res.json(performData(user));
        })
        .catch(err => res.status(500).json(err));
}

exports.updateEmail = (req, res, next) => {
    const email = req.body.email;
    const id = req.params.id;
    const query = { email: email };
    User.findOneAndUpdate({ _id: id }, query, { new: true, useFindAndModify: false })
        .then(user => res.json(user))
        .catch(err => res.status(500).json(err));
}

exports.updateGender = (req, res, next) => {
    const gender = req.body.gender;
    const id = req.params.id;
    const query = { gender: gender };
    User.findOneAndUpdate({ _id: id }, query, { new: true, useFindAndModify: false })
        .then(user => res.json(user))
        .catch(err => res.status(500).json(err));
}

exports.updateSectors = (req, res, next) => {
    const sectors = req.body.sectors;
    const id = req.params.id;
    const query = { sectors: sectors };
    User.findOneAndUpdate({ _id: id }, query, { new: true, useFindAndModify: false })
        .then(user => res.json(user))
        .catch(err => res.status(500).json(err));
}

exports.updateAddress = (req, res, next) => {
    const address = req.body.address;
    const id = req.params.id;
    const query = { address: address };
    User.findOneAndUpdate({ _id: id }, query, { new: true, useFindAndModify: false })
        .then(user => res.json(user))
        .catch(err => res.status(500).json(err));
}

exports.updateAccountType = (req, res, next) => {
    const account_type = req.body.account_type;
    const id = req.body.creator;
    const query = { account_type: account_type };
    User.findOneAndUpdate({ _id: id }, query, { new: true, useFindAndModify: false })
        .populate("seller_info", "-__v -_id")
        .then(user => {
            let token = jwt.sign({ id: user._id }, config.secret, {
                expiresIn: 259200
            });            
            return res.json({
                accessToken: token,
                user: performData(user),
                expiresIn: 259200
            });
        })
        .catch(err => res.status(500).json(err));
}

exports.updateProfile = (req, res, next) => {
    let query = {}; 
    let sectors = []
    if(req.body.username) {
        query.username = req.body.username;
    }
    if(req.body.lastname) {
        query.lastname = req.body.lastname;
    } 
    if(req.body.firstname) {
        query.firstname = req.body.firstname;
    } 
    if(req.body.gender) {
        query.gender = req.body.gender;
    } 
    if(req.body.address) {
        query.address = req.body.address;
    }
    if(req.body.email) {
        query.email = req.body.email;
    }

    if(req.body.birthday) {
        query.birthday = req.body.birthday; 
    } 
    if(req.body.birthday_place) {
        query.birthday_place = req.body.birthday_place;
    } 
    if(req.body.studies) {
        query.studies = req.body.studies;
    } 
    if(req.body.phone) {
        query.phone = req.body.phone;
    }
    if(req.body.web_sites) {
        query.web_sites = req.body.web_sites;
    }
    if(req.body.profession) {
        query.profession = req.body.profession;
    }
    if(req.body.languages) {
        query.languages = req.body.languages;
    }
    if(req.body.other_langue) {
        query.languages = req.body.other_langue;
    }

    if(req.body.sector1 && req.body.sector1.name) {
        sectors.push(req.body.sector1.name);
    }
    if(req.body.sector2 && req.body.sector2.name) {
        sectors.push(req.body.sector2.name);
    }

    if (sectors.length != 0) {
        query.sectors = sectors 
    }

    const id = req.params.id;
    User.findOneAndUpdate({ _id: id }, query, { new: true, useFindAndModify: false })
        .populate('seller_info', "-__v -_id")
        .then(user => {
            if(!user) {
                return res.json({message: "User not found."});
            }

            let token = jwt.sign({ id: user._id }, config.secret, {
                            expiresIn: 259200
                        });

            res.json({
                accessToken: token,
                user: performData(user),
                user_country_infos: req.user_country_infos,
                expiresIn: 259200
            });

        })
        .catch(err => res.status(500).json(err));
}

exports.findByIdAndUpdate = (req, res, next) => {
    let query = {};
    if(req.body.firstname) {
        query.firstname = req.body.firstname;
    }
    if(req.body.lastname) {
        query.lastname = req.body.lastname;
    }
    if(req.body.username) {
        query.username = req.body.username;
    }
    if(req.body.email) {
        query.email = req.body.email;
    }
    if(req.body.gender) {
        query.gender = req.body.gender;
    }
    if(req.body.sectors) {
        query.sectors = req.body.sectors;
    }
    if(req.body.profession) {
        query.profession = req.body.profession;
    }
    if(req.body.address) {
        query.address = req.body.address;
    }
    if(req.file) {
        if(req.file.mimetype.match('image/*')) {
            query.avatar = `${STATIC_URL}/static/images/avatar/${req.file.filename}`;
        }
    }
    if(req.body.bank_card) {
        query.bank_card = req.body.bank_card;
    }
    if(req.body.status) {
        query.status = req.body.status;
    }
    if(req.body.phone) {
        query.phone = req.body.phone;
    }
    if(req.body.account_type) {
        query.account_type = req.body.account_type;
    }
    User.findByIdAndUpdate(req.params.id, query, { new: true, useFindAndModify: false })
        .populate("seller_info", "-__v -_id")
        .then(user => {
            if(!user) {
                return res.json({message: "User not found."});
            }
            res.json(performData(user));
        })
        .catch(error => {
            return res.status(500).json(error);
        });
};


exports.addBiography = (req, res, next) => {
    const biography = req.body.biography;
    const id = req.body.owner;
    const query = { biography: biography };
    User.findOneAndUpdate({ _id: id }, query, { new: true, useFindAndModify: false })
        .populate("seller_info", "-__v -_id")
        .populate('biography', '-__v -_id')
        .then(user => {
            return res.json(performData(user));
        })
        .catch(err => res.status(500).json(err));

}
exports.removeBiography = (req, res, next) => {
    const biography = req.body.biography;
    const id = req.body.owner;
    const query = { biography: biography };
    User.findByIdAndUpdate(id, query, { new: true, useFindAndModify: false })
        .then(user => console.log(performData(user)))
        .catch(err => res.status(500).json(err));
}

exports.addSellerInfo = (req, res , next) => {
    const id = req.body.creator;
    const account_type = req.body.account_type;
    const seller_info = req.body.seller_id;
    const query = { account_type: account_type, seller_info: seller_info };
    User.findByIdAndUpdate(id, query, { new: true, useFindAndModify: false })
        .populate('seller_info', "-__v -_id")
        .then(user => {
            let token = jwt.sign({ id: user._id }, config.secret, {
                expiresIn: 259200
            });            
            return res.json({
                accessToken: token,
                user: performData(user),
                expiresIn: 259200
            });
        })
        .catch(err => res.status(500).json(err));
}

exports.updateIsNewFeed = (req, res, next) => {
    const id = req.params.id;
    const query = {isNewFeed: false};
    User.findByIdAndUpdate(id, query, {new: true, useFindAndModify:false})
        .then(user => {
            next();
            // res.json(user.isNewFeed);
        })
        .catch(err => res.status(500).json(err));
}

exports.updateFirstTime = (req, res, next) => {
    const id = req.params.id;
    const query = {isFirstTime: false};
    User.findByIdAndUpdate(id, query, {new: true, useFindAndModify:false})
        .then(user => {
            // next();
            res.json(user.isFirstTime);
        })
        .catch(err => res.status(500).json(err));
}

function performData(user) {
	if(user.length) {
		return user.map(elt => {
			if(elt && elt.avatar) {
				elt.avatar = `${STATIC_URL}/static${elt.avatar.split('/static').length > 1 ? elt.avatar.split('/static')[1]: elt.avatar.split('/static')[0]}`;
            }
            if(elt.seller_info && elt.seller_info.identity_card && elt.seller_info.identity_card.url) {
                elt.seller_info.identity_card.url = `${STATIC_URL}/static${elt.seller_info.identity_card.url.split('/static').length > 1 ? elt.seller_info.identity_card.url.split('/static')[1]: elt.seller_info.identity_card.url.split('/static')[0]}`;
            }
            return elt;
        })
    }
	if(user && user.avatar) {
		user.avatar = `${STATIC_URL}/static${user.avatar.split('/static').length > 1 ? user.avatar.split('/static')[1]: user.avatar.split('/static')[0]}`;
	}
    if(user.seller_info && user.seller_info.identity_card && user.seller_info.identity_card.url) {
        user.seller_info.identity_card.url = `${STATIC_URL}/static${user.seller_info.identity_card.url.split('/static').length > 1 ? user.seller_info.identity_card.url.split('/static')[1]: user.seller_info.identity_card.url.split('/static')[0]}`;
    }
	return user;
}