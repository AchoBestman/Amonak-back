const { Notification, PublicationNotification, CommentNotification, InvitationNotification, WelcomeNotification } = require('../models/notification');
const pusher = require('../middlewares/pusher');
const authConfig = require('../config/auth.config');
const { STATIC_URL } = require('../middlewares/api_url');

exports.saveNotification = (req, res, next) => {
    const content = req.body.content? req.body.content : "";
    const sender = req.body.sender;
    const receiver = req.body.receiver;

    const notif = new InvitationNotification({
        content: content,
        sender: sender,
        receiver: receiver
    });

    notif.save()
        .then(notifCreated => {
            console.log(notifCreated);
            notifCreated.populate('sender', 'username sectors avatar is_log', (err, data) => {
                if(err) throw err
                res.io.emit('notification', performData(data));
                res.io.emit('invitation', performData(data));
            })
        })
        .catch(err => res.status(500).json(err));
}

exports.savePubNotif = (req, res, next) => {
    const sender = req.body.sender;
    const receivers = req.body.receivers;
    const content = req.body.content;
    const target = req.body.target;
    const notif = new PublicationNotification({
        sender: sender,
        receivers: receivers,
        content: content,
        target: target
    });
    notif.save()
        .then(notif => {
            console.log(notif);
            notif.populate({
                path: 'sender',
                select: 'username sectors avatar',
            }, (err, data) => {
                if(err) throw err
                console.log(notif);
                pusher.trigger('notification', 'new', {
                    data: data
                })
                // res.io.emit('notification', notif);
            });
        })
        .catch(err => res.status(500).json(err));
}

exports.saveCommentNotif = (req, res, next) => {
    const notif = new PublicationNotification({
        sender: req.body.user_id,
        receiver: req.body.receiver,
        content: "publicationComment",
        receivers: req.body.receivers,
        target: req.body.target
    });

    notif.save()
        .then(notifCreated => {
            console.log(notifCreated);
            notifCreated
                .populate('target')
                .populate('sender', 'username sectors avatar is_log', (err, data) => {
                if(err) throw err
                res.io.emit('notification', performData(data));
            })
            
        })
        .catch(err => res.status(500).json(err));
}

exports.saveInvitationNotif = (req, res, next) => {
    const sender = req.body.user_id;
    const receivers = req.body.receivers;
    const content = req.body.content;
    const target = req.body.target;
    const notif = new Notification({
        sender: sender,
        receivers: receivers,
        content: content,
        target: target
    });
    notif.save()
        .then(notif => {
            console.log(notif);
            notif.populate({
                path: 'sender',
                select: 'username sectors avatar',
            }, (err, data) => {
                if(err) throw err
                console.log(notif);
                pusher.trigger('notification', 'new', {
                    data: data
                })
                // res.io.emit('notification', notif);
            });
        })
        .catch(err => res.status(500).json(err));
}
//
exports.saveWelcomeNotification = (req, res, next) => {
    const notif = new WelcomeNotification({
        sender: authConfig.teamAmonakId,
        receiver: req.userId,
        content: authConfig.welcomeMessage
    });

    notif.save()
        .then(notifCreated => console.log(notifCreated))
        .catch(err => res.status(500).json(err));
}
//
exports.saveLikeNotification = (req, res, next) => {
    const notif = new PublicationNotification({
        sender: req.body.user_id,
        receiver: req.body.receiver,
        content: "publicationLike",
        receivers: req.body.receivers,
        target: req.body.target
    });

    notif.save()
        .then(notifCreated => {
            console.log(notifCreated);
            notifCreated
                .populate('target')
                .populate('sender', 'username sectors avatar is_log', (err, data) => {
                if(err) throw err
                res.io.emit('notification', performData(data));
            })
            
        })
        .catch(err => res.status(500).json(err));
}
exports.saveShareNotification = (req, res, next) => {
    const notif = new PublicationNotification({
        sender: req.body.user_id,
        receiver: req.body.receiver,
        content: "publicationShare",
        receivers: req.body.receivers,
        target: req.body.target
    });

    notif.save()
        .then(notifCreated => {
            console.log(notifCreated);
            notifCreated
                .populate('target')
                .populate('sender', 'username sectors avatar is_log', (err, data) => {
                if(err) throw err
                res.io.emit('notification', performData(data));
            })
            
        })
        .catch(err => res.status(500).json(err));
}

exports.findAndUpdate = (req, res, next) => {
    const id = req.params.id;
    const user_id = req.body.user_id;
    const query = { $push: { read_by: {user: user_id } } };
    Notification.findByIdAndUpdate(id, query, { new: true, useFindAndModify: false })
        .then(notif => {
            console.log(notif);
            res.json(notif);
        })
        .catch(err => res.status(500).json(err));
}

exports.find = (req, res, next) => {
    Notification.find({ status: true })
        .sort({ createdAt: -1 })
        .populate('sender', 'username sectors avatar is_log')
        .populate('receivers', 'username sectors avatar')
        .populate('read_by.user', 'username sectors avatar')
        .populate('target')
        .then(notifs => res.json(performData(notifs)))
        .catch(err => res.status(500).json(err));
}

exports.findUserNotifs = (req, res, next) => {
    const user_id = req.params.user_id;
    const query = { $or: [{receivers: user_id}, {receiver: user_id}], status: true };
    Notification.find(query, '-__v -status -updatedAt')
        .sort({ createdAt: -1 })
        .populate('sender', 'username sectors avatar is_log')
        .populate('receivers', 'username sectors avatar')
        .populate('read_by.user', 'username sectors avatar')
        .populate('target')
        .then(notifs => res.json(performData(notifs)))
        .catch(err => res.status(500).json(err));
}

exports.findUserUnreadNotifs = (req, res, next) => {
    const user_id = req.params.user_id;
    const query = { receivers: user_id, $nin: { read_by: { user_id } } };
    Notification.find(query)
        .sort({ createdAt: -1 })
        .populate('sender', 'username sectors avatar is_log')
        .populate('receivers', 'username sectors avatar')
        .populate('read_by.user', 'username sectors avatar')
        .populate('target')
        .then(notifs => res.json(performData(notifs)))
        .catch(err => res.status(500).json(err));
}

exports.delete = (req, res, next) => {
    const id = req.params.id;
    const query = { status: false };
    Notification.findByIdAndUpdate(id, query, { new: true, useFindAndModify: false})
        .then(notif => res.json(notif))
        .catch(err => res.status(500).json(err));
}

function performData(notification) {
    if(notification.length) {
      return notification.map(elt => {
        if(elt.sender && elt.sender.avatar) {
          elt.sender.avatar = `${STATIC_URL}/static${elt.sender.avatar.split('/static').length > 1 ? elt.sender.avatar.split('/static')[1]: elt.sender.avatar.split('/static')[0]}`;
        }
        if(elt.receiver && elt.receiver.avatar) {
          elt.receiver.avatar = `${STATIC_URL}/static${elt.receiver.avatar.split('/static').length > 1 ? elt.receiver.avatar.split('/static')[1]: elt.receiver.avatar.split('/static')[0]}`;
        }
        if(elt.receivers && elt.receivers.length) {
            elt.receivers.forEach(r => {
                if(r && r.avatar) {
                    r.avatar = `${STATIC_URL}/static${r.avatar.split('/static').length > 1 ? r.avatar.split('/static')[1]: r.avatar.split('/static')[0]}`;
                }
            })
        }
        if(elt.readBy && elt.readBy.length) {
            elt.readBy.forEach(r => {
                if(r && r.user && r.user.avatar) {
                    r.avatar = `${STATIC_URL}/static${r.avatar.split('/static').length > 1 ? r.avatar.split('/static')[1]: r.avatar.split('/static')[0]}`;
                }
            })
        }
        if(elt.seenBy && elt.seenBy.length) {
            elt.seenBy.forEach(r => {
                if(r && r.user && r.user.avatar) {
                    r.avatar = `${STATIC_URL}/static${r.avatar.split('/static').length > 1 ? r.avatar.split('/static')[1]: r.avatar.split('/static')[0]}`;
                }
            })
        }
        return elt;
      })
    }
    if(notification.sender && notification.sender.avatar) {
      notification.sender.avatar = `${STATIC_URL}/static${notification.sender.avatar.split('/static').length > 1 ? notification.sender.avatar.split('/static')[1]: notification.sender.avatar.split('/static')[0]}`;
    }
    if(notification.receiver && notification.receiver.avatar) {
      notification.receiver.avatar = `${STATIC_URL}/static${notification.receiver.avatar.split('/static').length > 1 ? notification.receiver.avatar.split('/static')[1]: notification.receiver.avatar.split('/static')[0]}`;
    }
    if(notification.receivers && notification.receivers.length) {
        notification.receivers.forEach(r => {
            if(r && r.avatar) {
                r.avatar = `${STATIC_URL}/static${r.avatar.split('/static').length > 1 ? r.avatar.split('/static')[1]: r.avatar.split('/static')[0]}`;
            }
        })
    }
    if(notification.readBy && notification.readBy.length) {
        notification.readBy.forEach(r => {
            if(r && r.user && r.user.avatar) {
                r.avatar = `${STATIC_URL}/static${r.avatar.split('/static').length > 1 ? r.avatar.split('/static')[1]: r.avatar.split('/static')[0]}`;
            }
        })
    }
    if(notification.seenBy && notification.seenBy.length) {
        notification.seenBy.forEach(r => {
            if(r && r.user && r.user.avatar) {
                r.avatar = `${STATIC_URL}/static${r.avatar.split('/static').length > 1 ? r.avatar.split('/static')[1]: r.avatar.split('/static')[0]}`;
            }
        })
    }
    return notification;
  }