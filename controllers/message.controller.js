const Message = require('../models/message');
const { STATIC_URL } = require('../middlewares/api_url');
const pusher = require('../middlewares/pusher');


exports.save = (req, res, next) => {
    let new_message = new Message({
        content: req.body.content,
        from: req.body.from,
        to: req.body.to,
    });
    if(req.body.files){
        let files = req.body.files;
        files.forEach(element => {
            if(element.type.match('image/*')){
                new_message.files.push({
                    // url: `${STATIC_URL}/static/images/uploads/${element.name}`,
                    url: `images/uploads/${element.name}`,
                    type: element.type
                });
            } else if(element.type.match('video/*')) {
                new_message.files.push({
                    // url: `${STATIC_URL}/static/videos/uploads/${element.name}`,
                    url: `videos/uploads/${element.name}`,
                    type: element.type
                });
            }
        });
    }
    new_message.save()
        .then(message => {
            res.json(performData(message));
        })
        .catch(err => res.status(500).json(err));
};

exports.find = (req, res, next) => {
    Message.find()
        .populate('from', 'username avatar sectors is_log')
        .populate('to', 'username avatar sectors is_log')
        .select('content to from created_at status')
        .sort({created_at: -1})
        .then(messages => res.json(performData(messages)))
        .catch(err => res.status(500).json(err));
}

exports.findUserDiscussion = (req, res, next) => {
    const friend_id = req.query.friend;
    const user_id = req.params.user_id;
    console.log(user_id);
    let query;
    if(friend_id) {
        query = { $or: [{ from: user_id, to: friend_id }, { from: friend_id, to: user_id }], status: true };
    } else {
        query = { $or: [{ from: user_id }, { to: user_id }], status: true };
    }
    Message.find(query)
        .populate('from', 'username avatar sectors is_log')
        .populate('to', 'username avatar sectors is_log')
        .select('content from created_at read_at reply files')
        .sort({created_at: -1})
        .then(discussions => {
            if(!discussions.length) {
                res.json(discussions);
                return 0;
            }
            let data = [];
            discussions.forEach((elt, index) => {
                if(elt.from._id.toString() === user_id) {
                    discussions[index] = {'from': elt.from, 'to': elt.to, 'files': elt.files, 'content': elt.content, 'user': elt.to, 'created_at': elt.created_at, 'status': elt.read_at, 'reply': true, 'nbMsg': 0, 'is_log': elt.to.is_log, '_id': elt._id, "state": elt.status, 'deleters': elt.deleters };
                } else {
                    discussions[index] = {'from': elt.from, 'to': elt.to, 'files': elt.files, 'content': elt.content, 'user': elt.from, 'created_at': elt.created_at, 'status': elt.read_at, 'reply': false, 'nbMsg': 0, 'is_log': elt.from.is_log, '_id': elt._id, "state": elt.status, 'deleters': elt.deleters };
                }
            });
            data.push(discussions[0]);
            discussions.forEach(elt => {
                if(data.findIndex(d => d.user._id.toString() === elt.user._id.toString()) < 0) {
                    data.push(elt);
                }
            });
            // console.log(data);
            res.json(performData(data));
        })
        .catch(err => res.status(500).json(err));
}

exports.findOneAndUpdate = (req, res, next) => {
    Message.findOneAndUpdate({_id: req.params.id}, {read_at: Date.now()}, { new: true, useFindAndModify: false })
        .then(message => res.json(performData(message)))
        .catch(err => res.status(500).json(err));
};

exports.getDiscussionList = (req, res, next) => {
    Message.find({ $or: [{ from: req.params.id }, { to: req.params.id }] })
        .populate('from', 'username avatar sectors is_log')
        .populate('to', 'username avatar sectors is_log')
        .select('content to from created_at status')
        .sort({created_at: -1})
        .then(discussions => {
            let data = [];
            discussions.forEach((elt, index) => {
                if(elt.from._id.toString() === req.params.id) {
                    discussions[index] = {'from': elt.from, 'to': elt.to, 'content': elt.content, 'user': elt.to, 'created_at': elt.created_at, 'status': elt.read_at, 'reply': true, 'nbMsg': 0, 'is_log': elt.to.is_log, '_id': elt._id, "state": elt.status, 'deleters': elt.deleters };
                } else {
                    discussions[index] = {'from': elt.from, 'to': elt.to, 'content': elt.content, 'user': elt.from, 'created_at': elt.created_at, 'status': elt.read_at, 'reply': false, 'nbMsg': 0, 'is_log': elt.from.is_log, '_id': elt._id, "state": elt.status, 'deleters': elt.deleters };
                }
            });
            data.push(discussions[0]);
            discussions.forEach(elt => {
                if(data.findIndex(d => d.user._id.toString() === elt.user._id.toString()) < 0) {
                    data.push(elt);
                }
            });
            res.json(data);
        })
        .catch(err => res.status(500).json(err));
}

exports.sendReply = (req, res, next) => {
    let message = new Message({
        from: req.body.creator,
        to: req.body.destinateur,
        content: req.body.content,
        type: 'AlertReply'
    });
    if(req.body.data) {
        message.files = req.body.data;
    }
    message.save()
        .then(message => {
            message.populate('from', 'username sectors avatar is_log')
                .populate('to', 'username sectors avatar is_log', (err, data) => {
                let msg = {};
                msg.files = data.files;
                msg.user = data.to;
                msg.content = data.content;
                msg.created_at = data.created_at;
                msg.status = data.read_at;
                msg.reply = false;
                msg.is_log = data.is_log;
                msg.from = data.from;
                msg.to = data.to;
                msg._id = data._id;
                msg.state = data.status;
                msg.deleters = data.deleters;
                // console.log(msg);
                pusher.trigger('message', 'new', {
                    data: msg
                });
            })
            req.body.sender = req.body.creator;
            req.body.receivers = req.body.destinateur;
            req.body.content = "alerteResponse";
            req.body.target = req.body.publication;
            next();
        })
        .catch(err => res.status(500).json(err));
}

exports.deleteMessage = (req, res, next) => {
    const _id = req.params.id;
    const user = req.query.user;
    if(!user) {
        return res.statut(500).json({"error": "error"});
    }
    const query = { $push: { deleters: user} };
    Message.findByIdAndUpdate(_id, query, {new: true, useFindAndModify: false})
        .populate('to', 'username sectors avatar is_log')
        .populate('from', 'username sectors avatar is_log')
        .then((data) => {
            let msg = {};
            msg.files = data.files;
            msg.user = data.to;
            msg.content = data.content;
            msg.created_at = data.created_at;
            msg.status = data.read_at;
            msg.reply = false;
            msg.is_log = data.is_log;
            msg.from = data.from;
            msg.to = data.to;
            msg._id = data._id;
            msg.state = data.status;
            msg.deleters = data.deleters;
            res.json(performData(msg));
        })
        .catch(err => res.status(500).json(err));
}

function performData(message) {
    if(message.length) {
        return message.map(elt => {
            if(elt.from && elt.from.avatar) {
                elt.from.avatar = `${STATIC_URL}/static${elt.from.avatar.split('/static').length > 1 ? elt.from.avatar.split('/static')[1]: elt.from.avatar.split('/static')[0]}`;
            }
            if(elt.to && elt.to.avatar) {
                elt.to.avatar = `${STATIC_URL}/static${elt.to.avatar.split('/static').length > 1 ? elt.to.avatar.split('/static')[1]: elt.to.avatar.split('/static')[0]}`;
            }
            if(elt.user && elt.user.avatar) {
                elt.user.avatar = `${STATIC_URL}/static${elt.user.avatar.split('/static').length > 1 ? elt.user.avatar.split('/static')[1]: elt.user.avatar.split('/static')[0]}`;
            }
            if(elt.files && elt.files.length) {
                elt.files.forEach(file => {
                    file.url = `${STATIC_URL}/static${file.url.split('/static').length > 1 ? file.url.split('/static')[1]: file.url.split('/static')[0]}`;
                })
            }
            return elt;
        })
    }
    if(message.from && message.from.avatar) {
        message.from.avatar = `${STATIC_URL}/static${message.from.avatar.split('/static').length > 1 ? message.from.avatar.split('/static')[1]: message.from.avatar.split('/static')[0]}`
    }
    if(message.to && message.to.avatar) {
        message.to.avatar = `${STATIC_URL}/static${message.to.avatar.split('/static').length > 1 ? message.to.avatar.split('/static')[1]: message.to.avatar.split('/static')[0]}`
    }
    if(message.user && message.user.avatar) {
        message.user.avatar = `${STATIC_URL}/static${message.user.avatar.split('/static').length > 1 ? message.user.avatar.split('/static')[1]: message.user.avatar.split('/static')[0]}`;
    }
    if(message.files && message.files.length) {
        message.files.forEach(file => {
            file.url = `${STATIC_URL}/static${file.url.split('/static').length > 1 ? file.url.split('/static')[1]: file.url.split('/static')[0]}`;
        })
    }
    return message;
}