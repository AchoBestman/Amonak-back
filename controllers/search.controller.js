const User = require('../models/user');
const { Publication } = require('../models/publication');
const Product = require('../models/product');
const { STATIC_URL } = require('../middlewares/api_url');

exports.search = (req, res, next) => {
    console.log(req.params.search);
    User.find({ status: true, username: {$regex: new RegExp(req.params.search, 'i')}}, 'sectors username avatar')
        .then(users => {
            res.json(performData(users));
        })
        .catch(err => res.status(500).json(err));
};

exports.searchUser = (req, res, next) => {
    console.log(req.params.search);
    User.find({ status: true, username: {$regex: new RegExp(req.params.search, 'i')}}, 'sectors username avatar')
        .then(users => {
            res.json(performData(users));
        })
        .catch(err => res.status(500).json(err));
};

exports.searchPublication = (req, res, next) => {
    console.log(req.params.search);
    Publication.find({status: true, content: {$regex: new RegExp(req.params.search, 'i')}})
        .sort({createdAt: -1})
        .populate({
            path: 'publication',
            populate: { path: 'creator', select: 'sectors avatar username' },
        })
        .populate('product', 'price max_weight currency')
        .populate('creator', '_id username sectors avatar')
        .populate({
            path: 'comments',
            populate: {path: 'creator', select: 'sectors avatar username'},
            select: 'likes creator content'})
        .populate({
            path: 'comments',
            populate: {path: 'likes.creator', select: 'sectors avatar username'}
        })
        .then(publications => res.json(performData(publications)))
        .catch(err => res.status(500).json(err));
};

exports.searchProduct = (req, res, next) => {
    console.log(req.params.search);
    Product.find({$or: [{name: {$regex: new RegExp(req.params.search, 'i')}}, {content: {$regex: new RegExp(req.params.search, 'i')}}]})
        .populate('owner', 'username sectors avatar')
        .then(products => res.json(performData(products)))
        .catch(err => res.status(500).json(err));

};

function performData(data) {
    if(data.length) {
        return data.map(elt => {
            if(elt && elt.avatar) {
                elt.avatar = `${STATIC_URL}/static${elt.avatar.split('/static').length > 1 ? elt.avatar.split('/static')[1]: elt.creator.avatar.split('/static')[0]}`;
            }
            if(elt.creator && elt.creator.avatar) {
                elt.creator.avatar = `${STATIC_URL}/static${elt.creator.avatar.split('/static').length > 1 ? elt.creator.avatar.split('/static')[1]: elt.creator.avatar.split('/static')[0]}`;
            }
            if(elt.owner && elt.owner.avatar) {
                elt.owner.avatar = `${STATIC_URL}/static${elt.owner.avatar.split('/static').length > 1 ? elt.owner.avatar.split('/static')[1]: elt.owner.avatar.split('/static')[0]}`;
            }
            if(elt.files && elt.files.length) {
                elt.files.forEach(file => {
                    file.url = `${STATIC_URL}/static${file.url.split('/static').length > 1 ? file.url.split('/static')[1]: file.url.split('/static')[0]}`;
                })
            }
            if(elt.publication && elt.publication.files && elt.publication.files.length) {
                elt.publication.files.forEach(file => {
                    file.url = `${STATIC_URL}/static${file.url.split('/static').length > 1 ? file.url.split('/static')[1]: file.url.split('/static')[0]}`;
                })
            }
            if(elt.product && elt.product.files) {
                elt.product.files.forEach(file => {
                    file.url = `${STATIC_URL}/static${file.url.split('/static').length > 1 ? file.url.split('/static')[1]: file.url.split('/static')[0]}`;
                })
            }
            if(elt.comments && elt.comments.length) {
                elt.comments.forEach(com => {
                    if(com.creator && com.creator.avatar) {
                        com.creator.avatar = `${STATIC_URL}/static${com.creator.avatar.split('/static').length > 1 ? com.creator.avatar.split('/static')[1]: com.creator.avatar.split('/static')[0]}`;
                    }
                })
            }
            if(elt.likes && elt.likes.length) {
                elt.likes.forEach(like => {
                    if(like.creator && like.creator.avatar) {
                        like.creator.avatar = `${STATIC_URL}/static${like.creator.avatar.split('/static').length > 1 ? like.creator.avatar.split('/static')[1]: like.creator.avatar.split('/static')[0]}`;
                    }
                })
            }
            return elt;
        })
    }
    if(data && data.avatar) {
        data.avatar = `${STATIC_URL}/static${data.avatar.split('/static').length > 1 ? data.avatar.split('/static')[1]: data.creator.avatar.split('/static')[0]}`;
    }
    if(data.creator && data.creator.avatar) {
        data.creator.avatar = `${STATIC_URL}/static${data.creator.avatar.split('/static').length > 1 ? data.creator.avatar.split('/static')[1]: data.creator.avatar.split('/static')[0]}`
    }
    if(data.owner && data.owner.avatar) {
        data.owner.avatar = `${STATIC_URL}/static${data.owner.avatar.split('/static').length > 1 ? data.owner.avatar.split('/static')[1]: data.owner.avatar.split('/static')[0]}`
    }
    if(data.files && data.files.length) {
        data.files.forEach(file => {
            file.url = `${STATIC_URL}/static${file.url.split('/static').length > 1 ? file.url.split('/static')[1]: file.url.split('/static')[0]}`;
        })
    }
    if(data.publication && data.publication.files) {
        data.publication.files.forEach(file => {
            file.url = `${STATIC_URL}/static${file.url.split('/static').length > 1 ? file.url.split('/static')[1]: file.url.split('/static')[0]}`;
        })
    }
    if(data.product && data.product.files) {
        data.product.files.forEach(file => {
            file.url = `${STATIC_URL}/static${file.url.split('/static').length > 1 ? file.url.split('/static')[1]: file.url.split('/static')[0]}`;
        })
    }
    return data;
}
