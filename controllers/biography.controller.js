const Biography = require('../models/biography');

exports.save = (req, res, next) => {
    let doc = {};
    if(req.body.firstname) {
        doc.firstname = req.body.firstname;
    }
    if(req.body.lastname) {
        doc.lastname = req.body.lastname;
    }
    if(req.body.birthday) {
        doc.birthday = req.body.birthday;
    }
    if(req.body.birthday_place) {
        doc.birthday_place = req.body.birthday_place;
    }
    if(req.body.relationship_status) {
        doc.relationship_status = req.body.relationship_status;
    }
    if(req.body.family_members) {
        doc.family_members = req.body.family_members;
    }
    if(req.body.nickname) {
        doc.nickname = req.body.nickname;
    }
    if(req.body.interested_by) {
        doc.interested_by = req.body.interested_by;
    }
    if(req.body.politics) {
        doc.politics = req.body.politics;
    }
    if(req.body.confessions) {
        doc.confessions = req.body.confessions;
    }
    if(req.body.languages) {
        doc.languages = req.body.languages;
    }
    if(req.body.web_sites) {
        doc.web_sites = req.body.web_sites;
    }
    if(req.body.networks) {
        doc.networks = req.body.networks;
    }
    if(req.body.residences) {
        doc.residences = req.body.residences;
    }
    if(req.body.studies) {
        doc.studies = req.body.studies;
    }
    if(req.body.jobs) {
        doc.jobs = req.body.jobs;
    }
    if(req.body.owner) {
        doc.owner = req.body.owner;
    }
    const new_biography = new Biography(doc);
    new_biography.save()
        .then(biography => {
            console.log(biography);
            req.body.biography = biography._id;
            res.json(biography);
            next();
        })
        .catch(err => res.status(500).json(err));
}
exports.find = (req, res, next) => {
    Biography.find()
        .then(biographies => res.json(biographies))
        .catch(err => res.status(500).json(err));
}

exports.findById = (req, res, next) => {
    Biography.findById(req.params.id)
        .then(biographies => res.json(biographies))
        .catch(err => res.status(500).json(err));
}

exports.findUserBiography = (req, res, next) => {
    Biography.findOne({ owner: req.params.user_id })
        .then(biography => {
            if(!biography) {
                res.status(404).json({ message: "The user does not yet have a biography." });
                return
            }
            res.json(biography);
        })
        .catch(err => res.status(500).json(err))
}

exports.findByIdAndUpdate = (req, res, next) => {
    let query = {};
    if(req.body.firstname) {
        query.firstname = req.body.firstname;
    }
    if(req.body.lastname) {
        query.lastname = req.body.lastname;
    }
    if(req.body.birthday) {
        query.birthday = req.body.birthday;
    }
    if(req.body.birthday_place) {
        query.birthday_place = req.body.birthday_place;
    }
    if(req.body.relationship_status) {
        query.relationship_status = req.body.relationship_status;
    }
    if(req.body.family_members) {
        query.family_members = req.body.family_members;
    }
    if(req.body.nickname) {
        query.nickname = req.body.nickname;
    }
    if(req.body.interested_by) {
        query.interested_by = req.body.interested_by;
    }
    if(req.body.politics) {
        query.politics = req.body.politics;
    }
    if(req.body.confessions) {
        query.confessions = req.body.confessions;
    }
    if(req.body.languages) {
        query.languages = req.body.languages;
    }
    if(req.body.web_sites) {
        query.web_sites = req.body.web_sites;
    }
    if(req.body.networks) {
        query.networks = req.body.networks;
    }
    if(req.body.residences) {
        query.residences = req.body.residences;
    }
    if(req.body.studies) {
        query.studies = req.body.studies;
    }
    if(req.body.jobs) {
        query.jobs = req.body.jobs;
    }
    Biography.findByIdAndUpdate(req.params.id, query, { new: true, useFindAndModify: false })
        .then(biography => res.json("biography update successfuly."))
        .catch(err => res.status(500).json(err))
}

exports.delete = (req, res, next) => {
    let query = { status: 0 };
    Biography.findByIdAndUpdate(req.params.id, query, { new: true, useFindAndModify: false })
        .then(biography => {
            console.log(biography);
            req.body.biography = null;
            res.json(biography);
            next();
        })
        .catch(err => res.status(500).json(err));
}