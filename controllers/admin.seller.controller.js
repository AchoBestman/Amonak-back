const SellerInfo = require('../models/sellerInfo');


exports.getSellersList = (req, res, next) => {
    SellerInfo.find({status: 'accepted'})
        .sort({createdAt: -1})
        .populate('user', 'username photoUrl secteurs')
        .then(sellers => {
            res.json(performData(sellers));
        })
        .catch(err => res.status(500).json(err));
};

exports.getSellersRequest = (req, res, next) => {
    SellerInfo.find({status: 'pending'})
        .sort({createdAt: -1})
        .populate('owner', 'username photoUrl secteurs')
        .then(sellers => {
            res.json(performData(sellers));
        })
        .catch(err => res.status(500).json(err));

};

exports.getOneSeller = (req, res, next) => {
    SellerInfo.findById(req.params.id)
        .populate('user', 'username photoUrl secteurs')
        .then(seller => {
            if(!seller) {
                return res.status(404).json({ message: 'Seller not found' });
            }
            return res.status(201).json(performData(seller));
        })
        .catch(err => res.status(500).json(err));

};

exports.updateSeller = (req, res, next) => {
    SellerInfo.findByIdAndUpdate(req.params.id, {status: req.body.status}, { new: true, useFindAndModify: false })
        .then(seller => {
            res.json(performData(seller));
        })
        .catch(err => res.status(500).json(err));
};

exports.deleteOneSeller = (req, res, next) => {
    // this.updateSeller(req, res, next);
}

function performData(seller) {
    if(seller.length) {
        return seller.map(elt => {
            if(elt.creator && elt.creator.avatar) {
                elt.creator.avatar = `${STATIC_URL}/static${elt.creator.avatar.split('/static').length > 1 ? elt.creator.avatar.split('/static')[1]: elt.creator.avatar.split('/static')[0]}`;
            }
            if(elt.identity_card && elt.identity_card.url) {
                elt.identity_card.url = `${STATIC_URL}/static${elt.identity_card.url.split('/static').length > 1 ? elt.identity_card.url.split('/static')[1]: elt.identity_card.url.split('/static')[0]}`;
            }
            return elt;
        })
    }
    if(seller.creator && seller.creator.avatar) {
        seller.creator.avatar = `${STATIC_URL}/static${seller.creator.avatar.split('/static').length > 1 ? seller.creator.avatar.split('/static')[1]: seller.creator.avatar.split('/static')[0]}`;
    }
    if(seller.identity_card && seller.identity_card.url) {
        seller.identity_card.url = `${STATIC_URL}/static${seller.identity_card.url.split('/static').length > 1 ? seller.identity_card.url.split('/static')[1]: seller.identity_card.url.split('/static')[0]}`;
    }
    return seller;
}