"use strict"
const Amis = require('../models/amis');
const User = require('../models/user');
const pusher = require('../middlewares/pusher');
const Notification = require('../models/notification');
const { populate } = require('../models/user');
const { body } = require('express-validator');
const { STATIC_URL } = require('../middlewares/api_url');

exports.addAmi = function(req, res, next){
  const invitationSender = req.body.invitationSender;
  const invitationReceiver = req.body.invitationReceiver;
  console.log(invitationReceiver);
  console.log(invitationSender);
  const status = "pending";
  Amis.find({invitationSender: invitationSender, invitationReceiver: invitationReceiver})
    .then(amis => {
      if(amis.length){
        res.status(400).json('already friend');
      }else{
        const newAmi = new Amis({
          status,
          invitationSender,
          invitationReceiver,
        });
        // pusher.trigger('invitation', 'new', {
        //   destinator: invitationReceiver
        // });
        newAmi.save()
          .then(ami => {
            console.log(ami);
            // let notif = new Notification({
            //   creator: invitationSender,
            //   destinators: invitationReceiver,
            //   type: 'friend-request'
            // });
            // notif.save()
            //   .then(notification => {
            //     notification
            //       .populate('creator', 'username avatar', (err, data) => {
            //         if(err) throw err
            //         pusher.trigger('notification', 'new', {
            //             data: data
            //         });
            //         console.log(data);
            //       })
            //   })
            //   .catch(err => res.status(500).json(err));
            res.json(ami);
            req.body.content = "friend-request";
            req.body.sender = req.body.invitationSender;
            req.body.receiver = req.body.invitationReceiver;
            next();
          })
          .catch(err => res.status(400).json("error ami saved"));
      }
    });
}
//get users
exports.getAllUsers = function(req, res, next){
  let users = []
  users.push(req.params.id);
  Amis.find({$or: [{invitationSender: req.params.id}, {invitationReceiver: req.params.id}]})
    .then(amis => {
      amis.forEach(elt => {
        if(elt.invitationReceiver.toString() === req.params.id){
          users.push(elt.invitationSender);
        }else{
          users.push(elt.invitationReceiver);
        }
      })
      User.find({_id: {$nin: users}, status: true}, 'username sectors avatar')
        .then(users => {
          res.json(performData(users));
        })
        .catch(err => res.status(400).json(err));
    })
    .catch(err => res.status(400).json(err));
}

exports.getInvitations = function(req, res, next){
  Amis.find({status: 'pending', invitationReceiver: req.params.id})
    .select('invitationSender')
    .populate('invitationSender', 'username sectors avatar')
    .then(amis => {
      amis.forEach((ami, index) => {
        amis[index] = ami.invitationSender;
      });
      // console.log(amis);
      res.json(performData(amis));
    })
    .catch(err => res.status(500).json(err));
}

exports.getFriends = function(req, res, next){
  console.log(req.params.id);
  Amis.find({status: "accepted", $or: [{invitationSender: req.params.id}, {invitationReceiver: req.params.id}]})
    .populate('invitationSender', '_id username sectors avatar')
    .populate('invitationReceiver', '_id username sectors avatar')
    .then(amis => {
      amis.forEach((ami, index) => {
        // console.log(ami);
        if(ami.invitationSender === null) {
          amis.splice(index, 1);
        } else if(ami.invitationSender._id.toString() === req.params.id) {
          amis[index] = ami.invitationReceiver;
        } else {
          amis[index] = ami.invitationSender;
        }
      });
      res.json(performData(amis));
    })
    .catch(err => res.status(500).json(err));
}


exports.get = function(req, res, next){
  Amis.find()
    .populate('invitationSender', '_id username avatar')
    .populate('invitationReceiver', '_id username avatar')
    // .then(amis => res.json(amis))
    .then(amis => res.json(performData(amis)))
    .catch(err => res.status(400).json(err))
}

exports.getMobileSearch = (req, res, next) => {
  const user_id = req.params.id;
  const query = { $or: [{ invitationSender: user_id }, { invitationReceiver: user_id }] };
  Amis.find(query)
    .then(amis => {
      amis.forEach((elt, index) => {
        if(elt.invitationReceiver.toString() === user_id) {
          amis[index] = { 'user': elt.invitationSender, 'status': elt.status };
        } else {
          amis[index] = { 'user': elt.invitationReceiver, 'status': elt.status };
        }
      })
      res.json(amis);
    })
    .catch(err => res.status(500).json(err));
}

// exports.newAmis = function (req, res, next) {

//   Amis.find({ participants: { "$in": [req.body.userId, req.body.newAmisId] } })
//     .then(amis => {
//       if (amis.length) {
//         let error = 'friendship Exists in Database.';
//         return res.status(400).json(error);
//       } else {
//         var amis = new Amis({
//           participants: [req.body.userId, req.body.newAmisId],
//           status: "pending",
//           invitationSender: req.body.userId,
//           invitationReceiver: req.body.newAmisId

//         });

//         amis.save(function (err, newAmis) {
//           if (err) {
//             res.status(404).json({ error: err });

//           } else {
//             res.status(200).json({ message: 'frienship started!', Amis: newAmis });

//           }

//         });

//       }

//     })
// }
 
exports.updateStatus = function(req, res, next){
  const status = req.body.status;
  const invitationSender = req.body.invitationSender;
  const invitationReceiver = req.body.invitationReceiver;
  Amis.findOneAndUpdate({invitationSender: invitationSender, invitationReceiver: invitationReceiver}, {status: status}, { new: true, useFindAndModify: false })
    .then(ami => {
      if(status === "accepted") {
        // let notif = new Notification({
        //   creator: invitationReceiver,
        //   destinators: invitationSender,
        //   type: 'friend-response',
        // });
        // notif.save()
        //   .then(notification => {
        //     notification
        //       .populate('creator', 'username avatar', (err, data) => {
        //         if(err) throw err
        //         pusher.trigger('notification', 'new', {
        //             data: data
        //         });
        //         console.log(data);
        //       })
        //   })
        //   .catch(err => res.status(500).json(err));
      }
      res.json(ami);
    })
    .catch(err => res.status(500).json(err))
}

// exports.updateInvitationStatus = function (req, res, next) {
//   var amitieId=req.body.id 
//   Amis.findOneAndUpdate({ _id:amitieId }, {status:req.body.status}, { new: true })
//   .exec()
//   .then(result => {
//       res.status(200).json({
//           message: 'InvitationStatus updated',
//           data: result
//       });
//   })
//   .catch(err => {
//       console.log(err);
//       res.status(404).json({
//           error: err
//       });
//   });
// };

exports.suggestion = (req, res, next) => {
  Amis.find({$or: [{invitationSender: req.params.id}, {invitationReceiver: req.params.id}]})
    .select('invitationSender invitationReceiver')
    .then(amis => {
      let userFriends = [];
      userFriends.push(req.params.id);
      amis.forEach(ami => {
        if(ami.invitationReceiver.toString() === req.params.id) {
          userFriends.push(ami.invitationSender);
        } else {
          userFriends.push(ami.invitationReceiver);
        }
      });
      console.log(userFriends);
      User.find({_id: {$nin: userFriends}, status: true})
        .select('sectors avatar username')
        .then(users => {
          // console.log(users);
          res.json(performData(users));
        })
    })
    .catch(err => res.status(500).json(err));
}; 

exports.getFriendsList = (req, res, next) => {
  Amis.find({status: "accepted"})
    .then(amis => {
      let users = new Set();
      users.add(req.params.id);
      amis.forEach(ami => {
        if(ami.invitationReceiver.toString() === req.params.id || ami.invitationSender.toString() === req.params.id) {
          users.add(ami.invitationSender.toString());
          users.add(ami.invitationReceiver.toString());
        }
      });
      let data = users;
      data.forEach(user => {
        amis.forEach(ami => {
          if(ami.invitationReceiver.toString() === user || ami.invitationSender.toString() === user) {
            users.add(ami.invitationSender.toString());
            users.add(ami.invitationReceiver.toString());
          }
        });
      });
      console.log(users);
      req.users = Array.from(users);
      next();
    })
    .catch(err => res.status(500).json(err));
};

exports.deleteFriendship = (req, res, next) => {
  Amis.findOneAndDelete({invitationSender: {$in: [req.params.sender, req.params.receiver]}, invitationReceiver: {$in: [req.params.sender, req.params.receiver]}})
    .then(friendship => res.json(friendship))
    .catch(err => res.status(500).json(err));
};

exports.getBlocked = (req, res, next) => {
  Amis.find({status: 'blocked', $or:[{invitationSender: req.params.id}, {invitationReceiver: req.params.id}]})
    .then(amis => {
      res.json(amis);
    })
    .catch(err => res.status(500).json(err));
}

exports.blockUser = (req, res, next) => {
  const user_id = req.body.user;
  const friend_id = req.body.friend;
  const state = req.body.state;
  const query1 = { $or: [{invitationSender: user_id, invitationReceiver: friend_id}, {invitationSender: friend_id, invitationReceiver: user_id}] };
  const query2 = { blocked: true, blocked_date: new Date() };
  Amis.findOneAndUpdate(query1, query2, { new: true, useFindAndModify: false })
    .populate('invitationSender', 'username sectors avatar is_log')
    .populate('invitationReceiver', 'username sectors avatar is_log')
    .then(ami => {
      if(!ami) return res.json({});
      let friend = {};
      friend.blocked = ami.blocked;
      if(ami.invitationSender._id.toString() === user_id) {
        friend.sectors = ami.invitationReceiver.sectors;
        friend.username = ami.invitationReceiver.username;
        friend.avatar = ami.invitationReceiver.avatar;
        friend.is_log = ami.invitationReceiver.is_log;
        friend._id = ami.invitationReceiver._id;
      } else {
        friend.sectors = ami.invitationSender.sectors;
        friend.username = ami.invitationSender.username;
        friend.avatar = ami.invitationSender.avatar;
        friend.is_log = ami.invitationSender.is_log;
        friend._id = ami.invitationSender._id;
      }
      return res.json(friend);
    })
    .catch(err => res.status(500).json(err));
}

exports.deblockUser = (req, res, next) => {
  const user_id = req.body.user;
  const friend_id = req.body.friend;
  const state = req.body.state;
  const query1 = { $or: [{invitationSender: user_id, invitationReceiver: friend_id}, {invitationSender: friend_id, invitationReceiver: user_id}] };
  const query2 = { blocked: false, deblocked_date: new Date() };
  Amis.findOneAndUpdate(query1, query2, { new: true, useFindAndModify: false })
  .populate('invitationSender', 'username sectors avatar is_log')
  .populate('invitationReceiver', 'username sectors avatar is_log')
  .then(ami => {
    if(!ami) return res.json({});
    let friend = {};
    friend.blocked = ami.blocked;
    if(ami.invitationSender._id.toString() === user_id) {
      friend.sectors = ami.invitationReceiver.sectors;
      friend.username = ami.invitationReceiver.username;
      friend.avatar = ami.invitationReceiver.avatar;
      friend.is_log = ami.invitationReceiver.is_log;
      friend._id = ami.invitationReceiver._id;
    } else {
      friend.sectors = ami.invitationSender.sectors;
      friend.username = ami.invitationSender.username;
      friend.avatar = ami.invitationSender.avatar;
      friend.is_log = ami.invitationSender.is_log;
      friend._id = ami.invitationSender._id;
    }
    return res.json(friend);
  })
}

exports.findUsersBlocked = (req, res, next) => {
  const query = { _id: req.params.id, blocked: true };
  Amis.find(query)
    .then(amis => res.json(amis))
    .catch(err => res.status(500).json(err));
}


exports.find = (req, res, next) => {
  const id = req.params.id;
  const query = { status: {$ne: "pending"}, $or: [{ invitationSender: id }, { invitationReceiver: id }] };
  Amis.find(query)
    .populate('invitationSender', 'username sectors avatar is_log')
    .populate('invitationReceiver', 'username sectors avatar is_log')
    .then(amis => {
      let data = [];
      amis.forEach((elt, index) => {
        let user = {};
        if(elt.invitationSender._id.toString() === id) {
          user.sectors = elt.invitationReceiver.sectors;
          user.username = elt.invitationReceiver.username;
          user.avatar = elt.invitationReceiver.avatar;
          user.is_log = elt.invitationReceiver.is_log;
          user._id = elt.invitationReceiver._id;
        } else {
          user.sectors = elt.invitationSender.sectors;
          user.username = elt.invitationSender.username;
          user.avatar = elt.invitationSender.avatar;
          user.is_log = elt.invitationSender.is_log;
          user._id = elt.invitationSender._id;
        }
        user.blocked = elt.blocked;
        data.push(user);
      })
      res.json(performData(data));
    })
    .catch(err => res.status(500).json(err));
}

function performData(ami) {
  if(ami.length) {
    return ami.map(elt => {
      if(elt.invitationSender && elt.invitationSender.avatar) {
        elt.invitationSender.avatar = `${STATIC_URL}/static${elt.invitationSender.avatar.split('/static').length > 1 ? elt.invitationSender.avatar.split('/static')[1]: elt.invitationSender.avatar.split('/static')[0]}`;
      }
      if(elt.invitationReceiver && elt.invitationReceiver.avatar) {
        elt.invitationReceiver.avatar = `${STATIC_URL}/static${elt.invitationReceiver.avatar.split('/static').length > 1 ? elt.invitationReceiver.avatar.split('/static')[1]: elt.invitationReceiver.avatar.split('/static')[0]}`;
      }
      if(elt.avatar) {
        elt.avatar = `${STATIC_URL}/static${elt.avatar.split('/static').length > 1 ? elt.avatar.split('/static')[1]: elt.avatar.split('/static')[0]}`;
      }
      return elt;
    })
  }
  if(ami.invitationSender && ami.invitationSender.avatar) {
    ami.invitationSender.avatar = `${STATIC_URL}/static${ami.invitationSender.avatar.split('/static').length > 1 ? ami.invitationSender.avatar.split('/static')[1]: ami.invitationSender.avatar.split('/static')[0]}`;
  }
  if(ami.invitationReceiver && ami.invitationReceiver.avatar) {
    ami.invitationReceiver.avatar = `${STATIC_URL}/static${ami.invitationReceiver.avatar.split('/static').length > 1 ? ami.invitationReceiver.avatar.split('/static')[1]: ami.invitationReceiver.avatar.split('/static')[0]}`;
  }
  if(ami.avatar) {
    ami.avatar = `${STATIC_URL}/static${ami.avatar.split('/static').length > 1 ? ami.avatar.split('/static')[1]: ami.avatar.split('/static')[0]}`;
  }
  return ami;
}
