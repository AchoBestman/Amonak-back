const router = require('express').Router();
const controller = require('../controllers/friends.controller');
const userController = require('../controllers/user.controller');

router.get('/', controller.find);

router.post('/', controller.save, userController.friendRequest);

router.put('/accept', controller.accept);

router.put('/reject', controller.reject, userController.rejectFriendRequest);

