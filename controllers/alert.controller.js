const { AlertPublication } = require('../models/publication');

exports.findAll = (req, res, next) => {
    AlertPublication.find()
        .then(alerts => {
            res.json(alerts);
        })
        .catch(err => res.status(500).json(err));
}

// exports.findOne = (req, res, next) => {
//     AlertPublication.findOne()
// }


exports.createAlert = (req, res, next)=>{
    console.log(req.body);
    res.json({message: 'Create Alert'});
}