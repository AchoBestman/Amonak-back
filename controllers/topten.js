const TopTen  = require('../models/topten');
const sendMail = require('../middlewares/mail');
const { STATIC_URL } = require('../middlewares/api_url');

exports.createPostTopTen = (req, res, next)=>{

   let d = new Date();
   let jours = parseInt(req.body.duration, 10) * 7;
   let date_fin = d.setDate(d.getDate() + jours);
   let topTen = new TopTen({
       compagny: req.body.compagny,
       duration: req.body.duration,
       website: req.body.website,
       message: req.body.message,
       date_fin: date_fin,
       creator: req.body.user_id
    });

   if(req.files.length){
        req.files.forEach(element => {
            if(element.mimetype.match('image/*')){
                topTen.image.push({
                    url: `${STATIC_URL}/static/images/uploads/${element.filename}`,
                    contentType: element.mimetype
                });
            }
        });
    }
console.log(topTen)
    topTen.save()
        .then(topten => {
            topten.populate('creator', (err, data)=>{
                if(err) throw err
                res.json(performData(data));
            });
        })
        .catch(err => {
          res.status(500).json(err)
        } );

}; 

exports.getAll = (req, res, next) => {
    // let query = { status: 'anabled' };
    let query = {};
    TopTen.find(query)
        .sort({createdAt: -1}) 
        .exists('image')
        .where('image').ne([])
        //.where( { image: { $size: 2 } } )
        .limit(10)
        .populate('creator')
        .populate('followers')
        .then(topten => res.json(performData(topten)))
        .catch(err => res.status(500).json(err));
}

exports.findById = (req, res, next) => {
  const id = req.params.id
    TopTen.findOne({ _id: id})
        .exists('image')
        .where('image').ne([])
        .where( { image: { $size: 5 } } )
        .populate('creator')
        .populate('followers')
        .then(topten => res.json(performData(topten)))
        .catch(err => res.status(500).json(err));
}

exports.findByStatus = (req, res, next) => {
    req.params.status = 'disabled'
    TopTen.find({ status: req.params.status })
        .sort({createdAt: -1})
        .exists('image')
        .where('image').ne([])
        .where( { image: { $size: 5 } } )
        .limit(10)
        .populate('creator')
        .populate('followers')
        .then(topten => res.json(performData(topten)))
        .catch(err => res.status(500).json(err));
}

exports.deleteOne = (req, res, next) => {

  const item = req.params.id.split(' ');

  if (item[1] != 'anabled') {
      console.log("topten is deleted");
      TopTen.updateOne({ _id: item[0] },{status: item[1]})
          .then(topten => res.json('topten is deleted'))
          .catch(err => res.status(500).json(err));
  }else{
      TopTen.find({ status: item[1]}).count()
        .then(topten_length => {
            console.log(topten_length);
            if (topten_length < 11) {
                  console.log('topten is anabled');
                  TopTen.updateOne({ _id: item[0] },{status: item[1]})
                  .then(topten => res.json('topten is anabled'))
                  .catch(err => res.status(500).json(err));
            }else{
              console.log('place not found');
              res.json('place not found');
            }
        })
        .catch(err => console.log(err));
  }

}

exports.beInterested = (req, res, next) => {

    let query = { $push: { followers: req.body.creator } };
    TopTen.findByIdAndUpdate(req.body._id, query, { new: true, useFindAndModify: false })
        .then(topten => {
            let data = {};
            data.sender = req.body.sender;
            data.receiver = req.body.receiver;
            data.creator = req.body.creator;
            sendMail.sendRequest({topten: topten, data: data}, req, res);
            // return res.json({message: data});
        })
        .catch(err => res.status(500).json(err));
}

function performData(topten) {
    
    if(topten.length) {
        return topten.map(elt => {
            if(elt.creator && elt.creator.avatar) {
                elt.creator.avatar = `${STATIC_URL}/static${elt.creator.avatar.split('/static').length > 1 ? elt.creator.avatar.split('/static')[1]: elt.creator.avatar.split('/static')[0]}`;
            }
            if(elt.image && elt.image.length) {
                elt.image.forEach(file => {
                    file.url = `${STATIC_URL}/static${file.url.split('/static').length > 1 ? file.url.split('/static')[1]: file.url.split('/static')[0]}`;
                })
            }
            if(elt.followers && elt.followers.length) {
                elt.followers.forEach(follower => {
                    if (follower && follower.avatar) {
                        follower.avatar = `${STATIC_URL}/static${follower.avatar.split('/static').length > 1 ? follower.avatar.split('/static')[1]: follower.avatar.split('/static')[0]}`;
                    }
                })
            }
            return elt;
        })
    }
    if(topten.creator && topten.creator.avatar) {
        topten.creator.avatar = `${STATIC_URL}/static${topten.creator.avatar.split('/static').length > 1 ? topten.creator.avatar.split('/static')[1]: topten.creator.avatar.split('/static')[0]}`;
    }
    if(topten.image && topten.image.length) {
        topten.image.forEach(file => {
            file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
        })
    }
    if(topten.followers && topten.followers.length) {
        topten.followers.forEach(follower => {
            if (follower && follower.avatar) {
                follower.avatar = `${STATIC_URL}/static${follower.avatar.split('/static').length > 1 ? follower.avatar.split('/static')[1]: follower.avatar.split('/static')[0]}`;
            }
        })
    }
    return topten;
}
 
  
  