const Newsletter = require('../models/newsletter');


exports.save = (req, res, next) => {
    const name = req.body.name;
    const address = req.body.address;
    const email = req.body.email;
    Newsletter.findOne({email: email})
        .then(newLetter => {
            if(newLetter) {
                return res.status(400).json({ message: "email already exists."});
            }
            const new_letter = new Newsletter({
                name,
                address,
                email
            });
            new_letter.save()
                .then(letter => res.status(201).json({ message: "success" }))
                .catch(err => res.status(500).json(err))
        })
        .catch(err => res.status(500).json(err))
}

exports.find = (req, res, next) => {
    const query = (req.query.status && req.query.status === "true" ) ? { status: true } : {};
    Newsletter.find(query)
        .then(newLetters => res.json(newLetters))
        .catch(err => res.status(500).json(err))
}

exports.findOne = (req, res, next) => {
    const id = req.params.id;
    Newsletter.find({_id: id})
        .then(newLetter => {
            if(!newLetter) {
                return res.status(404).json({ message: "Not found." });
            }
            res.json(newLetter);
        })
        .catch(err => res.status(500).json(err))
}

exports.delete = (req, res, next) => {
    const id = req.params.id;
    const query = { status: false };
    Newsletter.findOneAndUpdate({_id: id}, query, { new: true, useFindAndModify: false })
        .then(newLetter => {
            res.json({ message: "success." });
        })
        .catch(err => res.status(500).json(err));
}
