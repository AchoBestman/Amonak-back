const router = require('express').Router();
const { avatarUpload, multipleUpload } = require('../middlewares/multer');
const path = require('path');
const fs = require('fs');

router.post('/', multipleUpload, (req, res, next) => {
    let files = [];
    req.files.forEach(elt => {
        files.push({name: elt.filename, type: elt.mimetype});
    })
    return res.json(files);
});

router.delete('/:name', (req, res, next) => {
    let file_path;
    if(req.query.ext === 'image') {
        file_path = path.resolve('./public/images/uploads').concat(`/${req.params.name}`);
    } else if(req.query.ext === 'video') {
        file_path = path.resolve('./public/videos/uploads').concat(`/${req.params.name}`);
    } else {
        file_path = path.resolve('./public/audios/uploads').concat(`/${req.params.name}`);
    }
    console.log("2");
    fs.unlink(file_path, (err) => {
        if(err) {
            return res.json(err);
        };
        res.json(req.params.name + ' deleted successfuly !');
    });
});

module.exports = router;
