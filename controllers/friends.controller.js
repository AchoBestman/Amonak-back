const Friends = require('../models/friends');

exports.find = (req, res, next) => {
    Friends.find()
        .then(friends => res.json(friends))
        .catch(err => res.status(500).json(err));
}

exports.save =(req, res, next) => {
    const user_id = req.body.user_id;
    const friend_id = req.body.friend_id;
    const query = { $or: [{ from: user_id, to: friend_id }, { from: friend_id, to: user_id }] };
    Friends.find(query)
        .then(friend => {
            if(friend && friend.length) {
                res.status(301).json({ message: "Error." });
                return 0;
            }
            let user_request = new Friends({
                from: user_id,
                to: friend_id,
                status: 1
            });
            let friend_request = new Friends({
                from: friend_id,
                to: user_id,
                status: 2
            });

            user_request.save()
                .then(friend => {
                    req.body.user_request = friend._id;
                    friend_request.save()
                        .then(friend => {
                            req.body.friend_request = friend._id;
                            next();
                        })
                        .catch(err => {
                            res.status(500).json(err);
                            return 0;
                        })
                })
                .catch(err => {
                    res.status(500).json(err);
                    return 0;
                })

        })
        .catch(err => {
            res.status(500).json(err);
            return 0;
        })
};

exports.accept = (req, res, next) => {
    const user_id = req.body.user_id;
    const friend_id = req.body.friend_id;
    const query1 = { from: user_id, to: friend_id };
    const query2 = { from: friend_id, to: user_id };
    Friends.findOneAndUpdate(query1, { status: 3 }, {new: true, useFindAndModify: false })
        .then(friend => {
            Friends.findOneAndUpdate(query2, { status: 3 }, {new: true, useFindAndModify: false })
                .then(friend =>{
                    console.log(friend);
                    res.json(friend);
                })
                .catch(err => {
                    res.status(500).json(err);
                    return 0;
                })
        })
        .catch(err => {
            res.status(500).json(err);
            return 0;
        })
}

exports.reject = (req, res, next) => {
    const user_id = req.body.user_id;
    const friend_id = req.body.friend_id;
    const query1 = { from: user_id, to: friend_id };
    const query2 = { from: friend_id, to: user_id };
    Friends.findOneAndUpdate(query1, { status: 3 }, {new: true, useFindAndModify: false })
        .then(friend => {
            req.body.user_request = friend._id;
            Friends.findOneAndUpdate(query2, { status: 3 }, {new: true, useFindAndModify: false })
                .then(friend =>{
                    console.log(friend);
                    req.body.friend_request = friend._id;
                    next();
                })
                .catch(err => {
                    res.status(500).json(err);
                    return 0;
                })
        })
        .catch(err => {
            res.status(500).json(err);
            return 0;
        })   
}
