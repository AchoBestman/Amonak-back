const { STATIC_URL } = require('../middlewares/api_url');
const { Publication, PostPublication, SalePublication, AlertPublication, SharePublication, PublicationReported } = require('../models/publication');
 
exports.savePost = (req, res, next) => {
    let new_post = new PostPublication({
        content: req.body.content,
        creator: req.body.user_id,
    });
    if(req.body.files){
        let files = req.body.files;
        files.forEach(element => {
            if(element.type.match('image/*')){
                new_post.files.push({
                    url: `${STATIC_URL}/static/images/uploads/${element.name}`,
                    type: element.type
                });
            } else if(element.type.match('video/*')) {
                new_post.files.push({
                    url: `${STATIC_URL}/static/videos/uploads/${element.name}`,
                    type: element.type
                });
            }
        });
    }
    new_post.save()
        .then(publication => {
            publication.populate('creator', 'username avatar sectors is_log address', (err, data)=>{
                if(err) throw err
                // data.map(elt => {
                //     if(elt.files && elt.files.length) {
                //         elt.files.forEach(file => {
                //             file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
                //         })
                //     }
                //     return elt;
                // })
                res.json(performData(data));
            });
        })
        .catch(err => res.status(500).json(err));
};

exports.saveAlert = (req, res, next) => {
    let new_alert = new AlertPublication({
        content: req.body.content,
        creator: req.body.user_id,
        name: req.body.name,
        duration: req.body.duration,
        alert_type: req.body.alert_type,
    });
    if(req.body.files){
        let files = req.body.files;
        files.forEach(element => {
            if(element.type.match('image/*')){
                new_alert.files.push({
                    url: `${STATIC_URL}/static/images/uploads/${element.name}`,
                    type: element.type
                });
            } else if(element.type.match('video/*')) {
                new_alert.files.push({
                    url: `${STATIC_URL}/static/videos/uploads/${element.name}`,
                    type: element.type
                });
            }
        });
    }
    new_alert.save()
        .then(publication => {
            publication.populate('creator', 'username avatar sectors is_log address', (err, data)=>{
                if(err) throw err
                res.json(performData(data));
            });
        })
        .catch(err => res.status(500).json(err));
};

exports.saveSale = (req, res, next) => {
    console.log(req.body);
    let new_sale = new SalePublication({
        content: req.body.content,
        creator: req.body.user_id,
        product: req.body.product_id,
        files: req.body.files,
        sale_type: req.body.sale_type,
    });
    new_sale.save()
        .then(publication => {
            publication.populate("product", "-owner -_id -__v -status")
                .populate("creator", "username sectors avatar is_log address", (err, data) => {
                    if(err) throw err
                    res.json(performData(data));
                })
        })
        .catch(err => res.status(500).json(err));
};

exports.saveShare = (req, res, next) => {
    const publication_id = req.body.publication_id;
    const creator_id = req.body.user_id;
    const content = req.body.content;

    let new_share = new SharePublication({
        creator: creator_id,
        publication: publication_id
    });
    if(content) {
        new_share.content = content;
    }
    console.log(new_share);
    new_share.save()
        .then(publication => {
            publication.populate({
                path: "publication",
                populate: { path: "creator", select: "username sectors avatar is_log address" }
            })
            .populate("creator", "username sectors avatar is_log", (err, data) => {
                if(err) throw err
                req.body.receiver = publication.creator;
                req.body.receivers = publication.followers;
                req.body.target = publication_id;
                next();

                res.json(performData(data));
            });
            Publication.findByIdAndUpdate(publication_id, { $inc: { share: 1 } }, { new: true, useFindAndModify: false })
                .then(publication => {
                    console.log("bonjour");
                    // req.body.content = "share_post";
                    // req.body.sender = req.body.user_id;
                    // req.body.receivers = publication.followers;
                    // req.body.target = publication_id;
                    // next();
                })
                .catch(err => res.status(500).json(err));
        })
        .catch(err => res.status(500).json(err));
};

exports.find = (req, res, next) => {
    Publication.find({status: true})
        .sort({createdAt: -1})
        .populate({
            path: 'publication',
            populate: [
                { path: 'creator', select: 'sectors avatar username is_log address' },
                { path: 'product', select: '-owner -__v -status' },
            ],
        })
        // .populate({
        //     path: 'publication',
        //     populate: { path: 'product', select: '-owner -__v -status' },
        // })
        .populate('product', '-owner -__v -status')
        .populate('creator', 'username sectors avatar is_log address')
        .populate({
            path: 'comments',
            populate: { path: 'creator', select: 'sectors avatar username is_log address' },
            select: 'likes creator content createdAt'
        })
        // .populate({
        //     path: 'comments',
        //     populate: { path: 'likes.creator' }
        // })
        // .populate('likes.creator', 'username sectors avatar')
        .then(publications => {
            
            // publications.map(elt => {
            //     if(elt.files && elt.files.length) {
            //         elt.files.forEach(file => {
            //             file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
            //         })
            //     }
            //     return elt;
            // })
            res.json(performData(publications));
        })
        .catch(err => res.status(500).json(err))
};

exports.findById = (req, res, next) => {
    const publication_id = req.params.id;
    Publication.findById(publication_id, "-__v -updatedAt -status")
        .populate({
            path: 'publication',
            populate: { path: 'creator', select: 'sectors avatar username is_log address' },
        })
        .populate('product', '-owner -__v -status')
        .populate('creator', 'username sectors avatar is_log')
        .populate({
            path: 'comments',
            populate: { path: 'creator', select: 'sectors avatar username is_log address' },
            select: 'likes creator content'
        })
        .populate({
            path: 'comments',
            populate: { path: 'likes.creator' }
        })
        .populate('likes.creator', 'username sectors avatar is_log address')
        .then(publications => {
            // publications.map(elt => {
            //     if(elt.files && elt.files.length) {
            //         elt.files.forEach(file => {
            //             file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
            //         })
            //     }
            //     return elt;
            // })
            res.json(performData(publications));
        })
        .catch(err => res.status(500).json(err));
};

exports.findNewsFeeds = (req, res, next) => {
    const creators = req.body.users;
    const user_id = req.params.user_id;
    // const query = { status: true, creator: { $in: creators }, deleters: { $nin: req.params.user_id }};
    const query = { creator: { $in: creators }, deleters: { $nin: user_id }};
    Publication.find(query)
        .sort({createdAt: -1})
        .populate({
            path: 'publication',
            populate: { path: 'creator', select: 'sectors avatar username is_log address' },
        })
        .populate('product', '-owner -__v -status')
        .populate('creator', 'username sectors avatar')
        .populate({
            path: 'comments',
            populate: { path: 'creator', select: 'sectors avatar username is_log address' },
            select: 'likes creator content'
        })
        .populate({
            path: 'comments',
            populate: { path: 'likes.creator' }
        })
        .populate('likes.creator', 'username sectors avatar is_log address')
        .then(publications => {
            // publications.map(elt => {
            //     if(elt.files && elt.files.length) {
            //         elt.files.forEach(file => {
            //             file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
            //         })
            //     }
            //     return elt;
            // })
            res.json(performData(publications));
        })
        .catch(err => res.status(500).json(err))
};

exports.findByProduct = (req, res, next) => {
    const product_id = req.params.product_id;
    const query = { product: product_id };
    SalePublication.findOne(query)
        .sort({createdAt: -1})
        .populate({
            path: 'publication',
            populate: { path: 'creator', select: 'sectors avatar username is_log address' },
        })
        .populate('product', '-owner -__v -status')
        .populate('creator', 'username sectors avatar is_log address')
        .populate({
            path: 'comments',
            populate: { path: 'creator', select: 'sectors avatar username is_log address' },
            select: 'likes creator content'
        })
        .populate({
            path: 'comments',
            populate: { path: 'likes.creator' }
        })
        .populate('likes.creator', 'username sectors avatar is_log address')
        .then(publications => {
            // publications.map(elt => {
            //     if(elt.files && elt.files.length) {
            //         elt.files.forEach(file => {
            //             file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
            //         })
            //     }
            //     return elt;
            // })
            res.json(performData(publications));
        })
        .catch(err => res.status(500).json(err))
}

exports.findUserPublications = (req, res, next) => {
    const user_id = req.params.user_id;
    const query = { creator: user_id };
    Publication.find(query)
        .sort({ createdAt: -1 })
        .populate({
            path: 'publication',
            populate: { path: 'creator', select: 'sectors avatar username is_log address' },
        })
        .populate('product', '-owner -__v -status')
        .populate('creator', 'username sectors avatar is_log address')
        .populate({
            path: 'comments',
            populate: { path: 'creator', select: 'sectors avatar username is_log address' },
            select: 'likes creator content'
        })
        .populate({
            path: 'comments',
            populate: { path: 'likes.creator' }
        })
        .populate('likes.creator', 'username sectors avatar is_log address')
        .then(publications => {
            // publications.map(elt => {
            //     if(elt.files && elt.files.length) {
            //         elt.files.forEach(file => {
            //             file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
            //         })
            //     }
            //     return elt;
            // })
            res.json(performData(publications));
        })
        .catch(err => res.status(500).json(err))
}

exports.findProfilePublications = (req, res, next) => {
    const user_id = req.params.user_id;
    const query = { creator: user_id };
    Publication.find(query)
        .sort({ createdAt: -1 })
        .populate({
            path: 'publication',
            populate: { path: 'creator', select: 'sectors avatar username is_log address' },
        })
        .populate('product', '-owner -__v -status')
        .populate('creator', 'username sectors avatar is_log address')
        .populate({
            path: 'comments',
            populate: { path: 'creator', select: 'sectors avatar username is_log address' },
            select: 'likes creator content'
        })
        .populate({
            path: 'comments',
            populate: { path: 'likes.creator' }
        })
        .populate('likes.creator', 'username sectors avatar is_log')
        .then(publications => {
            // publications.map(elt => {
            //     if(elt.files && elt.files.length) {
            //         elt.files.forEach(file => {
            //             file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
            //         })
            //     }
            //     return elt;
            // })
            res.json(performData(publications));
        })
        .catch(err => res.status(500).json(err))
}

exports.findUserPublicationsSaved = (req, res, next) => {
    const id = req.params.id;
    const query = { savers: id };
    Publication.find(query)
        .then(publications => res.json(publications))
        .catch(err => res.status(500).json(err));
}

exports.addLike = (req, res, next) => {
    const publication_id = req.params.id;
    const user_id = req.body.user_id;
    const query = { $push :  { likes: { creator: user_id, createdAt: Date.now() } } };
    Publication.findByIdAndUpdate(publication_id, query, { new: true, useFindAndModify: false })
        // .populate('likes.creator', 'username sectors avatar')
        .then(publication => {
            req.body.receiver = publication.creator;
            req.body.receivers = publication.followers;
            req.body.target = publication_id;
            next();
            
            res.json(publication.likes);
            // req.body.sender = user_id;
            // req.body.content = "post_like";
            // req.body.receivers = publication.followers;
            // req.body.target = publication_id;
            // next();
        })
        .catch(err => res.status(500).json(err))
}

exports.addComment = (req, res, next) => {
    const publication_id = req.body.publication;
    const comment_id = req.body.comment_id;
    const query = { $push :  { comments: comment_id } };
    Publication.findOneAndUpdate({_id: publication_id}, query, { new: true, useFindAndModify: false })
        .then(publication => {
            console.log('in here::::');
            req.body.receiver = publication.creator;
            req.body.receivers = publication.followers;
            req.body.target = publication_id;
            console.log(publication)
            next();
            // req.body.sender = req.body.user_id;
            // req.body.receivers = publication.followers;
            // req.body.target = publication_id;
            // req.body.content = "comment_post";
            // next();
        })
        .catch(err => res.status(500).json(err))
}

exports.addFollower = (req, res, next) => {
    const publication_id = req.params.id;
    const user_id = req.body.user_id;
    const query = { $push :  { followers: user_id } };
    Publication.findByIdAndUpdate(publication_id, query, { new: true, useFindAndModify: false })
        .populate({
            path: 'publication',
            populate: { path: 'creator', select: 'sectors avatar username is_log address' },
        })
        .populate('product', '-owner -__v -status')
        .populate('creator', 'username sectors avatar is_log')
        .populate({
            path: 'comments',
            populate: { path: 'creator', select: 'sectors avatar username is_log address' },
            select: 'likes creator content'
        })
        .populate({
            path: 'comments',
            populate: { path: 'likes.creator' }
        })
        .populate('likes.creator', 'username sectors avatar is_log address')
        .then(publications => {
            // publications.map(elt => {
            //     if(elt.files && elt.files.length) {
            //         elt.files.forEach(file => {
            //             file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
            //         })
            //     }
            //     return elt;
            // })
            res.json(performData(publications));
        })
        .catch(err => res.status(500).json(err))
}

exports.addSaver = (req, res, next) => {
    const publication_id = req.params.id;
    const user_id = req.body.user_id;
    const query = { $push :  { savers: user_id } };
    Publication.findByIdAndUpdate(publication_id, query, { new: true, useFindAndModify: false })
        .populate({
            path: 'publication',
            populate: { path: 'creator', select: 'sectors avatar username is_log address' },
        })
        .populate('product', '-owner -__v -status')
        .populate('creator', 'username sectors avatar is_log address')
        .populate({
            path: 'comments',
            populate: { path: 'creator', select: 'sectors avatar username is_log address' },
            select: 'likes creator content'
        })
        .populate({
            path: 'comments',
            populate: { path: 'likes.creator' }
        })
        .populate('likes.creator', 'username sectors avatar is_log address')
        .then(publications => {
            // publications.map(elt => {
            //     if(elt.files && elt.files.length) {
            //         elt.files.forEach(file => {
            //             file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
            //         })
            //     }
            //     return elt;
            // })
            res.json(performData(publications));
        })
        .catch(err => res.status(500).json(err))
}

exports.addDeleter = (req, res, next) => {
    const publication_id = req.params.id;
    const user_id = req.body.user_id;
    const query = { $push :  { deleters: user_id } };
    Publication.findByIdAndUpdate(publication_id, query, { new: true, useFindAndModify: false })
        .populate({
            path: 'publication',
            populate: { path: 'creator', select: 'sectors avatar username is_log address' },
        })
        .populate('product', '-owner -__v -status')
        .populate('creator', 'username sectors avatar is_log address')
        .populate({
            path: 'comments',
            populate: { path: 'creator', select: 'sectors avatar username is_log address' },
            select: 'likes creator content'
        })
        .populate({
            path: 'comments',
            populate: { path: 'likes.creator' }
        })
        .populate('likes.creator', 'username sectors avatar is_log address')
        .then(publications => {
            // publications.map(elt => {
            //     if(elt.files && elt.files.length) {
            //         elt.files.forEach(file => {
            //             file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
            //         })
            //     }
            //     return elt;
            // })
            res.json(performData(publications));
        })
        .catch(err => res.status(500).json(err))
}

exports.removeLike = (req, res, next) => {
    const publication_id = req.params.id;
    const user_id = req.body.user_id;
    const query = { $pull :  { likes: { creator: user_id } } };
    Publication.findByIdAndUpdate(publication_id, query, { new: true, useFindAndModify: false })
        .populate({
            path: 'publication',
            populate: { path: 'creator', select: 'sectors avatar username is_log address' },
        })
        .populate('product', '-owner -__v -status')
        .populate('creator', 'username sectors avatar is_log')
        .populate({
            path: 'comments',
            populate: { path: 'creator', select: 'sectors avatar username is_log address' },
            select: 'likes creator content'
        })
        .populate({
            path: 'comments',
            populate: { path: 'likes.creator' }
        })
        .populate('likes.creator', 'username sectors avatar is_log address')
        .then(publications => {
            // publications.map(elt => {
            //     if(elt.files && elt.files.length) {
            //         elt.files.forEach(file => {
            //             file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
            //         })
            //     }
            //     return elt;
            // })
            res.json(performData(publications));
        })
        .catch(err => res.status(500).json(err))
}

exports.removeComment = (req, res, next) => {
    const publication_id = req.body.publication_id;
    const comment_id = req.body.comment_id;
    const query = { $pull: { comments: comment_id } };
    Publication.findByIdAndUpdate(publication_id, query, { new: true, useFindAndModify: false })
        .then(publications => {
            // publications.map(elt => {
            //     if(elt.files && elt.files.length) {
            //         elt.files.forEach(file => {
            //             file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
            //         })
            //     }
            //     return elt;
            // })
            res.json(performData(publications));
        })
        .catch(err => res.status(500).json(err))
}

exports.removeFollower = (req, res, next) => {
    const publication_id = req.params.id;
    const user_id = req.body.user_id;
    const query = { $pull :  { followers: user_id } };
    Publication.findByIdAndUpdate(publication_id, query, { new: true, useFindAndModify: false })
        .populate({
            path: 'publication',
            populate: { path: 'creator', select: 'sectors avatar username is_log' },
        })
        .populate('product', '-owner -__v -status')
        .populate('creator', 'username sectors avatar is_log')
        .populate({
            path: 'comments',
            populate: { path: 'creator', select: 'sectors avatar username is_log' },
            select: 'likes creator content'
        })
        .populate({
            path: 'comments',
            populate: { path: 'likes.creator' }
        })
        .populate('likes.creator', 'username sectors avatar is_log')
        .then(publications => {
            // publications.map(elt => {
            //     if(elt.files && elt.files.length) {
            //         elt.files.forEach(file => {
            //             file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
            //         })
            //     }
            //     return elt;
            // })
            res.json(performData(publications));
        })
        .catch(err => res.status(500).json(err))
}

exports.removeSaver = (req, res, next) => {
    const publication_id = req.params.id;
    const user_id = req.body.user_id;
    const query = { $pull :  { savers: user_id } };
    Publication.findByIdAndUpdate(publication_id, query, { new: true, useFindAndModify: false })
        .populate({
            path: 'publication',
            populate: { path: 'creator', select: 'sectors avatar username is_log' },
        })
        .populate('product', '-owner -__v -status')
        .populate('creator', 'username sectors avatar is_log')
        .populate({
            path: 'comments',
            populate: { path: 'creator', select: 'sectors avatar username is_log' },
            select: 'likes creator content'
        })
        .populate({
            path: 'comments',
            populate: { path: 'likes.creator' }
        })
        .populate('likes.creator', 'username sectors avatar is_log')
        .then(publications => {
            // publications.map(elt => {
            //     if(elt.files && elt.files.length) {
            //         elt.files.forEach(file => {
            //             file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
            //         })
            //     }
            //     return elt;
            // })
            res.json(performData(publications));
        })
        .catch(err => res.status(500).json(err))
}

exports.removeDeleter = (req, res, next) => {
    const publication_id = req.params.id;
    const user_id = req.body.user_id;
    const query = { $pull :  { deleters: user_id } };
    Publication.findByIdAndUpdate(publication_id, query, { new: true, useFindAndModify: false })
        .populate({
            path: 'publication',
            populate: { path: 'creator', select: 'sectors avatar username is_log' },
        })
        .populate('product', '-owner -__v -status')
        .populate('creator', 'username sectors avatar is_log')
        .populate({
            path: 'comments',
            populate: { path: 'creator', select: 'sectors avatar username is_log' },
            select: 'likes creator content'
        })
        .populate({
            path: 'comments',
            populate: { path: 'likes.creator' }
        })
        .populate('likes.creator', 'username sectors avatar is_log')
        .then(publications => {
            // publications.map(elt => {
            //     if(elt.files && elt.files.length) {
            //         elt.files.forEach(file => {
            //             file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
            //         })
            //     }
            //     return elt;
            // })
            res.json(performData(publications));
        })
        .catch(err => res.status(500).json(err))
}

exports.reportPost = (req, res, next) => {
    const publication_id = req.params.id;
    const user_id = req.body.user_id;
    const reason = req.body.reason;
    // const query = { status: true, $push: { deleters: user_id } };
    const query = { $push: { deleters: user_id } };
    let new_publication = new PublicationReported({
        creator: user_id,
        publication: publication_id,
        reason: reason,
    });
    new_publication.save()
        .then(publication => {
            console.log(publication);
            Publication.findByIdAndUpdate(publication_id, query, { new: true, useFindAndModify: false })
                .then(publications => {
                    // publications.map(elt => {
                    //     if(elt.files && elt.files.length) {
                    //         elt.files.forEach(file => {
                    //             file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
                    //         })
                    //     }
                    //     return elt;
                    // })
                    res.json(performData(publications));
                })
                .catch(err => res.status(500).json(err));
        })
        .catch(err => res.status(500).json(err));

};

exports.delete = (req, res, next) => {
    const publication_id = req.params.id;
    const query = { status: false };
    Publication.findByIdAndUpdate(publication_id, query, { new: true, useFindAndModify: false })
        .then(publications => {
            // publications.map(elt => {
            //     if(elt.files && elt.files.length) {
            //         elt.files.forEach(file => {
            //             file.url = `${STATIC_URL}/static${file.url.split('/static')[0]}`;
            //         })
            //     }
            //     return elt;
            // })
            res.json(performData(publications));
        })
        .catch(err => res.status(500).json(err));
}

function performData(publications) {
    if(publications.length) {
        return publications.map(elt => {
            if(elt.creator && elt.creator.avatar) {
                elt.creator.avatar = `${STATIC_URL}/static${elt.creator.avatar.split('/static').length > 1 ? elt.creator.avatar.split('/static')[1]: elt.creator.avatar.split('/static')[0]}`;
            }
            if(elt.files && elt.files.length) {
                elt.files.forEach(file => {
                    file.url = `${STATIC_URL}/static${file.url.split('/static').length > 1 ? file.url.split('/static')[1]: file.url.split('/static')[0]}`;
                })
            }
            if(elt.publication && elt.publication.creator && elt.publication.creator.avatar) {
                elt.publication.creator.avatar = `${STATIC_URL}/static${elt.publication.creator.avatar.split('/static').length > 1 ? elt.publication.creator.avatar.split('/static')[1]: elt.publication.creator.avatar.split('/static')[0]}`;
            }
            if(elt.publication && elt.publication.files && elt.publication.files.length) {
                elt.publication.files.forEach(file => {
                    file.url = `${STATIC_URL}/static${file.url.split('/static').length > 1 ? file.url.split('/static')[1]: file.url.split('/static')[0]}`;
                })
            }
            if(elt.product && elt.product.files) {
                elt.product.files.forEach(file => {
                    file.url = `${STATIC_URL}/static${file.url.split('/static').length > 1 ? file.url.split('/static')[1]: file.url.split('/static')[0]}`;
                })
            }
            if(elt.comments && elt.comments.length) {
                elt.comments.forEach(com => {
                    if(com.creator && com.creator.avatar) {
                        com.creator.avatar = `${STATIC_URL}/static${com.creator.avatar.split('/static').length > 1 ? com.creator.avatar.split('/static')[1]: com.creator.avatar.split('/static')[0]}`;
                    }
                })
            }
            if(elt.likes && elt.likes.length) {
                elt.likes.forEach(like => {
                    if(like.creator && like.creator.avatar) {
                        like.creator.avatar = `${STATIC_URL}/static${like.creator.avatar.split('/static').length > 1 ? like.creator.avatar.split('/static')[1]: like.creator.avatar.split('/static')[0]}`;
                    }
                })
            }
            return elt;
        })
    }
    if(publications.creator && publications.creator.avatar) {
        publications.creator.avatar = `${STATIC_URL}/static${publications.creator.avatar.split('/static').length > 1 ? publications.creator.avatar.split('/static')[1]: publications.creator.avatar.split('/static')[0]}`
    }
    if(publications.publication && publications.publication.creator && publications.publication.creator.avatar) {
        publications.publication.creator.avatar = `${STATIC_URL}/static${publications.publication.creator.avatar.split('/static').length > 1 ? publications.publication.creator.avatar.split('/static')[1]: publications.publication.creator.avatar.split('/static')[0]}`;
    }
    if(publications.files && publications.files.length) {
        publications.files.forEach(file => {
            file.url = `${STATIC_URL}/static${file.url.split('/static').length > 1 ? file.url.split('/static')[1]: file.url.split('/static')[0]}`;
        })
    }
    if(publications.publication && publications.publication.files) {
        publications.publication.files.forEach(file => {
            file.url = `${STATIC_URL}/static${file.url.split('/static').length > 1 ? file.url.split('/static')[1]: file.url.split('/static')[0]}`;
        })
    }
    if(publications.product && publications.product.files) {
        publications.product.files.forEach(file => {
            file.url = `${STATIC_URL}/static${file.url.split('/static').length > 1 ? file.url.split('/static')[1]: file.url.split('/static')[0]}`;
        })
    }
    if(publications.comments && publications.comments.length) {
        publications.comments.forEach(com => {
            if(com.creator && com.creator.avatar) {
                com.creator.avatar = `${STATIC_URL}/static${com.creator.avatar.split('/static').length > 1 ? com.creator.avatar.split('/static')[1]: com.creator.avatar.split('/static')[0]}`;
            }
        })
    }
    if(publications.likes && publications.likes.length) {
        publications.likes.forEach(like => {
            if(like.creator && like.creator.avatar) {
                like.creator.avatar = `${STATIC_URL}/static${like.creator.avatar.split('/static').length > 1 ? like.creator.avatar.split('/static')[1]: like.creator.avatar.split('/static')[0]}`;
            }
        })
    }
    return publications;
}
