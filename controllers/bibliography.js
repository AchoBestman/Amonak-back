const Bibliography = require('../models/bibliography');
const Biography = require('../models/bibliography');
const User = require('../models/user');

exports.createBiography = (req,res,next) => {

 let newBiography = new Biography({
       creator: req.body.user_id
    }); 
 
 let query = {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        gender: req.body.gender,
        birthplace: req.body.birthplace,
        birthday: req.body.birthday,
        family_status: req.body.family_status,
        phone: req.body.phone,
        email: req.body.email,
        study: req.body.study,
        job: req.body.job,
        location: req.body.location,
        interest: req.body.interest,
        language: req.body.language,
        religion: req.body.religion,
        politic: req.body.politic,
        web_site: req.body.web_site,
        other_name: req.body.other_name,
        family_member: req.body.family_member
    };   
    let query1 = {
        gender: req.body.gender,
        phone: req.body.phone,
        profession: req.body.job
    };  

    
 
  Biography.find({ creator: req.body.user_id}).countDocuments()
        .then(bio_length => {
          console.log("element count"+bio_length+ ' user = '+req.body.user_id)
            if (bio_length < 1) {
               newBiography.save()
                .then(biographie => {
                  console.log(biographie);
                  if (req.body.birthday == '') {
                    query.birthday = '2020-12-01';
                  }
                  Biography.updateOne({ creator: req.body.user_id },query).then(biographie => {
                    console.log('success create before update ');
                    res.status(200).json("success");
                      User.updateOne({ _id: req.body.user_id },query1).then(user => {
                      console.log('success update user');
                      }).catch(err => console.log(err));
                  }).catch(err => console.log(err));
                  
                }).catch(err => res.status(500).json(err));
            }else{
                console.log('id ='+req.body.user_id)
                 if (req.body.birthday != '') {
                    query.birthday = '2020-12-01';
                  }
                  Biography.updateOne({ creator: req.body.user_id },{query}).then(biographie => {


        Biography.find({ creator: req.body.user_id })
        .then(biography => console.log(biography))
        .catch(err => res.status(500).json(err));


                    console.log('success only update ');
                    res.status(200).json("success");
                      User.updateOne({ _id: req.body.user_id },query1).then(user => {
                      console.log('success update user');
                      }).catch(err => console.log(err));
                  }).catch(err => console.log(err)); 
            }
        })
        .catch(err => console.log(err));

            console.log(query);
}


exports.createCivility = (req,res,next) =>{

let newBibliography = "";
let newFirstBibliography = new Bibliography({
       firstname: req.body.firstname,
       lastname: req.body.lastname,
       gender: req.body.gender,
       auteur: req.body.user_id
    }); 

if (req.body.edit_add == 'add_civility') {
	newBibliography = {
       firstname: req.body.firstname,
       lastname: req.body.lastname,
       gender: req.body.gender,
       birth_day: req.body.birth_day,
       birth_place: req.body.birth_place,
       civilite_visibilite: req.body.civilite_visibilite,
       marital_status: req.body.marital_status,
       birth_place: req.body.birth_place,
    };  
} 
 
Bibliography.find({ auteur: req.body.user_id}).count()
        .then(biblio_length => {
            if (biblio_length < 1) {
               newFirstBibliography.save()
                .then((bibliographie) => {
                  let query = { $push: newBibliography };
                  Bibliography.findByIdAndUpdate(bibliographie._id, query, { new: true, useFindAndModify: false })
                  console.log('success create before update '+bibliographie._id);
                }).catch(err => res.status(500).json(err));
            }else{
             Bibliography.find({ auteur: req.body.user_id})
        	     .then((bibliographie) => {
        	      let query = { $push: newBibliography };
                  Bibliography.findByIdAndUpdate(bibliographie._id, query, { new: true, useFindAndModify: false })
                  console.log('success not create before update '+bibliographie);
        	     })
       			 .catch(err => res.status(500).json(err));
            }
        })
        .catch(err => console.log(err));

}

exports.getAll = (req, res, next) => {
    Bibliography.find({})
        .sort({createAt: -1})
        .populate('creator')
        .then(topten => res.json(topten))
        .catch(err => res.status(500).json(err));
}

exports.deleteById = (req, res, next) => {
    Bibliography.deleteOne({_id: req.params.id})
        .then(topten => res.json(topten))
        .catch(err => res.status(500).json(err));
}

exports.getBiographyById = (req, res, next) => {
    Biography.find({ creator: req.params.id })
        .populate('creator')
        .then(biography => res.json(biography))
        .catch(err => res.status(500).json(err));
}

exports.allBiography = (req, res, next) => {
  //Biography.deleteMany({ status : "true" });
    Biography.find({})
        .sort({createAt: -1})
        .populate('creator')
        .then(topten => res.json(topten))
        .catch(err => res.status(500).json(err));
}