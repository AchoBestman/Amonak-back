const Categorie=require('../models/categorie');

exports.save = (req, res, next) => {
    const name = req.body.name;
    const description = req.body.description;
    const image = req.body.image;
    let new_category = new Categorie({
        name: name,
        description: description,
        image: image
    });    
    new_category.save()
        .then(category => res.json(category))
        .catch(err => res.status(500).json(err));
}

exports.find = (req, res, next) => {
    // res.sendFile(process.cwd()+"/assets/categories.json");
    Categorie.find({status: true})
        .then(categories => res.json(categories))
        .catch(err => res.status(500).json(err));
}

exports.findOne = (req, res, next) => {
    const id = req.params.id;
    Categorie.findOne({_id: id})
        .then(category => res.json(category))
        .catch(err => res.status(500).json(err));
}

exports.findOneAndUpdate = (req,res,next) => {
    const id = req.params.id;
    const name = req.body.name;
    const description = req.body.description;
    const image = req.body.image;
    let query = {};
    if(name) {
        query.name = name;
    }
    if(description) {
        query.description = description;
    }
    if(image) {
        query.image = image;
    }
    Categorie.findOneAndUpdate({_id: id})
        .then(categories=> res.json(categories))
        .catch(err => res.status(500).json(err));
}

exports.deleteOne = (req, res, next) => {
    const id = req.params.id;
    const query = { status: false };
    Categorie.findOneAndUpdate({_id: id}, query, { new: true, useFindAndModify: false })
        .then(category => res.json(category))
        .catch(err => res.status(500).json(err));
}

