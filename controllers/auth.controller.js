const config = require('../config/auth.config');
const User = require('../models/user');
const sendMail = require('../middlewares/mail');
const Token = require('../models/token');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const pusher = require('../middlewares/pusher');
const isOnline = require('is-online');
const { STATIC_URL } = require('../middlewares/api_url');

exports.signup = (req, res) => {
    
    const newUser = new User({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        isNewFeed: true,
        isFirstTime: true
    }); 

    bcrypt.genSalt(10, (err, salt) => { 
        if(err) throw err;
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            if(err) throw err;
            newUser.password = hash;
            newUser.save()
                .then(user => {
                    let data = {};
                    data.user = user;
                    data.type = 'activation';
                    data.subject = 'Activate your account';
                    
                    sendMail.send(data, req, res);

                    return res.status(200).json({data: data});
                    
                })
                .catch(err => {
                    return res.status(500).json({message: err});
                });
        });
    });
}  

exports.signin = (req, res) => {
   const query = req.body.query;
   console.log("::::::::::::::from signin query content ", query);
   console.log("::::::::::::::from signin query country content ", req.user_country_infos);
    let data = [];
    User.find(query, "-__v")
        .then(users => {
            if(!users || users === null || !users.length){
                return res.status(404).json({message: "User Not found."});
            }else{
                users.forEach(user => {
                    isMatch = bcrypt.compareSync(req.body.password, user.password);
                    if(isMatch){
                        data.push(user);
                    } 
                });
                if(data.length === 1){
                    if(data[0].status === false){
                        return res.status(401).json({message: 'Your account has not been verified.'});
                    }else{
                        let token = jwt.sign({ id: data[0]._id }, config.secret, {
                            expiresIn: 259200
                        });
                        User.findOneAndUpdate(query, {countryInfos: req.user_country_infos, is_log: true}, {new: true, useFindAndModify: false})
                            .populate('seller_info', "-__v -_id")
                            .then(user => {
                                // pusher.trigger('message', 'newConnection', {
                                //     data: user
                                // });
                                return res.json({
                                    accessToken: token,
                                    user: performData(user),
                                    user_country_infos: req.user_country_infos,
                                    expiresIn: 259200
                                })
                            })
                            .catch(err => {
                                return res.status(500).json(err);
                            })
                    }
                }else if(data.length > 1){
                    return res.json({
                        accessToken: null,
                        users: data
                    });
                }else{
                    return res.status(401).json({
                        accessToken: null,
                        message: "Invalid Password !"
                    });
                }
            }
        }
    );
};

exports.signinWith = (req, res, next) => {
    const query = req.body.query;
    User.findOne(query, "-__v")
        .then(user => {
            if(!user){
                return res.status(404).json({message: "Invalid Email !"});
            }else if(!bcrypt.compareSync(req.body.password, user.password)){
                return res.status(401).json({message: "Invalid Password !"});
            }else if(user.status === false){
                return res.status(401).json({message: 'Your account has not been verified.'});
            }else{
                let token = jwt.sign({ id: user._id }, config.secret, {
                    expiresIn: 259200
                });
                User.findOneAndUpdate({ email: req.body.email },{countryInfos: req.user_country_infos, is_log: true}, {new: true, useFindAndModify: false})
                    .populate("seller_info", "-__v _id")
                    .then(user1 => {
                        // pusher.trigger('message', 'newConnection', {
                        //     data: user1
                        // });
                        return res.json({
                            accessToken: token,
                            user: performData(user1),
                            user_country_infos: req.user_country_infos,
                            expiresIn: 259200
                        });
                    })
                    .catch(err => {
                        return res.status(500).json(err);
                    })
            }
        })
        .catch(err => {
            return res.status(500).json(err);
        })
};

exports.activation = (req, res, next) => {
    Token.findOne({token: req.params.token})
        .then(token => {
            console.log(token)
            if(!token){
                return res.status(400).json({
                    type: 'token-error',
                    message: 'We were unable to find a valid token. Your token may have expired.'});
            }else{
                User.findOneAndUpdate({_id: token._userId, status: false}, {status: true, countryInfos: req.user_country_infos}, {new: true, useFindAndModify: false})
                    .then(user => {
                        if(!user){
                            return res.status(400).json({
                                type: 'user-error',
                                message: 'This user\'s account is already activated'});
                        }else{
                            let token = jwt.sign({ id: user._id }, config.secret, {
                                expiresIn: 259200
                            });
                            req.userId = user._id;
                            next();     
                            return res.json({
                                accessToken: token,
                                user: performData(user),
                                user_country_infos: req.user_country_infos,
                                expiresIn: 259200
                            });
                        }
                    })
                    .catch(err => {
                        return res.status(500).json(err);
                    })
            }
        })
        .catch(err => {
            return res.status(500).json(err);
        })
}

exports.sendMail = (req, res, next) => {
    User.findOne({email: req.body.email})
        .then(user => {
            if(!user) {
                return res.status(400).json({message: 'email not found'});
            } else {
                let data = {};
                data.user = user;
                data.type = req.body.type;
                data.subject = req.body.subject;
                isOnline({ timeout: 1000 }).then(online => {
                        if (online) {
                            sendMail.send(data, req, res);
                            console.log('activate account mail is send to user with success')
                        }else{
                            console.log('activate account mail is not send to user because internet connection')
                        }
                })
                return res.json(user);
            }
        })
        .catch(err => {
            return res.status(500).json(err);
        })
}

exports.resetPassword = (req, res, next) => {
    let password;
    Token.findById(req.body.token)
        .then(token => {
            if(!token) {
                res.status(400).json({message: 'Invalide token !'});
            } else {
                bcrypt.genSalt(10, (err, salt) => {
                    if(err) throw err;
                    bcrypt.hash(req.body.password, salt, (err, hash) => {
                        if(err) throw err;
                        password = hash;
                        User.findByIdAndUpdate(token._userId, {password: password}, {new: true, useFindAndModify: false})
                            .populate("seller_info", "-__v _id")
                            .then(user => {
                                if(!user) {
                                    res.status(400).json({message: 'User not found !'});
                                } else {
                                    let token = jwt.sign({ id: user._id }, config.secret, {
                                        expiresIn: 259200
                                    });

                                    res.json({
                                        accessToken: token,
                                        user: performData(user),
                                        user_country_infos: req.user_country_infos,
                                        expiresIn: 259200
                                    });
                                }
                            })
                            .catch(err => res.status(500).json({message: err}));
                    });
                });
            }
        })
        .catch(err => res.status(500).json(err));
}

exports.checkTokenValidation = (req, res, next) => {
    Token.findById(req.params.token)
        .then(token => {
            if(!token) {
                res.status(400).json({message: 'Invalid token !'});
            } else {
                res.json({message: 'Valid token'});
            }
        })
        .catch(err => res.status(500).json(err));
}

exports.logout = (req, res, next) => {
    User.findOneAndUpdate({ _id: req.body._id },{is_log: false}, {new: true, useFindAndModify: false})
        .then(user => {
            // pusher.trigger('message', 'connectionClose', {
            //     data: user
            // });
            return res.json({message: "success."});
        })
        .catch(err => {
            return res.status(500).json(err);
        });
}

function performData(user) {
	if(user.length) {
		return user.map(elt => {
			if(elt && elt.avatar) {
				elt.avatar = `${STATIC_URL}/static${elt.avatar.split('/static').length > 1 ? elt.avatar.split('/static')[1]: elt.avatar.split('/static')[0]}`;
            }
            if(elt.seller_info && elt.seller_info.identity_card && elt.seller_info.identity_card.url) {
                elt.seller_info.identity_card.url = `${STATIC_URL}/static${elt.seller_info.identity_card.url.split('/static').length > 1 ? elt.seller_info.identity_card.url.split('/static')[1]: elt.seller_info.identity_card.url.split('/static')[0]}`;
            }
            return elt;
        })
    }
	if(user && user.avatar) {
		user.avatar = `${STATIC_URL}/static${user.avatar.split('/static').length > 1 ? user.avatar.split('/static')[1]: user.avatar.split('/static')[0]}`;
	}
    if(user.seller_info && user.seller_info.identity_card && user.seller_info.identity_card.url) {
        user.seller_info.identity_card.url = `${STATIC_URL}/static${user.seller_info.identity_card.url.split('/static').length > 1 ? user.seller_info.identity_card.url.split('/static')[1]: user.seller_info.identity_card.url.split('/static')[0]}`;
    }
	return user;
}