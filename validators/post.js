const Joi = require('joi');

const validators = (req, res, next) => {
    // create schema object
    let body = req.body
    
    const schema = Joi.object({
        content: Joi.string().min(10).required(),
        user_id: Joi.string().required()
    });

    // schema options
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(body, options);

    if (error) {
        // on fail return comma separated errors
        return res.status(401).json({
            "validators": error.details.map(x => {
               return {
                    key: x.path[0],
                    message : x.message.replace('"', '').replace('"', '')
                }
            })
        });

    } else {
        // on success replace req.body with validated value and trigger next middleware function
        req.body = value;
        next();
    }
};

module.exports = validators
