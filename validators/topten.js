const Joi = require('joi');

const validators = (req, res, next) => {
    // create schema object
    let body = req.body
    if (req.files) {
        body.files = req.files
    }
    if (!body.message) {
        body.message = '.'
    }

    const schema = Joi.object({
        compagny: Joi.string().min(2).max(50).required(),
        duration: Joi.string().required(),
        message: Joi.string().max(150),
        files: Joi.array().items(Joi.object({
                fieldname: Joi.string().valid('multi-files'),
                originalname: Joi.string(),
                encoding: Joi.string().valid('7bit'),
                destination: Joi.string(),
                mimetype: Joi.string(),
                filename: Joi.string(),
                path: Joi.string(),
                size: Joi.number()
              })).required().min(1).max(5),
        user_id: Joi.string().required()
    });

    // schema options
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(body, options);

    if (error) {
        // on fail return comma separated errors
        return res.status(401).json({
            "validators": error.details.map(x => {
               return {
                    key: x.path[0],
                    message : x.message.replace('"', '').replace('"', '')
                }
            })
        });

    } else {
        // on success replace req.body with validated value and trigger next middleware function
        req.body = value;
        next();
    }
};

module.exports = validators
