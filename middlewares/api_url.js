// cconst API_URL = 'https://amonak.com:3000';
const API_URL = 'https://localhost:3000/api';
// const STATIC_URL = 'http://amonak.com:3000';
const STATIC_URL = 'http://localhost:3000';
const FRONT_URL = 'https://localhost:4200';

module.exports = { API_URL, FRONT_URL, STATIC_URL};
