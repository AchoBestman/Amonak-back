const multer =require('multer');
const path = require('path');
const util = require('util');
const fs = require('fs');
const MIME_TYPES = {
    'image/jpg': 'jpg',
    'image/jpeg': 'jpeg',
    'image/png': 'png',
    'image/bmp': 'bmp',
    'image/gif': 'gif',
    'image/x-icon': 'ico',
    'audio/aac': 'aac',
    'audio/midi': 'midi',
    'audio/x-wav': 'wav',
    'audio/3gpp': '3gp',
    'audio/3gpp2': '3g2',
    'video/x-msvideo': 'avi',
    'video/mpeg': 'mpeg',
    'video/ogg': 'ogv',
    'video/webm': 'webm',
    'video/3gpp': '3gp',
    'video/3gpp2': '3g2'
};

const uploadStorage = multer.diskStorage({
    destination: (req, file, callBack) => {
        let dest;
        if(file.mimetype.match('video/*')) {
            // dest = "/data/videos/uploads/";
            dest = path.resolve('./public/videos/uploads/');
        } else if(file.mimetype.match('image/*')) {
            // dest = "/data/images/uploads/";
            dest = path.resolve('./public/images/uploads/');
        } else  if(file.mimetype.match('audios/*')) {
            // dest = "/data/audios/uploads/";
            dest = path.resolve('./public/audios/uploads/');
        } else {
            // dest = "/data/documents/uploads/";
            dest = path.resolve('./public/documents/uploads/');
        }
        fs.mkdirSync(dest, { recursive: true });
        callBack(null, dest);
    },
    filename: (req, file, callBack)=>{
        const name = file.originalname.split(' ').join('_').split('.')[0].substring(0,5);
        console.log(name);
        const extension = MIME_TYPES[file.mimetype];
        console.log(extension);
        // callBack(null, name + Date.now() + '.' + extension);
        callBack(null, file.originalname);
    }
});

const avatarStorage = multer.diskStorage({
    destination: (req, file, callBack) => {
        // let dest = "/data/images/avatar";
        let dest = path.resolve('./public/images/avatar');
        console.log('destination:::::', dest);
        fs.mkdirSync(dest, { recursive: true });
        callBack(null, dest);
    },
    filename: (req, file, callBack)=>{
        // const name = file.originalname.split(' ').join('_').split('.')[0];
        const name = file.originalname.split(' ').join('_').split('.')[0].substring(0,5);
        const extension = MIME_TYPES[file.mimetype];
        console.log(extension);
        // callBack(null, name + Date.now() + '.' + extension);
        callBack(null, file.originalname);
    }
});

const upload = multer({storage: uploadStorage});
const avatarUpload = multer({
    fileFilter: (req, file, cb) => {
        // if(file.mimetype.match('/\.(png|PNG|jpg|JPG|jpeg|JPEG)$/')) {
        if(file.mimetype.match('image/*') || file.mimetype.match('application/octet-stream')) {
            cb(null, true);
        } else {
            cb(new multer.MulterError('not image file.'));
        }
    },
    storage: avatarStorage
});
const uploadFiles = multer({ storage: uploadStorage }).array("multi-files", 10);
const uploadMultipleMiddleware = util.promisify(uploadFiles);

const multipleUpload = async (req, res, next) => {
    try {
        await uploadMultipleMiddleware(req,res);
    }catch(error){
        console.log(error);
        res.status(500).json(error);
        return
    }
    next();
};

module.exports = {
    avatarUpload,
    multipleUpload
}
