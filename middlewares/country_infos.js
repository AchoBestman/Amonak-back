const request = require('request')
const isOnline = require('is-online')

module.exports = (req, res, next) => {
    console.log("::::::::::::::::::::::::::::::::::::::::");
    console.log(req.socket.remoteAddress);
    console.log("::::::::::::::::::::::::::::::::::::::::");
    isOnline({ timeout: 1000 }).then(online => {
        if (online) {
            request("https://api64.ipify.org/", function(err,response,body) {
             request("https://api.ipgeolocation.io/ipgeo?apiKey=43fb53baa3cc4c5aa21f55e2ece612a9&ip="+req.socket.remoteAddress, 
                    function(err,response,body) {
                        req.user_country_infos = JSON.parse(body);
                        next();
                    })
                    
            })    
        }else{
            next();
        }
    });
};   