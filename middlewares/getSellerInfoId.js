const sellerInfo = require('../models/sellerInfo');
const SellerInfo = require('../models/sellerInfo');

getSellerInfoId = (req, res, next) => {
    const id = req.params.user_id;

    SellerInfo.findOne({creator: id})
        .then(info => {
            req.body.seller_id = info._id;
            console.log('id found:::::::::', info);
            next();
        }).catch(err => res.status(500).json(err))
}

module.exports = getSellerInfoId;
