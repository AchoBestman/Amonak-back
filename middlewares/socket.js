const User = require('../models/user');
const Message = require('../models/message');
const pusher = require('./pusher');
const { STATIC_URL } = require('../middlewares/api_url');

module.exports = (io) => {
    io.on('connection', socket => {
        console.log('one device connected.');
        socket.on('search', (data) => {
            console.log(data);
            io.emit("ok");
        })
        socket.on('join', (data)=>{
            console.log(data);
            let query = {};
            if(data.receiver) {
                query = { $or: [{ from: data.sender, to: data.receiver }, { from: data.receiver, to: data.sender }] };
            } else {
                query = { $or: [{ from: data.sender }, { to: data.sender }] };
            }
            Message.find(query)
                .populate('from', 'username avatar sectors is_log')
                .populate('to', 'username avatar sectors is_log')
                .then(msg => {
                    msg.forEach((elt, index) => {
                        if(elt.from._id.toString() === data.sender) {
                            // msg[index] = {'files': elt.files, 'content': elt.content, 'user': elt.to, 'created_at': elt.created_at, 'status': elt.read_at, 'reply': true, 'nbMsg': 0, 'is_log': elt.to.is_log};
                            msg[index] = {'from': elt.from, 'to': elt.to, 'files': elt.files, 'content': elt.content, 'user': elt.to, 'reply': true, 'created_at': elt.created_at, '_id': elt._id, "state": elt.status, 'deleters': elt.deleters };
                        } else {
                            // msg[index] = {'files': elt.files, 'content': elt.content, 'user': elt.from, 'created_at': elt.created_at, 'status': elt.read_at, 'reply': false, 'nbMsg': 0, 'is_log': elt.from.is_log};
                            msg[index] = {'from': elt.from, 'to': elt.to, 'files': elt.files, 'content': elt.content, 'user': elt.from, 'reply': false, 'created_at': elt.created_at, '_id': elt._id, "state": elt.status, 'deleters': elt.deleters };
                        }
                    });
                    socket.emit('msg',performData(msg));
                })
                .catch(err => socket.emit('msg', []));
        });

        socket.on('new', (data)=>{
            console.log(data);
            let newMessage;
            const content = data.content;
            const from = data.sender;
            const to = data.receiver;
            newMessage = new Message({
                content: content,
                from: from,
                to: to
            });
            if(data.files) {
                let files = data.files;
                files.forEach(element => {
                    if(element.type.match('image/*')){
                        newMessage.files.push({
                            url: `${ STATIC_URL }/static/images/uploads/${element.name}`,
                            type: element.type
                        });
                    } else if(element.type.match('video/*')) {
                        newMessage.files.push({
                            url: `${ STATIC_URL }/static/videos/uploads/${element.name}`,
                            type: element.type
                        });
                    }
                });
            }
            newMessage.save()
                .then(msg => {
                    msg.populate('from', 'username avatar sectors is_log')
                        .populate('to', 'username avatar sectors is_log', (err, data) => {
                            if(err) throw err
                            let msg = {...data.toJSON()};
                            msg.user = data.to;
                            // msg.files = data.files;
                            // msg.user = data.to;
                            // msg.content = data.content;
                            // msg.created_at = data.created_at;
                            // msg.status = data.read_at;
                            // msg.reply = false;
                            // msg.is_log = data.is_log;
                            // msg.from = data.from;
                            // msg.to = data.to;
                            // msg._id = data._id;
                            // msg.state = data.status;
                            // msg.deleters = data.deleters;
                            // data.user = data.to;
                            console.log('===============================', msg);
                            io.emit('newMessage', performData(msg));
                        })
                })
                .catch(err => console.log(err));
        });

        socket.on('typing', (data) => {
            console.log(data);
            io.emit("typing", data);
        })

        socket.on('disconnect', (reason) => {
            console.log("**********************");
            console.log(reason);
            console.log("**********************");
        })
    });
}

function performData(message) {
    if(message.length) {
        return message.map(elt => {
            if(elt.from && elt.from.avatar) {
                elt.from.avatar = `${STATIC_URL}/static${elt.from.avatar.split('/static').length > 1 ? elt.from.avatar.split('/static')[1]: elt.from.avatar.split('/static')[0]}`;
            }
            if(elt.to && elt.to.avatar) {
                elt.to.avatar = `${STATIC_URL}/static${elt.to.avatar.split('/static').length > 1 ? elt.to.avatar.split('/static')[1]: elt.to.avatar.split('/static')[0]}`;
            }
            if(elt.user && elt.user.avatar) {
                elt.user.avatar = `${STATIC_URL}/static${elt.user.avatar.split('/static').length > 1 ? elt.user.avatar.split('/static')[1]: elt.user.avatar.split('/static')[0]}`;
            }
            if(elt.files && elt.files.length) {
                elt.files.forEach(file => {
                    file.url = `${STATIC_URL}/static${file.url.split('/static').length > 1 ? file.url.split('/static')[1]: file.url.split('/static')[0]}`;
                })
            }
            return elt;
        })
    }
    if(message.from && message.from.avatar) {
        message.from.avatar = `${STATIC_URL}/static${message.from.avatar.split('/static').length > 1 ? message.from.avatar.split('/static')[1]: message.from.avatar.split('/static')[0]}`
    }
    if(message.to && message.to.avatar) {
        message.to.avatar = `${STATIC_URL}/static${message.to.avatar.split('/static').length > 1 ? message.to.avatar.split('/static')[1]: message.to.avatar.split('/static')[0]}`
    }
    if(message.user && message.user.avatar) {
        message.user.avatar = `${STATIC_URL}/static${message.user.avatar.split('/static').length > 1 ? message.user.avatar.split('/static')[1]: message.user.avatar.split('/static')[0]}`;
    }
    if(message.files && message.files.length) {
        message.files.forEach(file => {
            file.url = `${STATIC_URL}/static${file.url.split('/static').length > 1 ? file.url.split('/static')[1]: file.url.split('/static')[0]}`;
        })
    }
    return message;
}