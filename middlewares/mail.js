const nodemailer = require('nodemailer');
const crypto = require('crypto');
const Token = require('../models/token');
const cryptoRandomString = require('crypto-random-string');
const Email = require('email-templates');
const path = require('path');
const {API_URL, STATIC_URL} = require('../middlewares/api_url');

const email = new Email({
    message: {
        from: 'amonak.snsf@gmail.com'
    },
    send: true,
    juice: true,
    juiceResources: {
        preserveImportant: true,
        webResources: {
            relativeTo: path.resolve('public')
        }
    }
}); 
 
exports.send = (data, req, res, next) => {
    let user = data.user;
    let subject = data.subject;
    let type = data.type;

    let token = new Token({
        _userId: user._id,
        token: cryptoRandomString({length: 6, type: 'numeric'})
    });
    let transport = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        // port: 465,
        port: 587,
        secure: false,
        auth: {
            user: 'berniprod652@gmail.com',
            pass: 'rwnpkxjulfnvmetk'
        }
    });
    let mailOptions = {
        from: '"AMONAK Team" <amonak-snsf@gmail.com>',
        to: user.email,
        subject: subject,
        // attachments: [
        //     {
        //         filename: '',
        //         path: path.resolve('./public/img/uploads/avatar.png')
        //     }
        // ]
    };
    if(data.attachments) {
        mailOptions.attachments = data.attachments;
    }
    let link;
    let t='';
    let baseUrl = STATIC_URL;

    if(type === 'reset') {
        link = req.headers.origin + '\/auth\/reset-password\/' + token._id + '\n';
        t = token._id;
    } else {
        link = req.headers.origin + '\/auth\/activation\n';
        t = token.token;
    }
    email.render('email/'+type, {username: user.username.toUpperCase(), token: t, link: link, baseUrl: baseUrl})
        .then((msg) => {
            mailOptions.html = msg;
            token.save()
                .then((token) => {
                    transport.sendMail(mailOptions, (error, info) => {
                        if(error){
                            res.json({message: error});
                            return
                        }else{
                            res.json(user);
                            return
                        }
                    });
                })
                .catch(err => res.status(500).json(err));
        })
        .catch(err => res.status(500).json(err));
}

exports.sendRequest = (data, req, res, next) => {
    let sender = data.data.sender;
    let subject = "Someone is interesting about your top ten publication";
    let receiver = data.data.receiver;
    let creator = data.data.creator;
    let imgs = [];

    if (data.topten.image) {
        data.topten.image.forEach((element, index) => {
            imgs.push(element.url)
        })
    }
    let transport = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        // port: 465,
        port: 587,
        secure: false,
        auth: {
            user: 'berniprod652@gmail.com',
            pass: 'jfnqzafpflsrojws'
        }
    });
    let mailOptions = {
        from: '"Team Amonak 👻" <berniprod652@gmail.com>',
        to: receiver,
        subject: subject
    };
    if(data.attachments) {
        mailOptions.attachments = attachments;
    }
    let baseUrl = STATIC_URL;
    let link = req.headers.origin + '\/profile\/' + creator + '\n';
    email.render('email/interesting', {link: link, sender: sender, baseUrl: baseUrl, imgs: imgs}) 
        .then((msg) => {
            mailOptions.html = msg;
            transport.sendMail(mailOptions, (error, info) => {
                if(error){
                    console.log(error)
                    res.json({message: error});
                    return
                }else{
                    console.log(imgs)
                    res.json("mail sended !");
                    return
                }
            });
        })
        .catch(err => res.status(500).json(err));
}