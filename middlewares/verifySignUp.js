const User = require('../models/user');

const usernameRegex = RegExp('^[a-zA-Z]([a-zA-Z0-9_])?([a-zA-Z0-9_ ])*$');
const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

checkSignUpData = (req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;
    const email = req.body.email;
    if(!(username && password && email) || !(username.length && password.length && email.length)) {
        return res.status(400).json({message: 'Data required.'});
    }
    if(!usernameRegex.test(username)) {
        return res.status(400).json({message: 'Username invalid.'});
    } 
    if(!(emailRegexp.test(email) && email.length < 255)) {
        return res.status(400).json({ message: 'Email invalid.'});
    }
    User.findOne({email: email})
        .then(user => {
            if(user){
                return res.status(400).json({message: 'Email already in use.'});
            }else{
                next();
            }
        })
        .catch(err => {
            return res.status(500).json({message: err});
        }
    );
}

checkSignInData = (req, res, next) => {
    const username = req.body.username;
    let query;
    if(!(username && username.length)) {
        return res.status(400).json({message: 'Usernarme required.'});
    }
    if(emailRegexp.test(username)) {
        query = {email: username};
    }else {
        query = {username: username};
    }
    req.body.query = query;
    next();
}

checkEmail = (req, res, next) => {
    const email = req.body.email;
    User.findOne({ email: email})
        .then(user => {
            if(user) {
                res.json(true);
            } else {
                res.json(false);
            }
        })
        .catch(err => res.status(500).json(err));
}

const verifySignUp = {
    checkSignUpData,
    checkSignInData,
    checkEmail
};

module.exports =  verifySignUp;
