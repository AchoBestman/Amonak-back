var express = require('express');
var router = express.Router();
const fs = require('fs');
const rpath = require('path');
const { STATIC_URL } = require('../middlewares/api_url');
const Product = require('../models/product');
const TopTen = require('../models/topten');
const { Publication, SalePublication } = require('../models/publication');
const User = require('../models/user');
const sellerRequest = require('../models/sellerRequest');
const sellerInfo = require('../models/sellerInfo');
const message = require('../models/message');
const alertReply = require('../models/alertReply');
const categorie = require('../models/categorie');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Welcome to Amonak' });
    // res.sendFile(process.cwd()+"/amonak-front/dist/web/index.html")
});

router.get('/index/:id', (req, res, next) => {
    SalePublication.find({product: req.params.id})
        .populate({
            path: 'publication',
            populate: { path: 'creator', select: 'sectors avatar username is_log' },
        })
        .populate('product', '-owner -__v -status')
        .populate('creator', 'username sectors avatar is_log')
        .populate({
            path: 'comments',
            populate: { path: 'creator', select: 'sectors avatar username is_log' },
            select: 'likes creator content'
        })
        .populate({
            path: 'comments',
            populate: { path: 'likes.creator' }
        })
        .populate('likes.creator', 'username sectors avatar is_log')
        .then(s => res.json(s))
        .catch(err => res.status(500).json(err))
    // res.render('index', { title: 'Express' });
});

router.post('/index', (req, res, next) => {
    console.log(req.query)
    res.render('index', { title: 'Express' });
    
})

router.get('/publication/berniprod', (req, res, next) => {
    Publication.find()
        .then(pubs => {
            let files = [];
            let f = [];
            pubs.forEach(elt => {
                // console.log("::::publication ::::", elt);
                f = [];
                if(elt.files.length) {
                    elt.files.forEach(e => {
                        let file = {};
                        // file.url = STATIC_URL+"/static"+e.url.split('static')[1];
                        file.url = e.url.split('static')[1];
                        file.url = file.url.split(/\s/).join('').split("%20").join('');
                        file.type = e.type;
                        f.push(file);
                    })
                    // console.log("::::publication files ::::", files);
                    files.push(f);
                    let query = {files: f};
                    console.log(query);
                    Publication.findByIdAndUpdate(elt._id, query, {new: true, useFindAndModify: false})
                        .then((t) => console.log(t))
                        .catch(err => res.status(501).json(err));
                }
            })
            res.json(pubs);
        })
        .catch(err => res.status(500).json(err));
})
router.get('/publication/:id/:oldName/:newName/berniprod', (req, res, next) => {
    const name = req.params.newName;
    Publication.findById(req.params.id)
        .then(pub => {
            let files = [];
            let f = {};
            if(pub && pub.files) {
                files = pub.files;
                files.forEach(elt => {
                    if(elt.url === req.params.oldName) {
                        elt.url = name;
                    }
                })
                console.log('files:::', files);
                let query = { files: files };
                Publication.findByIdAndUpdate(req.params.id, query, { new: true, useFindAndModify: false })
                    .then(t => {return res.json(t);})
                    .catch(err => {return res.status(500).json(err);})
            }
            return res.json("Not found:::::");
        })
        .catch(err => {return res.status(500).json(err);});
})

router.get('/product/berniprod', (req, res, next) => {
    Product.find()
        .then(pubs => {
            let files = [];
            let f = [];
            pubs.forEach(elt => {
                // console.log("::::publication ::::", elt);
                f = [];
                if(elt.files.length) {
                    elt.files.forEach(e => {
                        let file = {};
                        // file.url = STATIC_URL+"/static"+e.url.split('static')[1];
                        file.url = e.url.split('static')[1];
                        file.url = file.url.split(/\s/).join('').split("%20").join('');
                        file.type = e.type;
                        f.push(file);
                    })
                    // console.log("::::publication files ::::", files);
                    files.push(f);
                    let query = {files: f};
                    console.log(query);
                    Product.findByIdAndUpdate(elt._id, query, {new: true, useFindAndModify: false})
                        .then((t) => console.log(t))
                        .catch(err => res.status(501).json(err));
                }
            })
            res.json(pubs);
        })
        .catch(err => res.status(500).json(err));
})

router.get('/seller-request/berniprod', (req, res, next) => {
    sellerRequest.find()
        .then(pubs => {
            let files = [];
            let f = [];
            pubs.forEach(elt => {
                // console.log("::::publication ::::", elt);
                f = [];
                if(elt.files.length) {
                    elt.files.forEach(e => {
                        let file = {};
                        // file.url = STATIC_URL+"/static"+e.url.split('static')[1];
                        file.url = e.url.split('static')[1];
                        file.url = file.url.split(/\s/).join('').split("%20").join('');
                        file.type = e.type;
                        f.push(file);
                    })
                    // console.log("::::publication files ::::", files);
                    files.push(f);
                    let query = {files: f};
                    console.log(query);
                    sellerRequest.findByIdAndUpdate(elt._id, query, {new: true, useFindAndModify: false})
                        .then((t) => console.log(t))
                        .catch(err => res.status(501).json(err));
                }
            })
            res.json(pubs);
        })
        .catch(err => res.status(500).json(err));
})

router.get('/message/berniprod', (req, res, next) => {
    message.find()
        .then(pubs => {
            let files = [];
            let f = [];
            pubs.forEach(elt => {
                // console.log("::::publication ::::", elt);
                f = [];
                if(elt.files.length) {
                    elt.files.forEach(e => {
                        let file = {};
                        // file.url = STATIC_URL+"/static"+e.url.split('static')[1];
                        file.url = e.url.split('static')[1];
                        file.url = file.url.split(/\s/).join('').split("%20").join('');
                        file.type = e.type;
                        f.push(file);
                    })
                    // console.log("::::publication files ::::", files);
                    files.push(f);
                    let query = {files: f};
                    console.log(query);
                    message.findByIdAndUpdate(elt._id, query, {new: true, useFindAndModify: false})
                        .then((t) => console.log(t))
                        .catch(err => res.status(501).json(err));
                }
            })
            res.json(pubs);
        })
        .catch(err => res.status(500).json(err));
})

router.get('/alert-reply/berniprod', (req, res, next) => {
    alertReply.find()
        .then(pubs => {
            let files = [];
            let f = [];
            pubs.forEach(elt => {
                // console.log("::::publication ::::", elt);
                f = [];
                if(elt.files.length) {
                    elt.files.forEach(e => {
                        let file = {};
                        // file.url = STATIC_URL+"/static"+e.url.split('static')[1];
                        file.url = e.url.split('static')[1];
                        file.url = file.url.split(/\s/).join('').split("%20").join('');
                        file.type = e.type;
                        f.push(file);
                    })
                    // console.log("::::publication files ::::", files);
                    files.push(f);
                    let query = {files: f};
                    console.log(query);
                    alertReply.findByIdAndUpdate(elt._id, query, {new: true, useFindAndModify: false})
                        .then((t) => console.log(t))
                        .catch(err => res.status(501).json(err));
                }
            })
            res.json(pubs);
        })
        .catch(err => res.status(500).json(err));
})

router.get('/topten/berniprod', (req, res, next) => {
    TopTen.find()
        .then(pubs => {
            let files = [];
            let f = [];
            pubs.forEach(elt => {
                // console.log("::::publication ::::", elt);
                f = [];
                if(elt.image.length) {
                    elt.image.forEach(e => {
                        let file = {};
                        // file.url = STATIC_URL+"/static"+e.url.split('static')[1];
                        file.url = e.url.split('static')[1];
                        file.url = file.url.split(/\s/).join('').split("%20").join('');
                        file.contentType = e.contentType;
                        f.push(file);
                    })
                    // console.log("::::publication files ::::", files);
                    files.push(f);
                    let query = {image: f};
                    console.log(query);
                    TopTen.findByIdAndUpdate(elt._id, query, {new: true, useFindAndModify: false})
                        .then((t) => console.log(t))
                        .catch(err => res.status(501).json(err));
                }
            })
            res.json(pubs);
        })
        .catch(err => res.status(500).json(err));
})

router.get('/user/berniprod', (req, res) => {
    User.find()
        .then(users => {
            let avatars = [];
            users.forEach(user => {
                // let avatar = STATIC_URL+"/static"+user.avatar.split('static')[1];
                let avatar = user.avatar.split('static')[1];
                User.findByIdAndUpdate(user._id, {avatar: avatar}, {new:true, useFindAndModify:false})
                    .then(t => avatars.push(t.avatar))
                    .catch(err => res.status(501).json(err))
                // avatars.push(avatar);
            })
            res.json(users);
        })
        .catch(err => res.status(500).json(err));
})
router.get('/seller-info/berniprod', (req, res) => {
    sellerInfo.find()
        .then(users => {
            let avatars = [];
            users.forEach(user => {
                let avatar = {};
                // avatar.url = STATIC_URL+"/static"+user.identity_card.url.split('static')[1];
                avatar.url = user.identity_card.url.split('static')[1];
                avatar.type = user.identity_card.type;
                sellerInfo.findByIdAndUpdate(user._id, {identity_card: avatar}, {new:true, useFindAndModify:false})
                    .then(t => avatars.push(t.avatar))
                    .catch(err => res.status(501).json(err))
                // avatars.push(avatar);
            })
            res.json(users);
        })
        .catch(err => res.status(500).json(err));
})

// router.get('/categorie/berniprod', (req, res) => {
//     categorie.find()
//         .then(users => {
//             let avatars = [];
//             users.forEach(user => {
//                 let avatar = STATIC_URL+"/static"+user.image.split('static')[1];
//                 categorie.findByIdAndUpdate(user._id, {image: avatar}, {new:true, useFindAndModify:false})
//                     .then(t => avatars.push(t.avatar))
//                     .catch(err => res.status(501).json(err))
//                 // avatars.push(avatar);
//             })
//             res.json(users);
//         })
//         .catch(err => res.status(500).json(err));
// })

// router.get('/video', (req, res, next) => {
//     const path = rpath.resolve('public/videos/uploads/video.mp4');
//     const stat = fs.statSync(path)
//     const fileSize = stat.size
//     const range = req.headers.range

//     if (range) {
//         const parts = range.replace(/bytes=/, "").split("-")
//         const start = parseInt(parts[0], 10)
//         const end = parts[1]
//         ? parseInt(parts[1], 10)
//         : fileSize-1

//         if(start >= fileSize) {
//         res.status(416).send('Requested range not satisfiable\n'+start+' >= '+fileSize);
//         return
//         }
        
//         const chunksize = (end-start)+1
//         const file = fs.createReadStream(path, {start, end})
//         const head = {
//         'Content-Range': `bytes ${start}-${end}/${fileSize}`,
//         'Accept-Ranges': 'bytes',
//         'Content-Length': chunksize,
//         'Content-Type': 'video/mp4',
//         }

//         res.writeHead(206, head)
//         file.pipe(res)
//     } else {
//         const head = {
//         'Content-Length': fileSize,
//         'Content-Type': 'video/mp4',
//         }
//         res.writeHead(200, head)
//         fs.createReadStream(path).pipe(res)
//     }
// });

module.exports = router;
