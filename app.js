const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const categorieRouter = require('./api/categorie.routes');


const socket = require('./middlewares/socket');
const authRouter = require('./api/auth.routes');
const userRouter = require('./api/user.routes');
const publicationRouter = require('./api/publication.routes');
const messageRouter = require('./api/message.routes');
const searchRouter = require('./api/search.routes');
const commentRouter = require('./api/comment.routes');
const productRouter = require('./api/product.routes');
const biographyRouter = require('./api/biography.routes');
const newsletterRouter = require('./api/newsletter.routes');

var indexRouter = require('./routes/index');
var usersaRouter = require('./api/user_a');
var toTenRouter = require('./api/topten');
var panierRouter = require('./api/panier');
var contact = require('./api/contact');
var payementRouter = require('./api/payement');

const mentionRouter = require('./api/mention');
const amisRouter = require('./api/amis');
const likeRouter = require('./api/like');
const alertRouter = require('./api/alertReply.routes');
const notificationRouter = require('./api/notification.routes');
const becomeSellerRouter = require('./api/sellerRequest.routes');
const fileUploaderRouter = require('./controllers/files.uploader');
const adminRoutes = require('./api/admin.seller.routes');
require('dotenv').config();
const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('./swagger_output.json')
const app = express(); 

const isLog = require('./middlewares/routes.secure');
//swagger documentation endpoint
app.use('/amonak/api-documentation', swaggerUi.serve, swaggerUi.setup(swaggerFile))
// app.use(cors(corsOptions));
app.use(cors());

//for swagger interface generated
//app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerFile))
// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

 
// //CORS bypass
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, x-access-token");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
  next();
});
 
app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
//app.use('/static', express.static('public'));
 app.use('/static', express.static(path.join(__dirname, 'public')));
// app.use(express.static(process.cwd()+"/../amonak-web/dist/web/"));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
//app.set('view engine', 'jade');
//app.set('view engine', 'pug');
app.use(express.json());
app.use('/favicon.ico', express.static('images/favicon.ico'));
console.log(`${process.env.DB_CONNEXION_STRING}`);
// mongoose.connect(`${process.env.DB_CONNEXION_STRING}`,{useNewUrlParser:true, useUnifiedTopology:true, useCreateIndex: true});
mongoose.connect("mongodb+srv://root:MAlolo1234@amonak-back-5wcd0.mongodb.net/back?retryWrites=true&w=majority",{useNewUrlParser:true, useUnifiedTopology:true, useCreateIndex: true});
// mongoose.connect("mongodb+srv://jeanphilippe:reussite2020@amonakbase-wnwzb.gcp.mongodb.net/back?retryWrites=true&w=majority",{useNewUrlParser:true, useUnifiedTopology:true, useCreateIndex: true});
// mongoose.connect("mongodb://localhost:27017/amonaktest",{useNewUrlParser:true, useUnifiedTopology:true, useCreateIndex: true});

const connection=mongoose.connection;
connection.once('open',()=>{
    console.log("Mongodb database connection successfully");
});

// app.use('/', indexRouter);
// app.use('/api', indexRouter);
// app.use('/api/auth', authRouter);
// app.use('/api/user', isLog, userRouter);
// app.use('/api/publication', isLog, publicationRouter);
// app.use('/api/upload', fileUploaderRouter);
// app.use('/api/message', isLog, messageRouter);
// app.use('/api/search', isLog, searchRouter);
// app.use('/api/comment', isLog, commentRouter);
// app.use('/api/product', isLog, productRouter);
// // app.use('/api/category', isLog, categorieRouter);
// app.use('/api/category',  categorieRouter);
// app.use('/api/biography', isLog, biographyRouter);
// app.use('/api/newsletter', isLog, newsletterRouter);
// app.use('/api/payement', payementRouter);

// // app.use('/', indexRouter);
// app.use('/api/user_a', isLog, usersaRouter);
// app.use('/api/topten', isLog, toTenRouter)
// app.use('/api/panier', isLog, panierRouter)
// app.use('/api/delivery', isLog, contact)
// app.use('/api/amis', isLog, amisRouter)
// app.use('/api/alert', isLog, alertRouter);
// app.use('/api/become-seller', isLog, becomeSellerRouter);
// // app.use('/api/notification', isLog, notificationRouter);
// app.use('/api/notification',  notificationRouter);

// app.use('/admin', isLog, adminRoutes);

//This simply adds socket.io to res in our event loop


// catch 404 and forward to error handler
// app.use(function (req, res, next) {
//   next(createError(404));
// });

// // error handler
// app.use(function (err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });


module.exports = app;

