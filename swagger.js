const swaggerAutogen = require('swagger-autogen')()


const doc = {
    info: {
        version: "1.0.0",
        title: "AMONAK WEB API DOCUMENTATION",
        description: "Documentation des routes api d'amonak"
    },
    host: "localhost:3000",
    basePath: "/",
    schemes: ['http', 'https'],
    consumes: ['application/json'],
    produces: ['application/json'],
    tags: [
        {
            "name": "AMONAK",
            "description": "Endpoints"
        }
    ],
    securityDefinitions: {
        api_key: {
            type: "apiKey",
            name: "api_key",
            in: "header"
        },
        petstore_auth: {
            type: "oauth2",
            authorizationUrl: "https://petstore.swagger.io/oauth/authorize",
            flow: "implicit",
            scopes: {
                read_pets: "read your pets",
                write_pets: "modify pets in your account"
            }
        }
    },
    definitions: {
        Project: {
            initiator: "JEAN PHILIPPE",
            project_name: "AMONAK"
        },
        Dev1: {
            name: "JEAN PHILIPPE",
            age: 20,
            parents: {
                $ref: '#/definitions/Project'
            },
            diplomas: [
                {
                    school: "XYZ University",
                    year: 2020,
                    completed: true,
                    internship: {
                        hours: 290,
                        location: "Tunisie"
                    }
                }
            ]
        },
        Dev2: {
            name: "BERNARD SOMBORO",
            age: 20,
            parents: {
                $ref: '#/definitions/Project'
            },
            diplomas: [
                {
                    school: "XYZ University",
                    year: 2020,
                    completed: true,
                    internship: {
                        hours: 290,
                        location: "Tunisie"
                    }
                }
            ]
        },
        Dev3: {
            name: "BAKAYOKO ISSOUF",
            age: 20,
            parents: {
                $ref: '#/definitions/Project'
            },
            diplomas: [
                {
                    school: "XYZ University",
                    year: 2020,
                    completed: true,
                    internship: {
                        hours: 290,
                        location: "Tunisie"
                    }
                }
            ]
        },
        Dev4: {
            name: "AIKPE ACHILE",
            age: 25,
            parents: {
                $ref: '#/definitions/Project'
            },
            diplomas: [
                {
                    school: "ESGT BENIN",
                    year: 2020,
                    completed: true,
                    internship: {
                        hours: 290,
                        location: "Benin"
                    }
                }
            ]
        },
        AddUser: {
            $name: "",
            $age: 10,
            about: ""
        }
    }
}

const outputFile = './swagger_output.json'
const endpointsFiles = ['./app.js']

swaggerAutogen(outputFile, endpointsFiles, doc).then(() => {
    require('./app')           // Your project's root file
})
