const router = require('express').Router();
const controller = require('../controllers/biography.controller');
const userController = require('../controllers/user.controller');

router.get('/', controller.find);

router.get('/:id', controller.findById);

router.get('/user/:id', controller.findUserBiography);

router.post('/', controller.save, userController.addBiography);

router.put('/:id', controller.findByIdAndUpdate);

router.put('/remove/:id', controller.delete, userController.removeBiography);

module.exports = router;
