const router =require('express').Router();
const payementcontroller = require('../controllers/payement');

router.get('/send/:token', payementcontroller.send);
router.get('/success', payementcontroller.success);
router.get('/cancel', payementcontroller.cancel);

module.exports = router;

