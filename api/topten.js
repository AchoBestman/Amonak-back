const router =require('express').Router();
const TopTen=require('../models/topten');
const upload=require('../middlewares/multer');
const topTencontroller = require('../controllers/topten');
const email = require('../middlewares/mail');
const validators = require('../validators/topten');
const isLog = require('../middlewares/routes.secure');

router.post('/postTopTen', isLog, [upload.multipleUpload, validators], topTencontroller.createPostTopTen);
router.post('/be-interested', topTencontroller.beInterested);
router.get('/', topTencontroller.getAll);
router.get('/:id', isLog, topTencontroller.findById);
router.get('/delete/:id', isLog, topTencontroller.deleteOne);
router.get('/status/:status', isLog, topTencontroller.findByStatus);
 
module.exports=router;  
 