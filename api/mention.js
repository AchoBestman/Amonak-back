const router = require('express').Router();
const Mention = require('../models/mention');
const pusher = require('../middlewares/pusher');

// const pusher = new Pusher({
//     appId: `${process.env.PUSHER_APP_ID}`,
//     key: `${process.env.PUSHER_API_KEY}`,
//     secret: `${process.env.PUSHER_API_SECRET}`,
//     cluster: `${process.env.PUSHER_APP_CLUSTER}`,
//     encrypted: true
// });

router.get('/', (req, res, next) => {
    res.redirect('/mention/getall');
});

router.get('/getall', (req, res, next) => {
    Mention.find()
        .then(mentions => res.json(mentions))
        .catch(err => res.status(400).json('mention get error ' + err));
});

router.get('/ber', (req, res) => {
    // pusher.trigger('notification', 'new', {
    //     msg: `${req.body.msg}`,
    // });
    // res.json(`${process.env.PUSHER_APP_ID}`);
    res.json("bonjour");
})

router.post('/create', (req, res, next) => {
    const date = new Date().toISOString();
    const creator = req.body.creator;
    const publication = req.body.publication;

    Mention.find({creator: creator, publication: publication})
        .then(mention => {
            if(!mention.length){
                const newMention = new Mention({
                    date,
                    creator,
                    publication
                });

                newMention.save()
                    .then(mention => res.json(mention))
                    .catch(err => res.status(400).json('mention save error ' + err));
            }else{
                console.log("existe déjà");
                res.json("existe déjà");
            }
        })
        .catch(err => res.status(400).json('erreur ajout mention ' + err))
});


router.delete('/delete/:id', (req, res, next) => {
    Mention.findOneAndDelete(req.params.id)
        .then(() => res.json('mention deleted'))
        .catch(err => res.status(400).json('mention delete error ' + err));
});

module.exports = router;