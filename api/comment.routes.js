const router =require('express').Router();
const controller = require('../controllers/comment.controller');
const publicationController = require('../controllers/publication.controller');
const notifController = require('../controllers/notification.controller');

router.get('/', controller.find);

router.get('/:id', controller.findById);

router.get('/publication/:id', controller.findPublicationComments);

router.get('/user/:id', controller.findUserComments);

// router.post('/', controller.save, publicationController.addComment, notifController.savePubNotif);
router.post('/', controller.save);
// router.post('/', controller.save, publicationController.addComment, notifController.saveCommentNotif);

router.post('/:id/like', controller.addLike);

router.post('/:id/comment', controller.addComment);

router.put('/:id/like', controller.removeLike);

router.put('/:id/comment', controller.removeComment);

router.put('/:id/remove', controller.delete, publicationController.removeComment);

module.exports = router;
