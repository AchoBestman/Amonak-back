const router =require('express').Router();
const UserM=require('../models/user');
const userC = require('../controllers/user_a');
const bibliography = require('../controllers/bibliography');
const upload=require('../middlewares/multer');

router.post('/bibliography',upload.multipleUpload,bibliography.createCivility);
router.get('/bibliography',bibliography.getAll);
router.get('/bibliography/delete/:id',bibliography.deleteById);

router.post('/biography',upload.multipleUpload,bibliography.createBiography);
router.get('/biography',bibliography.allBiography);
router.get('/biography/:id',bibliography.getBiographyById);

router.post('/', userC.getUser);
module.exports=router;  

