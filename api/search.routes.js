const router = require('express').Router();
const commentController = require('../controllers/comment.controller');
const publicationController = require('../controllers/publication.controller');
const userController = require('../controllers/user.controller');

const controller = require('../controllers/search.controller');

router.get('/:search', controller.search);

router.get('/user/:search', controller.searchUser);

router.get('/publication/:search', controller.searchPublication);

router.get('/product/:search', controller.searchProduct);

router.get('/comment/:search', commentController.findByContent);

module.exports = router;
