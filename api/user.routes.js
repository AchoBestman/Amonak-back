const router = require('express').Router();
const controller = require('../controllers/user.controller');
const { avatarUpload } = require('../middlewares/multer');
const isLog = require('../middlewares/routes.secure');
const publicationController = require('../controllers/publication.controller');

router.get('/', controller.find);

router.get('/:id', controller.findById);

router.post('/name', controller.findByName);

router.post('/email', controller.findByEmail);

// router.put('/:id', avatarUpload.single('file'), controller.findByIdAndUpdate);
router.put('/:id', avatarUpload.single('file'), controller.updateAvatar);

router.put('/:id/config', controller.findByIdAndUpdate);

//via the edit form
router.put('/:id/update-profile', controller.updateProfile);

router.put('/:id/biography', controller.addBiography);

router.put('/:id/updateIsNewFeed', controller.updateIsNewFeed, publicationController.find);

router.put('/:id/updateFirstTime', controller.updateFirstTime);

module.exports = router;
