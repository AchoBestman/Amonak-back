const router =require('express').Router();
const User = require('../models/user');
const Amis = require('../models/amis');

//update my profil

router.route('/updateMyProfil/:id').post((req,res)=>{
    const username=req.body.username;
    const email=req.body.email;
    const password=req.body.password;
    const secteur=req.secteur;
    const profession=req.body.profession;
    const adresse = req.body.adresse;
    const numCarteBank = req.body.numCarteBank;
    const photoUrl = req.body.photoUrl;
    User.findOneAndUpdate(req.params.id)
        .then(users=>{
            if(username){
                users.set('username',username);
            }
            if(email){
                users.set('email',email);
            }
            if(password){
                users.set('password',password);
            }
            if(secteur){
                users.set('secteur',secteur);
            }

            if(profession){
                users.set('profession',profession);
            }
             if(adresse){
                users.set('adresse',adresse);
            }
            if(numCarteBank){
                users.set('numCarteBank',numCarteBank);
            }
            if(photoUrl){
                users.set('photoUrl',photoUrl);
            }
           
            users.save();
            res.json(users);
        })
        .catch(err=>res.status(400).json('Error'+err));
});

//bloquer amis
router.route('/bloqueAmis/:idMe/:idAmis/:bloque').get((req,res)=>{
    Amis.findOneAndUpdate({invitationSender : req.params.idMe, invitationReceiver : req.params.idAmis})
        .then(amis => {
            date = new Date().toISOString();
            if(bloque == 'BAM'){
                amis.set('bloque_ami_message',1);
                amis.set('bloque_date',date);
            }if(bloque == 'BAP'){
                amis.set('bloque_ami_pubs',1);
                amis.set('bloque_date',date);
            }if(bloque == 'BAT'){
                amis.set('bloque_ami_tout',1);
                amis.set('bloque_date',date);
            }
            amis.save();
            res.json(amis);
        })
        .catch(err => res.status(400).json('Error : Friend not found ' + err));
});

//debloquer amis
router.route('/debloqueAmis/:idMe/:idAmis/:bloque').get((req,res)=>{
    Amis.findOneAndUpdate({invitationSender : req.params.idMe, invitationReceiver : req.params.idAmis})
        .then(amis => {
            date = new Date().toISOString();
            if(bloque == 'DBAM'){
                amis.set('bloque_ami_message',0);
                amis.set('bloque_date',date);
            }if(bloque == 'DBAP'){
                amis.set('bloque_ami_pubs',0);
                amis.set('bloque_date',date);
            }if(bloque == 'DBAT'){
                amis.set('bloque_ami_tout',0);
                amis.set('bloque_date',date);
            }
            amis.save();
            res.json(amis);
        })
        .catch(err => res.status(400).json('Error : Friend not found ' + err));
});


//search freinds to bloque




exports.getFriends = function(req, res, next){
  Amis.find({status: "accepted", $or: [{invitationSender: req.params.id}, {invitationReceiver: req.params.id}]})
    .populate('invitationSender', '_id username secteur')
    .populate('invitationReceiver', '_id username secteur')
    .then(amis => {
      console.log(req.params.id);
      res.json(amis)
    })
    .catch(err => res.status(400).json(err));
}