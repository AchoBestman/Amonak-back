const router = require('express').Router();
const controller = require('../controllers/notification.controller');


router.get('/', controller.find);

router.get('/:user_id', controller.findUserNotifs);

router.get('/:user_id/unread', controller.findUserUnreadNotifs);

router.post('/', controller.savePubNotif);

router.put('/:id', controller.findAndUpdate);

router.delete('/:id', controller.delete);

module.exports = router;
