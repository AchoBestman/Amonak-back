const router =require('express').Router();
const controller = require('../controllers/product.controller');
const uploadController = require('../middlewares/multer');


router.get('/', controller.find);

router.get('/:id', controller.findById);

router.get('/user/:id', controller.findUserProducts);

router.post('/', uploadController.multipleUpload, controller.save);

router.put('/:id', controller.findByIdAndUpdate);

module.exports=router;
