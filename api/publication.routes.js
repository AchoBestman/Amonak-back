const router =require('express').Router();
const controller = require('../controllers/publication.controller');
const productController = require('../controllers/product.controller');
const notifController = require('../controllers/notification.controller');

//All HTTP_GET ROUTES
router.get('/', controller.find);

router.get('/:id', controller.findById);

router.get("/:id/saved", controller.findUserPublicationsSaved);

router.get('/news-feed/:user_id', controller.findNewsFeeds);

router.get('/user/:user_id', controller.findUserPublications);

router.get('/user/:user_id/profile', controller.findProfilePublications);

router.get('/product/:product_id', controller.findByProduct);

//All HTTP_POST ROUTES
router.post('/post', controller.savePost);

router.post('/alert', controller.saveAlert);

router.post('/sale', productController.save, controller.saveSale); 

// router.post('/share', controller.saveShare, notifController.savePubNotif);
router.post('/share', controller.saveShare, notifController.saveShareNotification);

router.post('/report', controller.reportPost);

// router.post('/:id/like', controller.addLike, notifController.savePubNotif);
router.post('/:id/like', controller.addLike, notifController.saveLikeNotification);

router.post('/:id/follow', controller.addFollower);

router.post('/:id/save', controller.addSaver);

router.post('/:id/delete', controller.addDeleter);

//All HTTP_PUT ROUTES
router.put('/:id/like', controller.removeLike);

router.put('/:id/follow', controller.removeFollower);

router.put('/:id/save', controller.removeSaver);

router.put('/:id/delete', controller.removeDeleter);

router.put('/delete/:id', controller.delete);

module.exports=router;
