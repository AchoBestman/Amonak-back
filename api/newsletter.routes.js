const router = require('express').Router();
const controller = require('../controllers/newsletter.controller');

router.get('/', controller.find);

router.get('/:id', controller.findOne);

router.post('/', controller.save);

router.delete('/:id', controller.delete);

module.exports = router;
