const router = require('express').Router();

const amisController = require('../controllers/amis');
const notifController = require('../controllers/notification.controller');

router.get('/', amisController.get)

// Retrieve  friendList
// router.get('/amis/getFriendList/:userIdFriends', amisController.getAmis);
// Retrieve  friendRequests
// router.get('/amis/friendRequest/:userIdFriends', amisController.getInvitation);
// add  friend
router.post('/new', amisController.addAmi, notifController.saveNotification);
// update  friendShipStatus
//reminder friendRequest Status ===> //pending//accepted//refused//deleted 
router.put('/updateStatus', amisController.updateStatus);

router.get('/friends/:id', amisController.getFriends);
router.get('/invitations/:id', amisController.getInvitations);
router.get('/getAllUsers/:id', amisController.getAllUsers);
router.get('/suggestions/:id', amisController.suggestion);
router.delete('/deleteFriendship/:sender&:receiver', amisController.deleteFriendship);
// router.get('/blocked/:id', amisController.getBlocked);

router.get('/blocked/:id', amisController.findUsersBlocked);

router.put('/block', amisController.blockUser);

router.put('/deblock', amisController.deblockUser);

router.get("/:id", amisController.getMobileSearch);

router.get("/:id/all", amisController.find);

module.exports = router;