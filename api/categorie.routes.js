const router = require('express').Router();
const controller = require('../controllers/categorie.controller')


router.post('/', controller.save);

router.get('/', controller.find);

router.get('/:id', controller.findOne);

router.put('/:id', controller.findOneAndUpdate);

router.delete('/:id', controller.deleteOne);

module.exports = router;
