const router = require('express').Router();
const controller = require('../controllers/admin.seller.controller');


router.get('/', controller.getSellersList);

router.get('/request', controller.getSellersRequest);

router.get('/seller/:id', controller.getOneSeller);

router.put('/:id', controller.updateSeller);

// router.delete('/:id', controller.deleteOneSeller);

module.exports = router;
