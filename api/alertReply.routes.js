const router = require('express').Router();
const controller = require('../controllers/alertReply.controller');
const uploadController = require("../middlewares/multer");
const MessageController = require('../controllers/message.controller');
const notifController = require('../controllers/notification.controller');

router.get('/', controller.find);

router.get('/id', controller.findById);

router.post('/', uploadController.multipleUpload, controller.create);

router.post('/reply', controller.create, MessageController.sendReply, notifController.savePubNotif);

router.delete('/:id', controller.delete);

module.exports = router;
