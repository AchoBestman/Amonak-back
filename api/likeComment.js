const router = require('express').Router();
const LikeComment = require('../models/likeComment');

router.post('/create', (req, res ,next) => {
    const date = new Date().toISOString();
    const creator = req.body.creator;
    const comment = req.body.comment;
    
    newLikeComment = new LikeComment({
        date,
        creator,
        comment
    });
    newLikeComment.save()
        .then((like) => {
            console.log(like);
            res.json(like);
        })
        .catch(err => res.status(400).json(err));
});

router.get('/', (req, res, next) => {
    console.log(new Date().toString())
    LikeComment.find()
        .then((likes) => {
            res.json(likes);
        })
        .catch(err => res.status(400).json(err));
});

router.get('/:id', (req, res, next) => {
    LikeComment.find({creator: req.params.id}, "comment -_id")
        .then(likes => res.json(likes))
        .catch(err => res.status(400).json(err));
})

module.exports = router;