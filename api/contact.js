const router =require('express').Router();
const TopTen=require('../models/contact');
const contactcontroller = require('../controllers/contact');
const upload=require('../middlewares/multer');

router.post('/postContact',contactcontroller.create);
router.get('/contact', contactcontroller.getAll);

router.post('/postWork', upload.multipleUpload,contactcontroller.createDeliveryWork);
router.get('/work', contactcontroller.getAllWork);

router.post('/postMake',upload.multipleUpload,contactcontroller.createDeliveryMake);
router.get('/make', contactcontroller.getAllMake);


module.exports=router;  




