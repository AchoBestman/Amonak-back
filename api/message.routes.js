const router =require('express').Router();
const controller = require('../controllers/message.controller');
const amisController = require('../controllers/amis');

// router.route('/create').post(date, (req, res) => {

router.get('/', controller.find);

router.get('/:user_id', controller.findUserDiscussion);

router.post('/', controller.save);

router.put('/:id', controller.findOneAndUpdate);

router.delete('/:id', controller.deleteMessage);

module.exports=router;
