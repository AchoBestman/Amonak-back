const router = require('express').Router();
const controller = require('../controllers/alert.controller');

router.get('/', controller.findAll);
router.post('/', controller.createAlert);