const router = require('express').Router();
const controller = require('../controllers/auth.controller');
const notifController = require('../controllers/notification.controller');
const verify = require('../middlewares/verifySignUp');
const isLog = require('../middlewares/routes.secure');
const user_country_infos = require('../middlewares/country_infos');

router.get('/check-token/:token', isLog, controller.checkTokenValidation);

router.post('/signup', [verify.checkSignUpData], controller.signup);

router.post('/login', [verify.checkSignInData, user_country_infos], controller.signin);

router.post('/login-with-email', [verify.checkSignInData, user_country_infos], controller.signinWith);

router.get('/activate/:token',user_country_infos, controller.activation, notifController.saveWelcomeNotification);

router.post('/send', controller.sendMail);

router.post('/reset-password', [isLog, user_country_infos], controller.resetPassword);

router.post('/logout', isLog, controller.logout);

router.post('/check-email', user_country_infos, verify.checkEmail);

module.exports = router;
  