const router =require('express').Router();
const TopTen=require('../models/panier');
const upload=require('../middlewares/multer');
const paniercontroller = require('../controllers/panier');
const email = require('../middlewares/mail');

router.post('/panierCreate', upload.multipleUpload, paniercontroller.createPanier);
router.post('/update', paniercontroller.updatePanierCommentAdress);
router.get('/', paniercontroller.getAll);
router.get('/:user_id/:status', paniercontroller.getOneByStatus);
router.get('/:id', paniercontroller.getPanierByTransactionId);
module.exports=router;  
 