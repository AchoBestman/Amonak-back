const router = require('express').Router();
const Share = require('../models/share');

router.get('/', (req, res, next) => {
    res.redirect('/share/getall');
});

router.get('/getall', (req, res, next) => {
    Share.find()
        .then(shares => res.json(shares))
        .catch(err => res.status(400).json('share error ' + err));
});

router.post('/create', (req, res, next) => {
    const content = req.body.content;
    const date = new Date().toISOString();
    const creator = req.body.user_id;
    const publication = req.body.publication;
    const newShare = new Share({
        content,
        date,
        creator,
        publication
    });
    newShare.save()
        .then(share => res.json(share))
        .catch(err => res.status(400).json('share save error ' + err));
});

router.delete('/delete/:id', (req, res, next) => {
    Share.findOneAndDelete(req.params.id)
        .then(() => res.json('share deleted.'))
        .catch(err => res.status(400).json('share delete error' + err));
});